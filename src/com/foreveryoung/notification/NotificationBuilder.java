package com.foreveryoung.notification;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

public class NotificationBuilder {

	NotificationManager nm;

	public void sendMessage(FYNotification notification, Activity callingActivity) {
		
			Bitmap bit = BitmapFactory.decodeResource(callingActivity.getResources(), notification.getBigIconResID());
			NotificationCompat.Builder mBuilder =
					new NotificationCompat.Builder(callingActivity)
			.setSmallIcon(notification.getIconResID())
			.setLargeIcon(Bitmap.createScaledBitmap(bit, 150, 150, false))
			.setContentTitle(notification.getTitle())
			.setContentText(notification.getMessage());
			Intent resultIntent = new Intent(callingActivity, notification.getClassToOpen());

			
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(callingActivity);
			// Adds the back stack for the Intent (but not the Intent itself)
			stackBuilder.addParentStack(notification.getClassToOpen());
			// Adds the Intent that starts the Activity to the top of the stack
			stackBuilder.addNextIntent(resultIntent);
			
			PendingIntent resultPendingIntent =
					stackBuilder.getPendingIntent(
							0,
							PendingIntent.FLAG_UPDATE_CURRENT
							);
			mBuilder.setContentIntent(resultPendingIntent);
			
			nm = (NotificationManager)callingActivity.getSystemService(Context.NOTIFICATION_SERVICE);
			nm.notify(notification.getId(), mBuilder.build());
			//callingActivity.finish();
		}

}
