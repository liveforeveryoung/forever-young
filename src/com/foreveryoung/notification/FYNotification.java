package com.foreveryoung.notification;

import com.foreveryoung.ForeverYoungPatient;
import com.foreveryoung.R;
import com.foreveryoung.SessionData;

public class FYNotification {

	private String message;
	private String title;
	private NotificationType type;
	private int id;
	private int iconResId;
	private int iconBigId;
	private Class<?> classToOpen;
	
	public enum NotificationType {
		SEVERE,DOCTOR,BASIC,SUGGESTION,TREND
	}
	
	public FYNotification(String message, NotificationType type) {
		super();
		this.id = (int)(Math.random()*10000);
		switch (type) {
			case SEVERE:
				this.message = "A poor health condition has been recognized.";
				this.title = message;
				iconResId = R.drawable.notification_severe;
				iconBigId = R.drawable.notification_severe;
				break;
			case DOCTOR:
				this.message = message;
				this.title = "Doctor Recommendation";
				iconResId = R.drawable.notification_doctor;
				iconBigId = R.drawable.notification_doctor;
				break;
			case SUGGESTION:
				this.message = message;
				this.title = "Forever Young Suggestion of the Day";
				iconResId = R.drawable.notification_suggestion;
				iconBigId = R.drawable.notification_suggestion;
				break;
			case TREND:
				this.message = message;
				this.title = "Health Trend Analysis";
				iconResId = R.drawable.notification_trend;
				iconBigId = R.drawable.notification_trend;
				break;
			default:
				this.message = "Click here to open Forever Young.";
				this.title = SessionData.getInstance().getNoticeList().size()+" new health suggestions.";
				iconResId = R.drawable.notification_new;
				iconBigId = R.drawable.notification_new;
				break;
					}
		this.setClassToOpen(ForeverYoungPatient.class);
		this.setType(type);
	}

	public int getIconResID () {
		return iconResId;
	}
	
	public int getBigIconResID () {
		return iconBigId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Class<?> getClassToOpen() {
		return classToOpen;
	}

	public void setClassToOpen(Class<?> classToOpen) {
		this.classToOpen = classToOpen;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}
	
}
