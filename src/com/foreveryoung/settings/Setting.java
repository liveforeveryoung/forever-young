package com.foreveryoung.settings;

import android.view.View;

public class Setting {
	 
    int itemNameID;
    int iconResID;
    int secondIconResID;
    View setting;
    
	int backgroundID;

    public Setting(int itemNameID, int iconResID, int backgroundID, View typeOfSetting) {
          this.itemNameID = itemNameID;
          this.iconResID = iconResID;
          this.backgroundID = backgroundID;
          this.setting = typeOfSetting;
    }

    public View getSetting() {
		return setting;
	}

	public void setSetting(View setting) {
		this.setting = setting;
	}

	public int getBackgroundID() {
		return backgroundID;
	}

	public void setBackgroundID(int backgroundID) {
		this.backgroundID = backgroundID;
	}

	public int getItemNameID() {
          return itemNameID;
    }
    public void setItemNameID(int itemNameID) {
    	this.itemNameID = itemNameID;
    }
    public int getIconResID() {
          return iconResID;
    }
    public void setIconResID(int imgResID) {
          this.iconResID = imgResID;
    }

	public int getSecondIconResID() {
		return secondIconResID;
	}

	public void setSecondIconResID(int secondIconResID) {
		this.secondIconResID = secondIconResID;
	}

}