package com.foreveryoung;

import com.foreveryoung.views.fragments.content.SignIn;
import android.os.Bundle;

import android.widget.ImageView;

public class ForeverYoungWelcome extends FYBootstrapActivity {
		
		private ImageView logo;
	
		public ForeverYoungWelcome() {
			super();
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_welcome);
	        
	        logo = (ImageView) findViewById(R.id.logo_welcome);
	        getFragmentManager().beginTransaction().replace(R.id.welcome_content, new SignIn()).commit();
	        
	}
	}
