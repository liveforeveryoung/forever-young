package com.foreveryoung;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

/**
 * This bootstrap activity runs when the application is booting up.
 * @author jeremywallace
 *
 */

public class FYBootstrapActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	ApplicationData.getInstance();
		
		try {
			restoreState();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("login activity", "File not found: " + e.toString());
		} 
    	
    	// Load data from database here...
    }
	
	/**
	 * This function saves the state on exit of the application.
	 */
	protected void saveState()
	   {

		   String filename = "forever-young.json";
		   FileOutputStream outputStream;

		   try {
		     outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
		     outputStream.write(SessionData.getInstance().getJSON().getBytes());
		     outputStream.close();
		   } catch (Exception e) {
		     e.printStackTrace();
		   }
	       
	   }
	
	
	/**
	 * This function reads a stored JSON file to rebuild the users info.
	 */
	protected void restoreState() throws FileNotFoundException, IOException {
	    	
	        InputStream inputStream = openFileInput("forever-young.json");

	        if ( inputStream != null ) {
	        	
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String fyJSON = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (fyJSON = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(fyJSON);
	            }

	            inputStream.close();
	            
	            SessionData.getInstance().parseJSON(fyJSON);
	        }
	}	
	
}
