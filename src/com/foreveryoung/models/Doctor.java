package com.foreveryoung.models;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.foreveryoung.mongodb.MongoDBException;
import com.foreveryoung.mongodb.MongoDBTask;
import com.foreveryoung.mongodb.NoRecordFoundException;

/**
 * Doctor class is a model for a doctor.
 * @author jeremywallace
 * 
 */
public class Doctor extends User {
	
	private Clinic address;
	private String phone;
	private List<Patient> patients;

	public Doctor(int id, String email, Clinic address, String phone, List<Patient> patients) {
		super(email,id);
		// TODO Auto-generated constructor stub
		this.address = address;
		this.phone = phone;
		this.patients = patients;
	}
	
	public Doctor(JSONObject jsonObject) throws JSONException {
		super(jsonObject);
		
		Log.d("JSON",jsonObject.toString());

		address = new Clinic(
				jsonObject.getJSONObject("address").getString("addr1"), 
				jsonObject.getJSONObject("address").getString("addr2"), 
				jsonObject.getJSONObject("address").getString("city"),
				jsonObject.getJSONObject("address").getString("province"),
				jsonObject.getJSONObject("address").getString("country"), 
				jsonObject.getJSONObject("address").getString("postalcode"),
				jsonObject.getJSONObject("address").getString("phone"),
				jsonObject.getJSONObject("address").getString("latitude"),
				jsonObject.getJSONObject("address").getString("longitude"));
		phone = jsonObject.getString("phone");

	}

	@Override
	public String getFullName() {
		return "Dr. "+super.getFullName();
	}
	
	public List<Patient> getPatients() {
		if (patients==null) {
			try {
				patients = (new MongoDBTask()).getPatients(getId());
			} catch (NoRecordFoundException e) {
				patients = null;
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MongoDBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public String getPhone() {
		return phone;
	}

	public Clinic getAddress() {
		return address;
	}

	public void setAddress(Clinic address) {
		this.address = address;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
