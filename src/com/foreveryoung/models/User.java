package com.foreveryoung.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This represents a user object.
 * 
 * @author jeremywallace
 * 
 */
public abstract class User {

	// Implement Methods
	private String fullName;
	private String city;
	private String province;
	private String country;
	private String email;
	private String password;
	private int id;
//	private int year;
//	private int month;
//	private int day;
//	private boolean flag = true;

	public User(String email, int id) {
		this.id = id;
		this.email = email;
	}
	
	public User(JSONObject responseJSON) throws JSONException {
		String fullName = responseJSON.getString("name");
		this.email = responseJSON.getString("email");
		this.id = Integer.valueOf(responseJSON.getString("_id"));
 		this.fullName = fullName.substring(fullName.indexOf(":") + 2,
				fullName.indexOf("\","))+" "+
				fullName.substring(fullName.indexOf(":", 13) + 2,
				fullName.indexOf("\"}"));
 		this.email = responseJSON.getString("email");
 		try {
			JSONObject address = new JSONObject (responseJSON.getString("address"));
	//		JSONObject date = new JSONObject (data.getString("birthDate"));
	//		this.city = Integer.parseInt(address.getString("city"));
			this.city = address.getString("city");
			this.province = address.getString("province");
			this.country = address.getString("country");
 		} catch (JSONException ex) {
 			this.city = "";
 			this.province = "";
 			this.country = "";
 		}
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
//
//	public int getYear() {
//		return year;
//	}
//
//	public void setYear(int year) {
//		this.year = year;
//	}
//
//	public int getMonth() {
//		return month;
//	}
//
//	public void setMonth(int month) {
//		this.month = month;
//	}
//
//	public int getDay() {
//		return day;
//	}
//
//	public void setDay(int day) {
//		this.day = day;
//	}

}
