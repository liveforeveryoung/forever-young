package com.foreveryoung.models.parameters;

import java.util.Date;

import com.foreveryoung.Constants.ParameterType;

public abstract class HealthParameter {
	
	Date timestamp;
	HealthState state;
	boolean ignore;
	ParameterType type;
	
	public enum HealthState {
	    GOOD,MEDIOCRE,BAD	
	}
	
	public HealthParameter(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public abstract int getIconResID();
	
	public HealthState getState() {
		return state;
	}
	
	public abstract void setState();
	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public boolean isIgnored() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}
	
	public ParameterType getType() {
		return type;
	}
}
