package com.foreveryoung.models.parameters.lists;

import com.foreveryoung.models.parameters.Exercise;

public class ExerciseList extends HealthParameterList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Exercise get(int position) {
		return (Exercise) super.get(position);
	}
	
}
