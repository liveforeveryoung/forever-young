package com.foreveryoung.models.parameters.lists;

import com.foreveryoung.models.parameters.BloodPressure;
import com.foreveryoung.models.parameters.HealthParameter.HealthState;

public class BloodPressureList extends HealthParameterList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public BloodPressure get(int position) {
		return (BloodPressure) super.get(position);
	}
	
	@Override
	public HealthState getOverallState() {
		if (timeSeriesState==null) {
			return getLatestValue().getState();
		} else {
			switch (timeSeriesState) {
				case ALL_GOOD:
				case MOST_GOOD_NO_BAD:
					 return HealthState.GOOD;
				case ALL_MED:
				case HALF_GOOD_HALF_MED:
				case HALF_GOOD_HALF_BAD:
				case MOST_MED_NO_BAD:
				case MOST_MED_FEW_BAD:
				case MOST_GOOD_FEW_BAD:
					return HealthState.MEDIOCRE;
				case ALL_BAD:
				case HALF_MED_HALF_BAD:
				case MOST_BAD_NO_GOOD:
				case MOST_BAD_FEW_GOOD:
					return HealthState.BAD;
				default:
					return getLatestValue().getState();
			}
		}
	}
	
}
