package com.foreveryoung.models.parameters.lists;

import java.util.ArrayList;
import com.foreveryoung.models.parameters.HealthParameter;
import com.foreveryoung.models.parameters.HealthParameter.HealthState;

public class HealthParameterList extends ArrayList<HealthParameter> {

	int 	UNKNOWN = -1,
			GOOD = 0, 
			MEDIOCRE = 1, 
			BAD = 2;
	
	
	public enum HealthTrendResult {
		ALL_GOOD,ALL_MED,ALL_BAD,
		HALF_GOOD_HALF_BAD, HALF_GOOD_HALF_MED, HALF_MED_HALF_BAD,
		MOST_GOOD_FEW_BAD,MOST_GOOD_NO_BAD,
		MOST_BAD_FEW_GOOD,MOST_BAD_NO_GOOD,
		MOST_MED_FEW_BAD,MOST_MED_NO_BAD,
		ALL_LOW_VALUES,ALL_HIGH_VALUES
	}
	
	public enum HealthStateParam {
		MOST,FEW,HALF,ALL,NONE
	}
	
	protected HealthTrendResult timeSeriesState;
	protected HealthStateParam[] currentState = new HealthStateParam[3];
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HealthTrendResult getTimeSeriesState() {
		return timeSeriesState;
	}
	
	public HealthParameter getLatestValue() {
		return get(size() - 1);
	}
	
	public HealthStateParam[] getTrendState() {
		return currentState;
	}
	
	public HealthState getOverallState() {
		return getLatestValue().getState();
	}
	
	public HealthTrendResult getResults() {
		return timeSeriesState;
	}

	public HealthTrendResult analyzeState() {
			
		if (currentState[GOOD]==HealthStateParam.ALL) {
			return HealthTrendResult.ALL_GOOD;
		} else if (currentState[MEDIOCRE]==HealthStateParam.ALL) {
			return HealthTrendResult.ALL_MED;
		} else if (currentState[BAD]==HealthStateParam.ALL) {
			return HealthTrendResult.ALL_BAD;
		}
		
		if (currentState[GOOD]==HealthStateParam.HALF &&
			currentState[MEDIOCRE]==HealthStateParam.HALF) {
			return HealthTrendResult.HALF_GOOD_HALF_MED;
		} else if (currentState[GOOD]==HealthStateParam.HALF &&
			currentState[BAD]==HealthStateParam.HALF) {
			return HealthTrendResult.HALF_GOOD_HALF_BAD;
	    } else if (currentState[BAD]==HealthStateParam.HALF &&
			currentState[MEDIOCRE]==HealthStateParam.HALF) {
			return HealthTrendResult.HALF_MED_HALF_BAD;
		}
		
		if (currentState[BAD]==HealthStateParam.FEW &&
				currentState[GOOD]==HealthStateParam.MOST) {
				return HealthTrendResult.MOST_GOOD_FEW_BAD;
		} else if (currentState[GOOD]==HealthStateParam.MOST &&
				currentState[BAD]==HealthStateParam.NONE) {
				return HealthTrendResult.MOST_GOOD_NO_BAD;
		} else if (currentState[MEDIOCRE]==HealthStateParam.MOST &&
				currentState[BAD]==HealthStateParam.FEW) {
				return HealthTrendResult.MOST_MED_FEW_BAD;
		} else if (currentState[MEDIOCRE]==HealthStateParam.MOST &&
				currentState[BAD]==HealthStateParam.NONE) {
				return HealthTrendResult.MOST_MED_NO_BAD;
		} else if (currentState[BAD]==HealthStateParam.MOST &&
				currentState[GOOD]==HealthStateParam.NONE) {
				return HealthTrendResult.MOST_BAD_NO_GOOD;
		} else if (currentState[BAD]==HealthStateParam.MOST &&
				currentState[GOOD]==HealthStateParam.FEW) {
				return HealthTrendResult.MOST_BAD_FEW_GOOD;
		}
		return null;
	}

	public void setTimeSeriesState(HealthTrendResult timeSeriesState) {
		this.timeSeriesState = timeSeriesState;
	}

	public void setCurrentState(HealthStateParam[] currentState) {
		this.currentState = currentState;
	}

	

}
