package com.foreveryoung.models.parameters.lists;

import com.foreveryoung.models.parameters.Sleep;

public class SleepList extends HealthParameterList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public Sleep get(int position) {
		return (Sleep) super.get(position);
	}
	
	
}
