package com.foreveryoung.models.parameters.lists;

import com.foreveryoung.models.parameters.Weight;

public class WeightList extends HealthParameterList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public Weight get(int position) {
		return (Weight) super.get(position);
	}
	
}
