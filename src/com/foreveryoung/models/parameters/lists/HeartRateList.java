package com.foreveryoung.models.parameters.lists;

import com.foreveryoung.models.parameters.HealthParameter;
import com.foreveryoung.models.parameters.HealthParameter.HealthState;
import com.foreveryoung.models.parameters.HeartRate;

public class HeartRateList extends HealthParameterList {

	private static final long serialVersionUID = 1L;

	@Override
	public HeartRate get(int position) {
		return (HeartRate) super.get(position);
	}
	
	@Override
	public HealthState getOverallState() {
		if (timeSeriesState==null) {
			return getLatestValue().getState();
		} else {
			switch (timeSeriesState) {
				case ALL_GOOD:
					 return HealthState.GOOD;
				case ALL_LOW_VALUES:
					return HealthState.MEDIOCRE;
				case ALL_HIGH_VALUES:
					return HealthState.BAD;
				default:
					return getLatestValue().getState();
			}
		}
	}
	
	@Override
	public HealthTrendResult analyzeState() {
		int over100 = 0, under90 = 0, over100per, under90per;

		for (HealthParameter hr : this) {
			int hrVal = ((HeartRate)hr).getHeartRate();
			if (hrVal>100) {
				over100++;
			} else if (hrVal<90) {
				under90++;
			}
		}
		
		over100per = (100*over100/size());
		under90per = (100*under90/size());
		
		if (under90per>90) {
			return HealthTrendResult.ALL_LOW_VALUES;
		} else if (over100per>90) {
			return HealthTrendResult.ALL_HIGH_VALUES;
		} else {
			return HealthTrendResult.ALL_GOOD;
		}
	}
}
