package com.foreveryoung.models.parameters;

import java.util.Date;

import com.foreveryoung.Constants;
import com.foreveryoung.R;
import com.foreveryoung.Constants.ParameterType;

public class Sleep extends HealthParameter {

	double deepSleep;
	double totalSleep;
	int timesAwake;
	


	/**
	 * @param deepSleep
	 * @param totalSleep
	 * @param timesAwake
	 */
	public Sleep( double totalSleep, double deepSleep, int timesAwake, Date timestamp) {
		super(timestamp);
		this.deepSleep = deepSleep;
		this.totalSleep = totalSleep;
		this.timesAwake = timesAwake;
		this.type = ParameterType.SLEEP;
		setState();
	}

	/**
	 * @return the deepSleep
	 */
	public double getDeepSleep() {
		return deepSleep;
	}

	/**
	 * @return the totalSleep
	 */
	public double getTotalSleep() {
		return totalSleep;
	}

	/**
	 * @return the timesAwake
	 */
	public int getTimesAwake() {
		return timesAwake;
	}

	/**
	 * @param deepSleep the deepSleep to set
	 */
	public void setDeepSleep(double deepSleep) {
		this.deepSleep = deepSleep;
	}

	/**
	 * @param totalSleep the totalSleep to set
	 */
	public void setTotalSleep(double totalSleep) {
		this.totalSleep = totalSleep;
	}

	/**
	 * @param timesAwake the timesAwake to set
	 */
	public void setTimesAwake(int timesAwake) {
		this.timesAwake = timesAwake;
	}

	@Override
	public int getIconResID() {
		// TODO Auto-generated method stub

		switch (getState()) {
		case GOOD:
			return R.drawable.sleep_gr;
		case MEDIOCRE:
			return R.drawable.sleep_or;
		case BAD:
			return R.drawable.sleep_red;
		}
		return 0;

	}

	@Override
	public String toString() {
		return totalSleep+" hrs";
	}

	@Override
	public void setState() {
		
		// Default is a good sleep.
		state = HealthState.GOOD;
			
			// Check the number of times awake.
			if (timesAwake>Constants.TIMES_AWAKE_GOOD_LIMIT) {
				state = HealthState.MEDIOCRE;
			} else if (timesAwake<Constants.TIMES_AWAKE_MED_LIMIT) {
				state = HealthState.BAD;
			}
			
			// Check total sleep time.
			if (totalSleep<Constants.TOTAL_SLEEP_MED_LOW_LIMIT) {
				state = HealthState.BAD;
			} else if (totalSleep<Constants.TOTAL_SLEEP_GOOD_LOW_LIMIT) {
				state = HealthState.MEDIOCRE;
			} else if (totalSleep>Constants.TOTAL_SLEEP_GOOD_HIGH_LIMIT) {
				state = HealthState.MEDIOCRE;
			} else if (totalSleep>Constants.TOTAL_SLEEP_MED_HIGH_LIMIT) {
				state = HealthState.BAD;
			} else if (state == HealthState.BAD) {
				state = HealthState.MEDIOCRE;
			}
			
			/** Future DEEP SLEEP 
			if (deepSleep<LimitConstants.DEEP_SLEEP_GOOD_LIMIT) {
				state = HealthState.MEDIOCRE;
			} else if (deepSleep<LimitConstants.DEEP_SLEEP_MED_LIMIT) {
				state = HealthState.BAD;
			}*/
		}

}
