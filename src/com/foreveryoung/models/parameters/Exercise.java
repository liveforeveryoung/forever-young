package com.foreveryoung.models.parameters;

import java.util.Date;

import com.foreveryoung.Constants;
import com.foreveryoung.R;
import com.foreveryoung.Constants.ParameterType;

public class Exercise extends HealthParameter {

	private int stepsTaken;
	private double highlyActiveTime;


	
	/**
	 * @param stepsTaken
	 * @param highlyActiveTime
	 */
	public Exercise(int stepsTaken, double highlyActiveTime, Date timestamp) {
		super(timestamp);
		this.stepsTaken = stepsTaken;
		this.highlyActiveTime = highlyActiveTime;
		this.type = ParameterType.ACTIVITY;
		setState();
	}

	/**
	 * @return the stepsTaken
	 */
	public int getStepsTaken() {
		return stepsTaken;
	}

	/**
	 * @param stepsTaken the stepsTaken to set
	 */
	public void setStepsTaken(int stepsTaken) {
		this.stepsTaken = stepsTaken;
	}

	/**
	 * @return the highlyActiveTime
	 */
	public double getHighlyActiveTime() {
		return highlyActiveTime;
	}

	/**
	 * @param highlyActiveTime the highlyActiveTime to set
	 */
	public void setHighlyActiveTime(double highlyActiveTime) {
		this.highlyActiveTime = highlyActiveTime;
	}
	
	@Override
	public int getIconResID() {
		// TODO Auto-generated method stub
		
		switch (getState()) {
			case GOOD:
				return R.drawable.activity_gr;
			case MEDIOCRE:
				return R.drawable.activity_or;
			case BAD:
				return R.drawable.activity_red;
		}
		return 0;

	}

	@Override
	public void setState() {
		// TODO Auto-generated method stub
		
		state = HealthState.BAD;
		
		if (stepsTaken>=Constants.STEPS_TAKEN_GOOD_LIMIT) {
			state = HealthState.GOOD;
		} else if (stepsTaken>=Constants.STEPS_TAKEN_MED_LIMIT) {
			state = HealthState.MEDIOCRE;
		}
		
		if (state!=HealthState.GOOD) {
			if (highlyActiveTime>=Constants.ACTIVE_TIME_GOOD) {
				state = HealthState.GOOD;
			} else if (highlyActiveTime>=Constants.ACTIVE_TIME_MED) {
				state = HealthState.MEDIOCRE;
			}
		}
	}
	
	@Override
	public String toString() {
		return stepsTaken+" steps";
	}
}
