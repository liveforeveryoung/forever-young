package com.foreveryoung.models.parameters;

import java.util.Date;

import com.foreveryoung.Constants;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.R;

public class BloodPressure extends HealthParameter {

	private double systolic;
	private double diastolic;


	/**
	 * @param systolic
	 * @param diastolic
	 */
	public BloodPressure(double systolic, double diastolic, Date timestamp) {
		super(timestamp);
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.type = ParameterType.BLOOD_PRESSURE;
		setState();
	}

	@Override
	public Date getTimestamp() {
		return timestamp;
	}

	@Override
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the systolic
	 */
	public double getSystolic() {
		return systolic;
	}

	/**
	 * @return the diastolic
	 */
	public double getDiastolic() {
		return diastolic;
	}

	/**
	 * @param systolic the systolic to set
	 */
	public void setSystolic(double systolic) {
		this.systolic = systolic;
	}

	/**
	 * @param diastolic the diastolic to set
	 */
	public void setDiastolic(double diastolic) {
		this.diastolic = diastolic;
	}

	@Override
	public int getIconResID() {
		// TODO Auto-generated method stub

		switch (getState()) {
		case GOOD:
			return R.drawable.bloodpressure_gr;
		case MEDIOCRE:
			return R.drawable.bloodpressure_or;
		case BAD:
			return R.drawable.bloodpressure_red;
		}
		return 0;

	}

	@Override
	public void setState() {
		
		state = HealthState.GOOD;
		
		if(systolic>Constants.SYSTOLIC_MED_HIGH_LIMIT||diastolic>Constants.DIASTOLIC_MED_HIGH_LIMIT) {
			state = HealthState.BAD;
		} else if (systolic>Constants.SYSTOLIC_GOOD_HIGH_LIMIT||diastolic>Constants.SYSTOLIC_GOOD_HIGH_LIMIT) {
			state = HealthState.MEDIOCRE;
		} else if (systolic<Constants.SYSTOLIC_GOOD_LOW_LIMIT||diastolic<Constants.DIASTOLIC_GOOD_LOW_LIMIT) {
			state = HealthState.BAD;
		}
		
	}

	@Override
	public String toString() {
		return (int)systolic+" / "+(int)diastolic+" mmHg";
	}



}
