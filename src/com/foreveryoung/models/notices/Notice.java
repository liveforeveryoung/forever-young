package com.foreveryoung.models.notices;

import org.json.JSONException;
import org.json.JSONObject;

import com.foreveryoung.R;

public class Notice {

	protected String notice;
	protected String source;
	protected NoticeStatus status;
	
	public enum NoticeStatus {
		FINISHED,DELETED,NEW
	}
	/**
	 * @param notice
	 * @param source
	 * @param type
	 */
	public Notice(String notice, String source) {
		super();
		this.notice = notice;
		this.source = source;
		this.status = NoticeStatus.NEW;
	}
	
	public Notice (JSONObject responseJSON) throws JSONException {
		String fullName = responseJSON.getString("name");
		this.notice = responseJSON.getString("notice");
		//this.type = responseJSON.getString("notice");
		/*this.fullName = fullName.substring(fullName.indexOf(":") + 2,
				fullName.indexOf("\","))+" "+
				fullName.substring(fullName.indexOf(":", 13) + 2,
				fullName.indexOf("\"}"));
         */
	}

    public void setTypeByInt(int id) {
    	 /*switch (id) {
    	 	//case 0: 
    	 }*/
    }
	
	public String getNotice() {
		return notice;
	}
	
	public void setNotice(String suggestion) {
		this.notice = suggestion;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public int getIconResID() {
		return R.drawable.doc_rec_grey;
	}

	public NoticeStatus getStatus() {
		return status;
	}


	public void setStatus(NoticeStatus status) {
		this.status = status;
	}
	
}
