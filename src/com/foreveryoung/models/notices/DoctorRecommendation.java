package com.foreveryoung.models.notices;

import com.foreveryoung.R;

public class DoctorRecommendation extends Notice {

	private String prescribingDoctor;
	
	public DoctorRecommendation(String recommendation, String prescribingDoctor) {
		super(recommendation,prescribingDoctor);
		this.prescribingDoctor = prescribingDoctor;
	}
	
	public String getRecommendation() {
		return getNotice();
	}
	public void setRecommendation(String recommendation) {
		setNotice(recommendation);
	}
	public String getPrescribingDoctor() {
		return prescribingDoctor;
	}
	public void setPrescribingDoctor(String prescribingDoctor) {
		this.prescribingDoctor = prescribingDoctor;
	}
	
	@Override
	public int getIconResID() {
		return R.drawable.doc_rec_grey;
	}
	
}
