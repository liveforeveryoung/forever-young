package com.foreveryoung.models.notices;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.foreveryoung.R;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.HealthParameter.HealthState;

public class Suggestion extends Notice {

	HashMap<ParameterType,HealthState> stateTemplate = new HashMap<ParameterType,HealthState>();
	ParameterType type;
	
	public Suggestion(JSONObject responseJSON) throws JSONException {
		super(responseJSON.getString("Suggestion"),responseJSON.getString("Source"));
		
		stateTemplate.put(ParameterType.HEART_RATE, HealthState.GOOD);
		stateTemplate.put(ParameterType.BLOOD_PRESSURE,intToHealthState(responseJSON.getString("BloodPressure")));
		stateTemplate.put(ParameterType.WEIGHT,intToHealthState(responseJSON.getString("Weight")));
		stateTemplate.put(ParameterType.SLEEP,intToHealthState(responseJSON.getString("Sleep")));
		stateTemplate.put(ParameterType.ACTIVITY,intToHealthState(responseJSON.getString("Activity")));
		
		String type = responseJSON.getString("Type");
		if (type.equals("BLOOD_PRESSURE")) 
			this.type = ParameterType.BLOOD_PRESSURE;
		else if (type.equals("SLEEP")) 
			this.type = ParameterType.SLEEP;
		else if (type.equals("EXERCISE")) 
			this.type = ParameterType.ACTIVITY;
		else if (type.equals("WEIGHT")) 
			this.type = ParameterType.WEIGHT;
		else 
			this.type = ParameterType.HEART_RATE;

	}
	
	@Override
	public int getIconResID() {
		switch (type) {
			case BLOOD_PRESSURE:
				return R.drawable.bloodpressure_grey;
			case HEART_RATE:
				return R.drawable.heartrate_grey;
			case SLEEP:
				return R.drawable.sleep_grey;
			case ACTIVITY:
				return R.drawable.activity_grey;
			case WEIGHT:
				return R.drawable.weight_flyout_icon;
			default:
				return R.drawable.heartrate_grey;
		}
	}
	
	public Suggestion(String notice, String source, ParameterType type, HashMap<ParameterType,HealthState> stateTemplate ) {
		super(notice,source);
		this.type = type;
		this.stateTemplate = stateTemplate;
		// TODO Auto-generated constructor stub
	}

	public ParameterType getSuggestionType() {
		return type;
	}

	public void setSuggestionType(ParameterType type) {
		this.type = type;
		
	}
	
	private HealthState intToHealthState(String jsonOutput) {
		switch(Integer.valueOf(jsonOutput)) {
			case 1:
				return HealthState.GOOD;
			case 2:
				return HealthState.MEDIOCRE;
			case 3:
				return HealthState.BAD;
			default:
				return null;
		}
	}

	public HashMap<ParameterType, HealthState> getStateTemplate() {
		return stateTemplate;
	}

	public void setStateTemplate(HashMap<ParameterType, HealthState> stateTemplate) {
		this.stateTemplate = stateTemplate;
	}
}
