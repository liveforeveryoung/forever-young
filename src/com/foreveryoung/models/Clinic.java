package com.foreveryoung.models;

import android.text.Html;
import android.text.Spanned;

public class Clinic {

	private String city;
	private String country;
	private String postalCode;
	private String province;
	private String addr1;
	private String addr2;
	private String telephone;
	private String latitude;
	private String longitude;
	
	public Clinic(String addr1, String addr2, String city,
			String province, String country, String postalCode, String telephone,
			String latitude, String longitude) {
		super();
		this.city = city;
		this.country = country;
		this.postalCode = postalCode;
		this.province = province;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.telephone = telephone;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public Spanned getFullAddressText() {
		if (!addr2.isEmpty()) {
			addr2 = "<br />"+addr2;
		}
		return Html.fromHtml(addr1+addr2+"<br />"+city+", "+province+", "+country+" "
				+postalCode+"<br />"+" <strong>Clinic phone: </strong>"+telephone);
	}
	
	@Override
	public String toString() {
		return addr1+" "+addr2+" "+city+","+province+","+country+" "
				+postalCode+" Clinic phone:"+telephone;
	}
	
}
