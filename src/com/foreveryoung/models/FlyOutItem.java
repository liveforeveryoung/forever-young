package com.foreveryoung.models;

public class FlyOutItem {
	 
    int itemNameID;
    int iconResID;
    int secondIconResID;
    
	int backgroundID;

    public FlyOutItem(int itemNameID, int iconResID, int backgroundID, int secondIconResID) {
          this.itemNameID = itemNameID;
          this.iconResID = iconResID;
          this.backgroundID = backgroundID;
          this.secondIconResID = secondIconResID;
    }

    public int getBackgroundID() {
		return backgroundID;
	}

	public void setBackgroundID(int backgroundID) {
		this.backgroundID = backgroundID;
	}

	public int getItemNameID() {
          return itemNameID;
    }
    public void setItemNameID(int itemNameID) {
    	this.itemNameID = itemNameID;
    }
    public int getIconResID() {
          return iconResID;
    }
    public void setIconResID(int imgResID) {
          this.iconResID = imgResID;
    }

	public int getSecondIconResID() {
		return secondIconResID;
	}

	public void setSecondIconResID(int secondIconResID) {
		this.secondIconResID = secondIconResID;
	}

}