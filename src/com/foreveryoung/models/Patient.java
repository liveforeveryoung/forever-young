package com.foreveryoung.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.foreveryoung.R;
import com.foreveryoung.models.notices.Notice;
import com.foreveryoung.mongodb.MongoDBException;
import com.foreveryoung.mongodb.MongoDBTask;
import com.foreveryoung.mongodb.NoRecordFoundException;

public class Patient extends User {

	private double weight;
	private double height;
	private int dateOfBirth;
	private Doctor doctor;
	private String gender;
	private int doctorID;
	private List<Notice> notices;
	
	private String dob;


	public Patient(int id, String email, String fullName, int dateOfBirth, double weight,
			double height, Doctor doctor, String gender) {
		super(email,id);
		this.weight = weight;
		this.height = height;
		this.dateOfBirth = dateOfBirth;
		this.doctor = doctor;
		this.gender = gender;
		notices = new ArrayList<Notice>();
		// TODO Auto-generated constructor stub
	}

	public Patient(JSONObject responseJSON) throws JSONException {
		
		super(responseJSON);
		
		JSONObject patientData = responseJSON.getJSONObject("patientData");
		this.dateOfBirth = Integer.valueOf(patientData.getJSONObject("birthDate").getString("year"));
		this.height = Double.valueOf(patientData.getString("height"));
		this.weight = Double.valueOf(patientData.getString("weight"));
		this.gender = patientData.getString("gender");
		this.doctorID = Integer.valueOf(responseJSON.getString("doctor"));
		this.dob = patientData.getJSONObject("birthDate").toString();
		/*
		MongoDBTask mongo = new MongoDBTask();
		
		try {
			this.doctor = mongo.getPatientDoctor(Integer.valueOf(patientData.substring(
					patientData.indexOf("doctor", 3) + 8,
					patientData.indexOf("doctor", 3) + 12)));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoRecordFoundException e) {
			this.doctor = null;
		} catch (MongoDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public int getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(int dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public int getGenderIcon() {
		if (this.gender.equals("male")) {
			return R.drawable.man_icon;
		} else {
			return R.drawable.woman_icon;
		}
	}
	
	public Doctor getDoctor() {
		if (doctor == null) {
			MongoDBTask mongo = new MongoDBTask();
			
			try {
				this.doctor = mongo.getPatientDoctor(this.doctorID);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoRecordFoundException e) {
				this.doctor = null;
			} catch (MongoDBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			return doctor;
		}
		return doctor;
	}

	public void setDoctorID(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getGender() {
		return gender;
	}
	
	public int getAge() {
		return Calendar.getInstance().get(Calendar.YEAR) - this.dateOfBirth;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<Notice> getNotices() {
		if (notices==null) {
			notices = new ArrayList<Notice>();
		}
		return notices;
	}

	public void setNotices(List<Notice> notices) {
		this.notices = notices;
	}
	

	public int getDoctorID() {
		return doctorID;
	}

	public void setDoctorID(int doctorID) {
		this.doctorID = doctorID;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	
}
