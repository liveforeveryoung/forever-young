package com.foreveryoung.models;

import android.util.Log;

import com.foreveryoung.models.parameters.HealthParameter;

public class HealthStatusIcon {
	
	int titleStringResID;
	HealthParameter currentStatus;
	int imageResID;



	/**
	 * @param titleStringResID
	 * @param currentStatus
	 * @param imageResID
	 */
	public HealthStatusIcon(int titleStringResID, HealthParameter currentStatus) {
		super();
		
		this.titleStringResID = titleStringResID;
		this.currentStatus = currentStatus;
		Log.d("Status Icon:",currentStatus.getState().toString());
		this.imageResID = currentStatus.getIconResID();
	}

	/**
	 * @return the titleStringResID
	 */
	public int getTitleStringResID() {
		return titleStringResID;
	}

	/**
	 * @return the currentStatus
	 */
	public HealthParameter getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * @return the imageResID
	 */
	public int getImageResID() {
		return imageResID;
	}

	/**
	 * @param titleStringResID the titleStringResID to set
	 */
	public void setTitleStringResID(int titleStringResID) {
		this.titleStringResID = titleStringResID;
	}

	/**
	 * @param currentStatus the currentStatus to set
	 */
	public void setCurrentStatus(HealthParameter currentStatus) {
		this.currentStatus = currentStatus;
		this.imageResID = currentStatus.getIconResID();
	}
	
	
}
