package com.foreveryoung;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.widget.Toast;

import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.notices.DoctorRecommendation;
import com.foreveryoung.models.notices.Notice;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.models.notices.Notice.NoticeStatus;
import com.foreveryoung.notification.FYNotification;
import com.foreveryoung.notification.FYNotification.NotificationType;
import com.foreveryoung.views.fragments.content.add.AddParameter;
import com.foreveryoung.views.fragments.content.ConnectedAppsList;
import com.foreveryoung.views.fragments.content.DayMap;
import com.foreveryoung.views.fragments.content.DoctorDetails;
import com.foreveryoung.views.fragments.content.GeneralText;
import com.foreveryoung.views.fragments.content.Profile;
import com.foreveryoung.views.fragments.content.Settings;
import com.foreveryoung.views.fragments.content.Notices;
import com.foreveryoung.views.fragments.content.doctor.PatientList;
import com.foreveryoung.views.fragments.main.Blank;
import com.foreveryoung.views.fragments.main.DashboardBar;
import com.foreveryoung.views.fragments.main.TitleBar;
import com.foreveryoung.views.trends.BloodPressureTrend;
import com.foreveryoung.views.trends.ExcerciseTrend;
import com.foreveryoung.views.trends.HeartRateTrend;
import com.foreveryoung.views.trends.SleepActivityTrend;
import com.foreveryoung.views.trends.TrendConditionFrame;
import com.foreveryoung.views.trends.WeightTrend;

public class Navigator {

	protected static final String DrawerLayout = null;
	private static Navigator instance = null;

	public enum Page {
	    DAY_MAP,PRIVACY_POLICY,DISCLAIMER,CONNECTED_APPS,ADD_PARAMETER,SETTINGS, RESTART, PATIENTS, MAKE_RECOMMENDATION, SIGN_OUT,WELCOME,MY_PROFILE,DASHBOARD,LOGIN,ABOUT,BLOODPRESSURE_TREND,HEARTRATE_TREND,SLEEP_TREND,ACTIVITY_TREND,WEIGHT_TREND,DOCTOR 
	}
	
	protected Navigator() {
	      // Exists only to defeat instantiation.
	   }
	   public static Navigator getInstance() {
	      if(instance == null) {
	         instance = new Navigator();
	      }
	      return instance;
	   }
	   
	   public static void loadFragments(Activity FYapp, HashMap<Integer,Fragment> frags) {
		   FragmentTransaction transaction = FYapp.getFragmentManager().beginTransaction();
		   Iterator<Entry<Integer, Fragment>> it = frags.entrySet().iterator();
		    while (it.hasNext()) {
		        Entry<Integer,Fragment> pairs = it.next();
		        if (pairs.getValue()!=null) {
		        	transaction.replace(pairs.getKey(), pairs.getValue());
		        } else {
		        	transaction.replace(pairs.getKey(), new Blank());
		        }
		        it.remove();
		    }
		    transaction.commit();
	   }
	   
	   public static void navigate(Activity application, Page to) {
			
		    TitleBar titleBar = new TitleBar();
		    HashMap<Integer,Fragment> load = new HashMap<Integer,Fragment>();
		    
		    // The areas of the main template.
		    load.put(R.id.titleBar, null);
		    load.put(R.id.content_frame, null);
		    load.put(R.id.content_frame_two, null);
		    load.put(R.id.help, null);
		    
		    switch (to) {
				case MY_PROFILE:
					titleBar.setTitle(R.string.my_profile);
					load.put(R.id.content_frame, new Profile());
					break;
				case DASHBOARD:
					load.put(R.id.titleBar, new DashboardBar());
					Notices notices = new Notices();
					notices.setItems(NoticeStatus.NEW,null,Suggestion.class);
					load.put(R.id.content_frame, notices);
					break;
				case ABOUT:
					Intent i = new Intent(application, ForeverYoungAboutUs.class);
				    application.startActivity(i);
					return;
				case HEARTRATE_TREND:
					titleBar.setTitle(R.string.hr_trend);
					load.put(R.id.content_frame_two, new HeartRateTrend());
					load.put(R.id.content_frame, TrendConditionFrame.newInstance(ParameterType.HEART_RATE));
					break;
				case BLOODPRESSURE_TREND:
					titleBar.setTitle(R.string.bp_trend);
					//titleBar.setStatusIconResID(SessionData.getInstance().getUserHealthData().getBloodPressureList().getLatestValue().getIconResID());
					load.put(R.id.content_frame_two, new BloodPressureTrend());
					load.put(R.id.content_frame, TrendConditionFrame.newInstance(ParameterType.BLOOD_PRESSURE));
					break;
				case SLEEP_TREND:
					titleBar.setTitle(R.string.sleep_trend);
					//titleBar.setStatusIconResID(SessionData.getInstance().getUserHealthData().getSleepList().getLatestValue().getIconResID());
					load.put(R.id.content_frame_two, new SleepActivityTrend());
					load.put(R.id.content_frame, TrendConditionFrame.newInstance(ParameterType.SLEEP));
					break;
				case WEIGHT_TREND:
					titleBar.setTitle(R.string.weight_trend);
					load.put(R.id.content_frame_two, new WeightTrend());
					load.put(R.id.content_frame, TrendConditionFrame.newInstance(ParameterType.WEIGHT));
					break;
				case ACTIVITY_TREND:
					titleBar.setTitle(R.string.activity_trend);
					//titleBar.setStatusIconResID(SessionData.getInstance().getUserHealthData().getExerciseList().getLatestValue().getIconResID());
					load.put(R.id.content_frame_two, new ExcerciseTrend());
					load.put(R.id.content_frame, TrendConditionFrame.newInstance(ParameterType.ACTIVITY));
					break;
				case DOCTOR:
					titleBar.setTitle(R.string.doctors_recommendations);
					load.put(R.id.content_frame, new DoctorDetails());
					Notices docRec = new Notices();
					docRec.setItems(NoticeStatus.NEW,null,DoctorRecommendation.class);
					load.put(R.id.content_frame_two, docRec);
					break;
				case PATIENTS:
					titleBar.setTitle(R.string.patients);
					load.put(R.id.content_frame, new PatientList());
					break;
				case MAKE_RECOMMENDATION:
					titleBar.setTitle(R.string.make_recommendation);
					break;
				case SIGN_OUT:
					restartApp(application);
					return;
				case DISCLAIMER:
					titleBar.setTitle(R.string.legal_disclaimer);
					GeneralText gt = new GeneralText();
					gt.setText(ApplicationData.LEGAL_DISCLAIMER);
					load.put(R.id.content_frame, gt);
					break;
				case PRIVACY_POLICY:
					titleBar.setTitle(R.string.privacy_policy);
					GeneralText gt_pp = new GeneralText();
					gt_pp.setText(ApplicationData.PRIVACY_POLICY);
					load.put(R.id.content_frame, gt_pp);
					break;
				case SETTINGS:
					titleBar.setTitle(R.string.settings);
					load.put(R.id.content_frame, new Settings());
					break;
				case CONNECTED_APPS:
					titleBar.setTitle(R.string.connected_apps);
					load.put(R.id.content_frame, new ConnectedAppsList());
					break;
				case ADD_PARAMETER:
					titleBar.setTitle(R.string.add_parameter);
					load.put(R.id.content_frame, new AddParameter());
					break;
				case DAY_MAP:
					titleBar.setTitle(R.string.day_map);
					load.put(R.id.content_frame, new DayMap());
					break;
			default:
					break;
			}
		  
		    if (load.get(R.id.titleBar)==null) {
		    	load.put(R.id.titleBar, titleBar);
		    }
		    
		    loadFragments(application, load);
			
		}
	   
	   public static void launchApp(Activity FYapp, String packageName) {
		   Intent mIntent = FYapp.getPackageManager().getLaunchIntentForPackage(packageName);
		   if (mIntent != null) {	
		               try {
		                   FYapp.startActivity(mIntent);
		               } catch (ActivityNotFoundException err) {
		                   Toast t = Toast.makeText(FYapp.getApplicationContext(),
		                           R.string.app_not_found, Toast.LENGTH_SHORT);
		                   t.show();
		               }
		   
		           }
		 
		       }
	   public static void restartApp(Activity FYapp) {
		   Intent i = FYapp.getBaseContext().getPackageManager()
		             .getLaunchIntentForPackage( FYapp.getBaseContext().getPackageName() );
		   i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		   FYapp.startActivity(i);
	   }
	   
	   public static Boolean refreshSuggestions(Activity FYapp) {
		   RefreshSuggestions rs = new RefreshSuggestions();
		   try {
			   Boolean complete = rs.execute().get();
			   List<Notice> nl = SessionData.getInstance().getNoticeList();
			   try {
				   ApplicationData.getInstance().addNotificationToRegister(new FYNotification(nl.get(nl.size()-1).getNotice(),NotificationType.SUGGESTION));
			   } catch (NullPointerException np) {};
			   return complete;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   return false;
	   }
	   
	}