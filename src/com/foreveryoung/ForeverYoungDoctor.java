package com.foreveryoung;

import com.foreveryoung.Navigator.Page;
import com.foreveryoung.flyout.DoctorFlyOutBuilder;
import com.foreveryoung.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ForeverYoungDoctor extends ForeverYoung {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (savedInstanceState == null) {
			if (SessionData.getInstance().getUser()==null) {
				Intent i = new Intent(this, ForeverYoungWelcome.class);     
			    startActivity(i);
			} else {
				Navigator.navigate(this, Page.PATIENTS);
				DoctorFlyOutBuilder.getInstance().buildFlyOut(this);
				getActionBar().setDisplayHomeAsUpEnabled(true);
				getActionBar().setHomeButtonEnabled(true);
			}
			loadFlyOut();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_bar_doctor, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (flyOutToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		// Handle action buttons
		switch (item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/* Called whenever we call invalidateOptionsMenu() 
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = flyOutLayout.isDrawerOpen(flyOutContent);
		menu.findItem(R.id.action_add).setVisible(!drawerOpen);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}*/
}
