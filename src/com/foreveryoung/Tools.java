package com.foreveryoung;

import com.foreveryoung.models.parameters.HealthParameter.HealthState;

import android.app.Activity;
import android.util.DisplayMetrics;

public class Tools {
	
	protected Tools() {}
	
	public static int convertDpToPixel(Activity act, float dp) {
        DisplayMetrics metrics = act.getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }
	
	public static int valueOfHealthState(HealthState hs) {
		if (hs == null) {
			return 0;
		}
		switch (hs) {
			case GOOD:
				return 1;
			case MEDIOCRE:
				return 2;
			case BAD:
				return 3;
			default:
				return 0;
			}
	}
	
}
