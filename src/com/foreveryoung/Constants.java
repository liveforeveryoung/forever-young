package com.foreveryoung;

public class Constants {
	
	public static double CONVERSION_LBS_KG = 2.20462262185;
	public static int CONVERSION_CM_M = 100;
	public static int 	
				STEPS_TAKEN_GOOD_LIMIT = 8000,
				STEPS_TAKEN_MED_LIMIT = 5000,
				ACTIVE_TIME_GOOD = 30,
			    ACTIVE_TIME_MED = 10,
			    DEEP_SLEEP_GOOD_LIMIT = 4,
			    DEEP_SLEEP_MED_LIMIT = 2,
			    TOTAL_SLEEP_GOOD_LOW_LIMIT = 6,
			    TOTAL_SLEEP_GOOD_HIGH_LIMIT = 10,
			    TOTAL_SLEEP_MED_LOW_LIMIT = 4,
			    TOTAL_SLEEP_MED_HIGH_LIMIT = 12,
			    TIMES_AWAKE_GOOD_LIMIT = 4,
			    TIMES_AWAKE_MED_LIMIT = 8,
			    BMI_VERY_LOW = 16,
			    BMI_LOW = 19,
			    BMI_GOOD = 25,
			    BMI_HIGH = 31,
				SYSTOLIC_MED_HIGH_LIMIT = 140,
				SYSTOLIC_GOOD_HIGH_LIMIT = 120,
				DIASTOLIC_MED_HIGH_LIMIT = 100,
				DIASTOLIC_GOOD_HIGH_LIMIT = 80,
				SYSTOLIC_GOOD_LOW_LIMIT = 90,
				DIASTOLIC_GOOD_LOW_LIMIT = 60;
	
	// Engine Constants
	public static int 	ENGINE_MOST_LIMIT = 60,
				ENGINE_FEW_LIMIT = 40,
				ENGINE_ALL_LIMIT = 95;
	
	public enum ParameterType {
		BLOOD_PRESSURE,HEART_RATE,SLEEP,ACTIVITY,WEIGHT,NONE
	}

	public static ParameterType[] supportedTypes = { ParameterType.BLOOD_PRESSURE,
											  ParameterType.HEART_RATE,
											  ParameterType.SLEEP,
											  ParameterType.ACTIVITY,
											  ParameterType.WEIGHT 
											 };
		
	
}
