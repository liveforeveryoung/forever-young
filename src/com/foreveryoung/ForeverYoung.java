package com.foreveryoung;

import java.util.List;

import com.foreveryoung.Navigator.Page;
import com.foreveryoung.adapters.FlyOutAdapter;
import com.foreveryoung.models.FlyOutItem;
import com.foreveryoung.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class ForeverYoung extends FYBootstrapActivity {

	protected ProgressBar mProgressBar;

	
	protected  DrawerLayout flyOutLayout;
	protected  RelativeLayout flyOutContent;
	protected RelativeLayout flyOutSettings;
	
	protected  List<FlyOutItem> flyOutItemsList;
	protected  FlyOutAdapter adapter;
	
	protected  ActionBarDrawerToggle flyOutToggle;
	protected CharSequence mDrawerTitle;
	protected CharSequence mTitle;
	private String PREFS_NAME = "FY_Disclaimer";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	       SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	       boolean accepted = settings.getBoolean("accepted", false);
	       if( !accepted ){
	    	   
	    	   new AlertDialog.Builder(this)
	    	    .setTitle("Forever Young Legal Disclaimer")
	    	    .setMessage(Html.fromHtml(ApplicationData.LEGAL_DISCLAIMER))
	    	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	    	        @Override
					public void onClick(DialogInterface dialog, int which) { 
	    	        	SharedPreferences savedSettings = getSharedPreferences(PREFS_NAME, 0);
	    		        SharedPreferences.Editor editor = savedSettings.edit();
	    		        editor.putBoolean("accepted", true);
	    		        editor.commit();
	    	        }
	    	     })
	    	    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
	    	        @Override
					public void onClick(DialogInterface dialog, int which) { 
	    	            System.exit(0);
	    	        }
	    	     })
	    	    .setIcon(android.R.drawable.ic_dialog_alert)
	    	    .show();

	       }
	}
	
	public void loadFlyOut() {
		// enable ActionBar app icon to behave as action to toggle nav drawer

				flyOutLayout = (DrawerLayout) findViewById(R.id.forever_young_main_app);
				flyOutLayout.setDrawerShadow(R.drawable.drawer_shadow,
						GravityCompat.START);
				flyOutLayout.setDrawerShadow(R.drawable.drawer_shadow_end,
						GravityCompat.END);
				
				flyOutContent = (RelativeLayout) findViewById(R.id.flyout);
				
				// ActionBarDrawerToggle ties together the the proper interactions
				// between the sliding drawer and the action bar app icon
				flyOutToggle = new ActionBarDrawerToggle(this, /* host Activity */
				flyOutLayout, /* DrawerLayout object */
				R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description for accessibility */
				R.string.drawer_close /* "close drawer" description for accessibility */
				) {
					
					@Override
					public void onDrawerClosed(View view) {
						invalidateOptionsMenu(); 
					}

					@Override
					public void onDrawerOpened(View drawerView) {
						invalidateOptionsMenu();
					}
					
					public void onDrawerStateChanged() {
						if (flyOutLayout.isDrawerOpen(flyOutSettings)) {
							toggleSettings();
						}
					}
				};
				
				flyOutLayout.setDrawerListener(flyOutToggle);
	}

	
	
	public void toggleSettings() {
		boolean settingsOpen = flyOutLayout.isDrawerOpen(flyOutSettings);
		boolean menuOpen = flyOutLayout.isDrawerOpen(flyOutContent);
		
		if (settingsOpen) {
			flyOutLayout.closeDrawer(Gravity.END);
		} else {
			if (menuOpen) {
				flyOutLayout.closeDrawer(Gravity.START);
			}
			flyOutLayout.openDrawer(Gravity.END);
		}
	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		case R.id.dropdown_settings:
			Navigator.navigate(this, Page.SETTINGS);
			return true;
		case R.id.dropdown_about:
			Navigator.navigate(this, Page.ABOUT);
			return true;
		case R.id.dropdown_disclaimer:
			Navigator.navigate(this, Page.DISCLAIMER);
			return true;
		case R.id.dropdown_privacy_policy:
			Navigator.navigate(this, Page.PRIVACY_POLICY);
			return true;
		case R.id.dropdown_signout:
			Navigator.navigate(this, Page.SIGN_OUT);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	

	/**
	 * On destroy of the application (close) save the current state of the data.
	 */
	@Override
	protected void onDestroy() {
		ApplicationData.getInstance().clearNotifications(this);
		super.onDestroy();
		//this.saveState();
	}
}
