package com.foreveryoung;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.foreveryoung.DataProfiles.PersonType;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.Patient;
import com.foreveryoung.models.User;
import com.foreveryoung.models.parameters.BloodPressure;
import com.foreveryoung.models.parameters.Exercise;
import com.foreveryoung.models.parameters.HealthParameter;
import com.foreveryoung.models.parameters.HeartRate;
import com.foreveryoung.models.parameters.Sleep;
import com.foreveryoung.models.parameters.Weight;
import com.foreveryoung.models.parameters.lists.*;
import com.foreveryoung.models.parameters.lists.HealthParameterList.HealthStateParam;
import com.foreveryoung.models.parameters.lists.HealthParameterList.HealthTrendResult;
import com.foreveryoung.notification.FYNotification;
import com.foreveryoung.notification.FYNotification.NotificationType;

public class HealthData {

	private HealthParameterList bloodPressureList, sleepList, exerciseList, heartRateList, weightList;
	private JSONObject json;
	private Context ctxt;
	
	public HealthData(Context ctxt) {
		this.ctxt = ctxt;
		bloodPressureList = new BloodPressureList();
		sleepList = new SleepList();
		exerciseList = new ExerciseList();
		heartRateList = new HeartRateList();
		weightList = new WeightList();
		// test
	}
	
	public HealthData(JSONObject json) {
		try {
			loadData(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// here I remove weight, since it's almost the same for a period of time...
	public HealthData(BloodPressureList bd, SleepList sp,
			ExerciseList ex, HeartRateList ht, WeightList weight) {

		bloodPressureList = bd;
		sleepList = sp;
		exerciseList = ex;
		heartRateList = ht;
		weightList = weight;
		// test
	}


	public JSONObject getJsonVersion() {
		return json;
	}
	
	/**
	 * This class will load the data from a JSON file.
	 * 
	 * @param fyJSON
	 * @throws ParseException 
	 * @throws JSONException 
	 */
	public void loadData(JSONObject responseJSON) throws ParseException, JSONException {
		
				this.json = responseJSON;

				JSONArray activities = responseJSON.getJSONArray("activities");
				JSONArray sleep = responseJSON.getJSONArray("sleep");
				JSONArray heartBeats = responseJSON.getJSONArray("heartBeats");
				JSONArray bloodPressures = responseJSON.getJSONArray("bloodPressures");
				JSONArray weights = responseJSON.getJSONArray("weight");
				
				bloodPressureList = new BloodPressureList ();
				
				for (int i = 0; i < bloodPressures.length(); i++) {
					JSONObject o = bloodPressures.getJSONObject(i);
					String date = o.getString("date");
					String time = o.getString("time");
					double systolic = o.getDouble("systolic");
					double diastolic = o.getDouble("diastolic");
					
					BloodPressure bp = new BloodPressure((int)systolic, (int)diastolic, new SimpleDateFormat("MM dd, yyyy").parse(date));
					bloodPressureList.add(bp);
				}

				// excercise list
				exerciseList = new ExerciseList();
				
				for (int i = 0; i < activities.length(); i++) {
					JSONObject o = activities.getJSONObject(i);
					double distance = o.getDouble("distance");
					double duration = o.getDouble("duration");
					String date = o.getString("date");
					String startTime = o.getString("startTime");
					double steps = o.getDouble("steps");
					
					Exercise exc = new Exercise((int)steps, (int)duration, new SimpleDateFormat("MM dd, yyyy").parse(date));
					exerciseList.add(exc);
				}
				
				
				// sleep
				sleepList = new SleepList();
				
				for (int i = 0; i < sleep.length(); i++) {
					JSONObject o = sleep.getJSONObject(i);
					double efficiency = o.getDouble("efficiency");
					double minutesAsleep = o.getDouble("minutesAsleep");
					String date = o.getString("date");
					String startTime = o.getString("startTime");
					double awakeningsCount = o.getDouble("awakeningsCount");
					double timeInBed = o.getDouble("timeInBed");

					Sleep sp = new Sleep((int)efficiency, (int)minutesAsleep, (int)awakeningsCount, new SimpleDateFormat("MM dd, yyyy").parse(date));
					sleepList.add(sp);
				}		

				// heart beat
				heartRateList = new HeartRateList();
				
				for (int i = 0; i < heartBeats.length(); i++) {
					JSONObject o = heartBeats.getJSONObject(i);
					double count = o.getDouble("count");
					String date = o.getString("date");
					String time = o.getString("time");

					HeartRate hr = new HeartRate((int)count, new SimpleDateFormat("MM dd, yyyy").parse(date));
					heartRateList.add(hr);
				}
				
				// heart beat
				weightList = new WeightList ();
				
				for (int i = 0; i < weights.length(); i++) {
					JSONObject o = weights.getJSONObject(i);
					double weight = o.getDouble("weightVal");
					String date = o.getString("date");
					String time = o.getString("time");

					Weight we = new Weight(weight, new SimpleDateFormat("MM dd, yyyy").parse(date));
					weightList.add(we);
				}

	}

	private double getRandomNum(double high, double low) {
		return Math.round((low + Math.random() * ((high - low) + 1)) * 100.0) / 100.0;
	}

	public void dummyData(Activity act) {
		
		PersonType type;
		int rand = (int) getRandomNum(0,3);
		
		User usr = SessionData.getInstance().getUser();
		
		if (usr==null) {
			switch(rand) {
			case 0:
				type = PersonType.FIT;
				break;
			case 1:
				type = PersonType.OBESE;
				break;
			case 2:
				type = PersonType.INACTIVE;
				break;
			case 3:
				type = PersonType.STRESSED;
				break;
			default:
				type = PersonType.HIGH_BP;
				break;
			}
		} else {
			FYNotification notification;
			type = PersonType.FIT;
			if  (usr.getEmail().equals("ginny")) {
				type = PersonType.OBESE;
				ApplicationData.getInstance().addNotificationToRegister(new FYNotification("You have not exercised in 10 days.",NotificationType.TREND));
			} else if  (usr.getEmail().equals("april")) {
				ApplicationData.getInstance().addNotificationToRegister(new FYNotification("Your heart rate has not gone above 100 in 20 days.",NotificationType.TREND));
				type = PersonType.INACTIVE;
			} else if (usr.getEmail().equals("steve")) {
				ApplicationData.getInstance().addNotificationToRegister(new FYNotification("Your recent sleep trend is bad.",NotificationType.TREND));
				type = PersonType.STRESSED;
			} else if (usr.getEmail().equals("bob")) {
				ApplicationData.getInstance().addNotificationToRegister(new FYNotification("Blood pressure is high.",NotificationType.SEVERE));
				type = PersonType.HIGH_BP;
			}
			
		}
		
		Log.d("HEALTH DATA","Generating data for type: "+type.toString());
		GenerateData data = new GenerateData();
		
		Log.d("HEALTH DATA","Launching dialog box.");
		//ctxt.launchProgressDialog("Generating Data");
		Log.d("HEALTH DATA","Dialog opened.");
		try {
			Log.d("HEALTH DATA","Launching generate data thread.");
			data.execute(type).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//ctxt.closeDialog();
	}
	
	public HealthParameterList getList(ParameterType type) {
		switch(type) {
			case BLOOD_PRESSURE:
				return bloodPressureList;
			case SLEEP:
				return sleepList;
			case HEART_RATE:
				return heartRateList;
			case ACTIVITY:
				return exerciseList;
			case WEIGHT:
				return weightList;
			default:
				return null;
		}
	}
	
	public HealthParameterList getListByDate(Date thisDate, HealthParameterList list) {
		HealthParameterList listFiltered = new HealthParameterList();
		for (HealthParameter hp : list) {
			if (hp.getTimestamp().getDate()==thisDate.getDate())
			listFiltered.add(hp);
		}
		return listFiltered;
	}
	
	public HealthParameterList getListByDate(Date thisDate) {
		
		HealthParameterList list = new HealthParameterList();
		list.addAll(getListByDate(thisDate,bloodPressureList));
		list.addAll(getListByDate(thisDate,heartRateList));
		list.addAll(getListByDate(thisDate,sleepList));
		list.addAll(getListByDate(thisDate,weightList));
		list.addAll(getListByDate(thisDate,exerciseList));
		return list;
		
	}

	/**
	 * @param bloodPressureList
	 *            the bloodPressureList to set
	 */
	public void setBloodPressureList(BloodPressureList bloodPressureList) {
		this.bloodPressureList = bloodPressureList;
	}

	/**
	 * @param sleepList
	 *            the sleepList to set
	 */
	public void setSleepList(SleepList sleepList) {
		this.sleepList = sleepList;
	}

	/**
	 * @param exerciseList
	 *            the exerciseList to set
	 */
	public void setExerciseList(ExerciseList exerciseList) {
		this.exerciseList = exerciseList;
	}

	/**
	 * @param heartRateList
	 *            the heartRateList to set
	 */
	public void setHeartRateList(HeartRateList heartRateList) {
		this.heartRateList = heartRateList;
	}

	/**
	 * @param weightList
	 *            the weightList to set
	 */
	public void setWeightList(WeightList weightList) {
		this.weightList = weightList;
	}
	
	class ParseHealthData extends AsyncTask<HealthParameterList, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(HealthParameterList... data) {
			
			for (HealthParameterList hl : data) {
				
				Double[] percentages = getPercentages(hl);
				int count = 0;
				
				HealthStateParam[] currentState = new HealthStateParam[3];

				for (Double val : percentages) {
					currentState[count] = analyzePercentage(val);
					count++;
				}
				
				hl.setCurrentState(currentState);
				
			}
			return true;

		}
		
		private Double[] getPercentages(HealthParameterList hl) {
			Log.d("PARSE DATA","Getting percentages.");
			double countGOOD = 0, countMED = 0, countBAD = 0;
			for (HealthParameter param : hl) {
				switch(param.getState()) {
					case GOOD:
						countGOOD++;
						break;
					case MEDIOCRE:
						countMED++;
						break;
					case BAD:
						countBAD++;
						break;
					default:
						break;
				}
			}
			
			Log.d("PARSE DATA","Good: "+countGOOD+" MED"+countMED+" BAD"+countBAD);
			
			double totalReadings = hl.size();
			Double[] percentages = new Double[3];
			percentages[0] = 100*(countGOOD / totalReadings);
		    percentages[1] = 100*(countMED / totalReadings);
		    percentages[2] = 100*(countBAD / totalReadings);
		    
		    Log.d("PARSE DATA","% Good: "+percentages[0]+" MED"+percentages[1]+" BAD"+percentages[2]);
		    
		    return percentages;
		    
		}

		private HealthStateParam analyzePercentage(Double percentage) {
			
			if (percentage >= Constants.ENGINE_ALL_LIMIT) {
				return HealthStateParam.ALL;
			} else if (percentage >= Constants.ENGINE_MOST_LIMIT) {
				return HealthStateParam.MOST;
			} else if (percentage <= Constants.ENGINE_MOST_LIMIT || percentage >= Constants.ENGINE_FEW_LIMIT) {
				return HealthStateParam.HALF;
			} else if (percentage <= Constants.ENGINE_FEW_LIMIT) {
				return HealthStateParam.FEW;
			} else if (percentage == 0) {
				return HealthStateParam.NONE;
			}
			return null;
		}

	}
	
	class GenerateData extends AsyncTask<PersonType, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(PersonType... type) {
			Log.d("HEALTH DATA","Generate data thread running.");
			generateRandomData(type[0]);
			return true;

		}
		
		public void generateRandomData(PersonType type) {
			
			String dt = "2014-03-01";  // Start date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			
			try {
				c.setTime(sdf.parse(dt));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			for (int i = 0; i<=50; i++) {
				c.add(Calendar.DATE, 1);  // number of days to add
				dt = sdf.format(c.getTime());  // dt is now the new date
				String[] bits = dt.split("-");
				generateRandomDataPoint(((Patient)SessionData.getInstance().getUser()).getWeight(),new Date(Integer.valueOf(bits[0]),
						Integer.valueOf(bits[1]),Integer.valueOf(bits[2])), type);
			}
	
		}
		
		public Boolean analyze() {
			Log.d(this.getClass().toString()+" List","Instantiating the parser.");
			ParseHealthData parser = new ParseHealthData();
			HealthParameterList[] lists = { bloodPressureList, heartRateList, sleepList, weightList, exerciseList };
			try {
				Log.d(this.getClass().toString()+" List","Parsing data...");
			    return parser.execute(lists).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		public void generateRandomDataPoint(double currentWeight, Date day, PersonType type) {
			
			DataProfiles profile = new DataProfiles(type);
			
			double systolic = getRandomNum(profile.systolic_high,profile.systolic_low), 
				diastolic = getRandomNum(profile.diastolic_high,profile.diastolic_low), 
				sleepTotal = getRandomNum(profile.sleep_high,profile.sleep_low), 
				deepSleep = getRandomNum(sleepTotal,profile.deep_sleep_low), 
				timesAwake = getRandomNum(profile.times_awake_high,profile.times_awake_low), 
				weight = getRandomNum(currentWeight+profile.weight_delta_high,currentWeight-profile.weight_delta_low), 
				stepsTaken = getRandomNum(profile.steps_taken_high,profile.steps_taken_low),
				highlyActiveTime = getRandomNum(profile.active_time_high,profile.active_time_low);

			for (int i = 0; i<10; i++) {
				heartRateList.add(new HeartRate((int)getRandomNum(profile.resting_heart_rate_high,profile.resting_heart_rate_low), day));
			}
			
			for (int i = 0; i<10; i++) {
				heartRateList.add(new HeartRate((int)getRandomNum(profile.exercise_heart_rate_high,profile.exercise_heart_rate_low), day));
			}

			bloodPressureList.add(new BloodPressure((int)systolic, (int)diastolic, day));
			sleepList.add(new Sleep(sleepTotal, deepSleep, (int)timesAwake, day));
			exerciseList.add(new Exercise((int)stepsTaken, highlyActiveTime, day));
			weightList.add(new Weight(weight, day));
			
			
		}
		
		@Override
		public void onPostExecute(Boolean result) {
	
			Log.d("HEALTH DATA","Data generated. Analyzing the data.");
			analyze();
			Log.d("HEALTH DATA","Analyzing Blood Pressure..");
			
			HealthTrendResult rs;
			rs = bloodPressureList.analyzeState();
			Log.d("HEALTH DATA","Blood Pressure: "+rs.toString());
			rs = sleepList.analyzeState();
			Log.d("HEALTH DATA","Sleep: "+rs.toString());
			rs = exerciseList.analyzeState();
			Log.d("HEALTH DATA","Exercise: "+rs.toString());
			rs = weightList.analyzeState();
			Log.d("HEALTH DATA","Weight: "+rs.toString());
			rs = heartRateList.analyzeState();
			Log.d("HEALTH DATA","HeartRate: "+rs.toString());
			
			super.onPostExecute(result);
		}
	}
}
