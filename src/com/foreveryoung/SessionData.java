package com.foreveryoung;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.util.Log;

import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.Patient;
import com.foreveryoung.models.notices.DoctorRecommendation;
import com.foreveryoung.models.notices.Notice;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.models.notices.Notice.NoticeStatus;
import com.foreveryoung.models.User;
import com.foreveryoung.notification.FYNotification;
import com.foreveryoung.notification.FYNotification.NotificationType;

/**
 * Application singleton holds all of the data that our app will use.
 * @author jeremywallace
 *
 */
public class SessionData {

	private static SessionData instance = null;
	private User thisUser;
	private HealthData userHealthData;
	private int hrMonth;
	private int hrYear;
	
	private int bpMonth;
	private int bpYear;
	private int slMonth;
	private int slYear;
	private int acMonth;
	private int acYear;
	private int wgMonth;
	private int wgYear;
//	
	protected SessionData() {
		Calendar today = Calendar.getInstance();
		setHrMonth(today.get(Calendar.MONTH));
		setHrYear(today.get(Calendar.YEAR));
		setBpMonth(today.get(Calendar.MONTH));
		setBpYear(today.get(Calendar.YEAR));
		setSlMonth(today.get(Calendar.MONTH));
		setSlYear(today.get(Calendar.YEAR));
		setAcMonth(today.get(Calendar.MONTH));
		setAcYear(today.get(Calendar.YEAR));
		setWgMonth(today.get(Calendar.MONTH));
		setWgYear(today.get(Calendar.YEAR));		
	}
	
	public void loadDummyData(Activity FYapp) {
		//ApplicationData.getInstance().setUser(new User("Dummy Data",1984,45.5,43.2));
		userHealthData = new HealthData(FYapp);
		this.userHealthData.dummyData(FYapp);
		Log.d("SESSION","Dummy data loaded.");

		Patient user = ((Patient)thisUser);
		user.getNotices().add(new DoctorRecommendation("Take 50 mg of magnesium nightly.",user.getDoctor().getFullName()));
		user.getNotices().add(new DoctorRecommendation("Take 2000 mg of Omega-3 in the morning.",user.getDoctor().getFullName()));
		
		ApplicationData.getInstance().addNotificationToRegister(new FYNotification("Take 2000 mg of Omega-3 in the morning.",NotificationType.DOCTOR));
    
	}
	
	public static SessionData getInstance() {
	      if(instance == null) {
	         instance = new SessionData();
	      }

	      return instance;
	   }
	
	public HealthData getUserHealthData() {
		return userHealthData;
	}

	public List<Notice> getNotices(NoticeStatus status, Class<?> category, ParameterType type) {
		List<Notice> totalList = new ArrayList<Notice>();
		
			for (Notice s: ((Patient)thisUser).getNotices()) {
				if ((status==null||s.getStatus()==status)&&(category==null||category==s.getClass())) {
					if (s.getClass()==Suggestion.class) {
						if (type==null||((Suggestion)s).getSuggestionType()==type) {
							totalList.add(s);
						}
					} else {
						totalList.add(s);
					}
				}
		}

		return totalList;
		
	}
	
	public List<Notice> getNoticeList() {
		return ((Patient)thisUser).getNotices();
	}
	
	private void setUserHealthData(HealthData userHealthData) {
		this.userHealthData = userHealthData;
	}

	public void setUser(User newUser) {
		thisUser = newUser;
	}

	public User getUser() {
		return thisUser;
	}
	
	/**
	 * Parses an incoming JSON string and populates the UserData object.
	 * @param json
	 */
	public void parseJSON(String json) {
		// Parse a JSON string and create a user object.
		
//		// get user id (email)
//		String email = thisUser.getEmail();
//		String collection = "Users";
//		
//		DBConnector conn = new DBConnector("foreveryoung");
//		DB db = conn.getDB();
//		
//		DBOperation dbo = new DBOperation();
//		String healthDataJson = dbo.findRecord(db, email, collection);
//	
//		
//		// Create new user with parsed data
//		//this.thisUser = new User("My User",1984,80,80);
//		
//		// Parse the file to find the encapsulated object with health data.
//		
//		
//		//String healthDataJson = "";
//		this.userHealthData.LoadData(healthDataJson);
		
	}
	
	/**
	 * This method builds a JSON object from the UserData and returns it.
	 * @return UserData in JSON Format.
	 */
	public String getJSON() {
		// Generate JSON file from current data.
		return "";
	}
	

	public int getHrMonth() {
		return hrMonth;
	}

	public void setHrMonth(int hrMonth) {
		this.hrMonth = hrMonth;
	}

	public int getHrYear() {
		return hrYear;
	}

	public void setHrYear(int hrYear) {
		this.hrYear = hrYear;
	}
	public int getBpMonth() {
		return bpMonth;
	}

	public void setBpMonth(int bpMonth) {
		this.bpMonth = bpMonth;
	}

	public int getBpYear() {
		return bpYear;
	}

	public void setBpYear(int bpYear) {
		this.bpYear = bpYear;
	}

	public int getSlMonth() {
		return slMonth;
	}

	public void setSlMonth(int slMonth) {
		this.slMonth = slMonth;
	}

	public int getSlYear() {
		return slYear;
	}

	public void setSlYear(int slYear) {
		this.slYear = slYear;
	}

	public int getAcMonth() {
		return acMonth;
	}

	public void setAcMonth(int acMonth) {
		this.acMonth = acMonth;
	}

	public int getAcYear() {
		return acYear;
	}

	public void setAcYear(int acYear) {
		this.acYear = acYear;
	}

	public int getWgMonth() {
		return wgMonth;
	}

	public void setWgMonth(int wgMonth) {
		this.wgMonth = wgMonth;
	}

	public int getWgYear() {
		return wgYear;
	}

	public void setWgYear(int wgYear) {
		this.wgYear = wgYear;
	}
	
	

}
