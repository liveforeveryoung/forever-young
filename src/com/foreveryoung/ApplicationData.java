package com.foreveryoung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;

import com.foreveryoung.connect.ConnectedApp;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.models.parameters.lists.HealthParameterList.HealthTrendResult;
import com.foreveryoung.mongodb.MongoDBException;
import com.foreveryoung.mongodb.MongoDBTask;
import com.foreveryoung.mongodb.NoRecordFoundException;
import com.foreveryoung.notification.FYNotification;
import com.foreveryoung.notification.NotificationBuilder;

public class ApplicationData {

	List<Suggestion> allSuggestions;
	List<ConnectedApp> connectedApps;
	private List<FYNotification> notificationRegister;
	private HashMap<Integer,Boolean> notificationsSent;

	public static HashMap<HealthTrendResult,Integer[]> healthTrendTextResIDs;

	static String LEGAL_DISCLAIMER = "Forever Young provides suggestions for the user to improve their overall health. These suggestions are in no way meant to be medical advice and any personal health action taken should be consulted with a doctor before engaging. Below is the legal disclaimer for use of this software. <br /><br /> <h2>1.	Credit</h2>1.1	This document was created using a template from SEQ Legal (http://www.seqlegal.com).<br><br /><h2>2.	No advice</h2>2.1	Our application contains general medical information.<br />2.2	The medical information is not advice and should not be treated as such.<br><br /><h2>3.	No warranties</h2>3.1	The medical information on our application is provided without any representations or warranties, express or implied.<br />3.2	Without limiting the scope of Section 3.1, we do not warrant or represent that the medical information on this application:<br />(a)	will be constantly available, or available at all; or<br />(b)	is true, accurate, complete, current or non-misleading.<br><br /><h2>4.	Medical assistance</h2>4.1	You must not rely on the information on our application as an alternative to medical advice from your doctor or other professional healthcare provider.<br />4.2	If you have any specific questions about any medical matter, you should consult your doctor or other professional healthcare provider.<br />4.3	If you think you may be suffering from any medical condition, you should seek immediate medical attention.<br />4.4	You should never delay seeking medical advice, disregard medical advice or discontinue medical treatment because of information on our application.<br><br /><h2>5.	Interactive features</h2>5.1	Our application includes interactive features that allow users to communicate with us.<br />5.2	You acknowledge that, because of the limited nature of communication through our application's interactive features, any assistance you may receive using any such features is likely to be incomplete and may even be misleading.<br />5.3	Any assistance you may receive using any our application's interactive features does not constitute specific advice and accordingly should not be relied upon without further independent confirmation.<br />6.	Limits upon exclusions of liability<br><br /><h2>6.1	Nothing in this disclaimer will:</h2>(a)	limit or exclude any liability for death or personal injury resulting from negligence;<br />(b)	limit or exclude any liability for fraud or fraudulent misrepresentation;<br />(c)	limit any liabilities in any way that is not permitted under applicable law; or<br />(d)	exclude any liabilities that may not be excluded under applicable law.";	
	static String PRIVACY_POLICY = "<p>In order to receive information about your Personal Data, the  purposes and the parties the Data is shared with, contact the Owner.</p></div><div><h2>Data Controller and Owner</h2></div><div><h2>Types of Data collected</h2><p>The owner does not provide a list of Personal Data types collected.</p><p>Other Personal Data collected may be described in other sections of  this privacy policy or by dedicated explanation text contextually with  the Data collection.<br>The Personal Data may be freely provided by the User, or collected automatically when using this Application.<br>Any  use of Cookies - or of other tracking tools - by this Application or by  the owners of third party services used by this Application, unless  stated otherwise, serves to identify Users and remember their  preferences, for the sole purpose of providing the service required by  the User.<br>Failure to provide certain Personal Data may make it impossible for this Application to provide its services.</p><p>The User assumes responsibility for the Personal Data of third  parties published or shared through this Application and declares to  have the right to communicate or broadcast them, thus relieving the Data  Controller of all responsibility.</p></div><div><h2>Mode and place of processing the Data</h2><h3>Method of processing</h3><p>The Data Controller processes the Data of Users in proper manner and  shall take appropriate security measures to prevent unauthorized access,  disclosure, modification or unauthorized destruction of the Data.<br>The  Data processing is carried out using computers and/or IT enabled tools,  following organizational procedures and modes strictly related to the  purposes indicated. In addition to the Data Controller, in some cases,  the Data may be accessible to certain types of persons in charge,  involved with the operation of the site (administration, sales,  marketing, legal, system administration) or external parties (such as  third party technical service providers, mail carriers, hosting  providers, IT companies, communications agencies) appointed, if  necessary, as Data Processors by the Owner. The updated list of these  parties may be requested from the Data Controller at any time.</p><h3>Place</h3><p>The Data is processed at the Data Controller's operating offices and  in any other places where the parties involved with the processing are  located. For further information, please contact the Data Controller.</p><h3>Conservation Time</h3><p>The Data is kept for the time necessary to provide the service  requested by the User, or stated by the purposes outlined in this  document, and the User can always request the Data Controller for their  suspension or removal.</p></div><div><h2>Additional information about Data collection and processing</h2><h3>Legal Action</h3><p> The User's Personal Data may be used for legal purposes by the Data  Controller, in Court or in the stages leading to possible legal action  arising from improper use of this Application or the related services. </p><h3>Additional Information about User's Personal Data</h3><p> In addition to the information in this privacy policy, this Application  may provide the User with contextual information concerning particular  services or the collection and processing of Personal Data. </p><h3>System Logs and Maintenance</h3><p> For operation and maintenance purposes, this Application and any third  party services may collect files that record interaction with this  Application (System Logs) or use for this purpose other Personal Data  (such as IP Address). </p><h3>Information not contained in this policy</h3><p> More details concerning the collection or processing of Personal Data  may be requested from the Data Controller at any time at its contact  information. </p><h3>The rights of Users</h3><p> Users have the right, at any time, to know whether their Personal Data  has been stored and can consult the Data Controller to learn about their  contents and origin, to verify their accuracy or to ask for them to be  supplemented, cancelled, updated or corrected, or for their  transformation into anonymous format or to block any data held in  violation of the law, as well as to oppose their treatment for any and  all legitimate reasons. Requests should be sent to the Data Controller  at the contact information set out above. </p><p> This Application does not support &ldquo;do not track&rdquo; requests.<br>To understand if any of the third party services it uses honor the &ldquo;do not track&rdquo; requests, please read their privacy policies. </p><h3>Changes to this privacy policy</h3><p> The Data Controller reserves the right to make changes to this privacy  policy at any time by giving notice to its Users on this page. It is  strongly recommended to check this page often, referring to the date of  the last modification listed at the bottom. If a User objects to any of  the changes to the Policy, the User must cease using this Application  and can request the Data Controller to erase the Personal Data. Unless  stated otherwise, the then-current privacy policy applies to all  Personal Data the Data Controller has about Users. </p><h3>Information about this privacy policy</h3><p> The Data Controller is responsible for this privacy policy, prepared  starting from the modules provided by Iubenda and hosted on the  Iubenda's servers. </p></div>";
	private static ApplicationData instance;
	private MongoDBTask mongo;
	
	protected ApplicationData() {
		connectedApps = new ArrayList<ConnectedApp>();
		notificationRegister = new ArrayList<FYNotification>();
		notificationsSent = new HashMap<Integer,Boolean>();
		loadTrendMap();
		try {
			mongo = new MongoDBTask();
			allSuggestions = mongo.getSuggestions();
		} catch (NoRecordFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ApplicationData getInstance() {
		if (instance==null) {
			instance = new ApplicationData();
		}
		return instance;
	}
	
	public List<Suggestion> getAllSuggestions() {
		return allSuggestions;
	}

	public void setAllSuggestions(List<Suggestion> allSuggestions) {
		this.allSuggestions = allSuggestions;
	}

	public List<ConnectedApp> getConnectedApps() {
		return connectedApps;
	}

	public void setConnectedApps(List<ConnectedApp> connectedApps) {
		this.connectedApps = connectedApps;
	}

	public List<FYNotification> getNotificationRegister() {
		return notificationRegister;
	}

	public void setNotificationRegister(List<FYNotification> notificationRegister) {
		this.notificationRegister = notificationRegister;
	}

	public void addNotificationToRegister(FYNotification fyNot) {
		notificationRegister.add(fyNot);
		notificationsSent.put(fyNot.getId(),false);
	}
	public void clearNotifications(Context ctx) {
		String ns = Context.NOTIFICATION_SERVICE;
	    NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
		for (FYNotification fyNot : notificationRegister) {
			nMgr.cancel(fyNot.getId());
		}
		
		this.notificationRegister.clear();
	}
	
	public void sendNotifications(Activity act) {
		for (FYNotification fyNot : notificationRegister) {
			if (!this.notificationsSent.get(fyNot.getId())) {
				new NotificationBuilder().sendMessage(fyNot, act);
				this.notificationsSent.put(fyNot.getId(),true);
			}
		}
		
	}
	
	private void loadTrendMap() {
		healthTrendTextResIDs = new HashMap<HealthTrendResult,Integer[]>();
    	healthTrendTextResIDs.put(HealthTrendResult.ALL_GOOD, new Integer[]{ R.string.ALL_GOOD, R.string.ALL_GOOD_DESC, R.color.trend_green });
    	healthTrendTextResIDs.put(HealthTrendResult.ALL_BAD, new Integer[]{ R.string.ALL_BAD, R.string.ALL_BAD_DESC, R.color.trend_red });
    	healthTrendTextResIDs.put(HealthTrendResult.ALL_MED, new Integer[]{ R.string.ALL_MED, R.string.ALL_MED_DESC, R.color.trend_orange});
    	healthTrendTextResIDs.put(HealthTrendResult.HALF_GOOD_HALF_BAD, new Integer[]{ R.string.HALF_GOOD_HALF_BAD, R.string.HALF_GOOD_HALF_BAD_DESC, R.color.trend_red });
    	healthTrendTextResIDs.put(HealthTrendResult.HALF_MED_HALF_BAD, new Integer[]{ R.string.HALF_MED_HALF_BAD, R.string.HALF_MED_HALF_BAD_DESC, R.color.trend_red});
    	healthTrendTextResIDs.put(HealthTrendResult.HALF_GOOD_HALF_MED, new Integer[]{ R.string.HALF_GOOD_HALF_MED, R.string.HALF_GOOD_HALF_MED_DESC, R.color.trend_orange });
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_GOOD_FEW_BAD, new Integer[]{ R.string.MOST_GOOD_FEW_BAD, R.string.MOST_GOOD_FEW_BAD_DESC, R.color.trend_orange });
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_GOOD_NO_BAD, new Integer[]{ R.string.MOST_GOOD_NO_BAD, R.string.MOST_GOOD_NO_BAD_DESC, R.color.trend_green });
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_BAD_FEW_GOOD, new Integer[]{ R.string.MOST_BAD_FEW_GOOD, R.string.MOST_BAD_FEW_GOOD_DESC, R.color.trend_red });
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_BAD_NO_GOOD, new Integer[]{ R.string.MOST_BAD_NO_GOOD, R.string.MOST_BAD_NO_GOOD_DESC, R.color.trend_red});
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_MED_FEW_BAD, new Integer[]{ R.string.MOST_MED_FEW_BAD, R.string.MOST_MED_FEW_BAD_DESC, R.color.trend_orange });
    	healthTrendTextResIDs.put(HealthTrendResult.MOST_MED_NO_BAD, new Integer[]{ R.string.MOST_MED_NO_BAD, R.string.MOST_MED_NO_BAD_DESC, R.color.trend_orange });
    	healthTrendTextResIDs.put(HealthTrendResult.ALL_LOW_VALUES, new Integer[]{ R.string.ALL_LOW_VALUES, R.string.ALL_LOW_VALUES_DESC, R.color.trend_orange });
    	healthTrendTextResIDs.put(HealthTrendResult.ALL_HIGH_VALUES, new Integer[]{ R.string.ALL_HIGH_VALUES, R.string.ALL_HIGH_VALUES_DESC, R.color.trend_red });


	}
}
