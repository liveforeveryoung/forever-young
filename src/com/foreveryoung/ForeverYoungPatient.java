package com.foreveryoung;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.foreveryoung.Navigator.Page;
import com.foreveryoung.flyout.PatientFlyOutBuilder;
import com.foreveryoung.notification.FYNotification;
import com.foreveryoung.notification.FYNotification.NotificationType;

public class ForeverYoungPatient extends ForeverYoung {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (savedInstanceState == null) {
			if (SessionData.getInstance().getUser()==null) {
				Intent i = new Intent(this, ForeverYoungWelcome.class);     
			    startActivity(i);
			} else {
				
				ApplicationData.getInstance().addNotificationToRegister(new FYNotification("",NotificationType.BASIC));

				Navigator.navigate(this, Page.DASHBOARD);
				PatientFlyOutBuilder.getInstance().buildFlyOut(this);
				getActionBar().setDisplayHomeAsUpEnabled(true);
				getActionBar().setHomeButtonEnabled(true);
				
				ApplicationData.getInstance().sendNotifications(this);
			}
			loadFlyOut();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_bar, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (flyOutToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_add:
			Navigator.navigate(this, Page.ADD_PARAMETER);	
			return true;
		case R.id.dropdown_connected_apps:
			Navigator.navigate(this, Page.CONNECTED_APPS);	
			return true;
		case R.id.action_dashboard:
			Navigator.navigate(this, Page.DASHBOARD);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	
}
