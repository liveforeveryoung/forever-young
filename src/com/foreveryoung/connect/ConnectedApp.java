package com.foreveryoung.connect;

import com.foreveryoung.R;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;

public class ConnectedApp {

	Drawable icon;
	String appName;
	String packageName;
	ConnectAppStatus connectedStatus;
	AppInstallStatus installStatus;
	
	public enum AppInstallStatus {
		NOT_INSTALLED, INSTALLED
	}
	
	public enum ConnectAppStatus {
		CONNECTED, DISCONNECTED, BROKEN
	}
	
	public ConnectedApp(Activity act, String packageName, String appName)  {
		
		this.packageName = packageName;
		this.appName = appName;
		
		try {
			PackageManager pm = act.getPackageManager();
			ApplicationInfo ai = pm.getApplicationInfo( this.getPackageName(), 0);
			
			icon = pm.getApplicationIcon(ai);
			this.appName = (String) pm.getApplicationLabel(ai);
			this.installStatus = AppInstallStatus.INSTALLED;
			
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.installStatus = AppInstallStatus.NOT_INSTALLED;
		}
		
		this.connectedStatus = ConnectAppStatus.DISCONNECTED;

	}

	public Drawable getIcon() {
		return icon;
	}

	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public AppInstallStatus getInstallStatus() {
		return installStatus;
	}
	
	public ConnectAppStatus getConnectedStatus() {
		return connectedStatus;
	}

	public void setConnectAppStatus(ConnectAppStatus status) {
		this.connectedStatus = status;
	}
	
	public int getConnectedBtnTextResID() {
		switch (connectedStatus) {
		case DISCONNECTED:
			return R.string.btn_connect;
		case CONNECTED:
			return R.string.btn_disconnect;
		default:
			return 0;
		}
	}
	
	public int getConnectedIconResID() {
		switch (connectedStatus) {
		case DISCONNECTED:
			return R.drawable.disconnected;
		case CONNECTED:
			return R.drawable.connected;
		default:
			return 0;
		}
	}
	
}
