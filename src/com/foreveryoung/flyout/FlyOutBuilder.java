package com.foreveryoung.flyout;

import java.util.ArrayList;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.foreveryoung.Navigator;
import com.foreveryoung.R;
import com.foreveryoung.Navigator.Page;
import com.foreveryoung.adapters.FlyOutAdapter;
import com.foreveryoung.models.FlyOutItem;
import com.foreveryoung.views.fragments.flyout.AccountDetails;
import com.foreveryoung.views.fragments.flyout.SectionTitle;

public abstract class FlyOutBuilder{

	protected ArrayList<FlyOutItem> flyOutMenuList;
	protected ListView flyOutMenu;
	protected LinearLayout flyOutButtonsView;
	protected static FlyOutBuilder instance = null;
	protected FlyOutAdapter adapter;
	protected Activity application;
	protected DrawerLayout flyOutLayout;
	
	protected FlyOutBuilder() {
	      // Exists only to defeat instantiation.
	   }
	
	public void buildFlyOut(Activity application) {
		
		this.application = application;
		
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_account, new AccountDetails()).commit();
				
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_menu_title, new SectionTitle("MAIN MENU")).commit();
		
		
		
	}
	
	protected void addOnClick(View thisView, final Page to) {
		thisView.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v)
            {
            	flyOutLayout = (DrawerLayout) application.findViewById(R.id.forever_young_main_app);
                Navigator.navigate(application,to);
                flyOutLayout.closeDrawers();
                
            } 
        });
	}

	class MenuItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }
	
	/**
     * Swaps fragments in the main content view
     */
    protected abstract void selectItem(int position);
}
