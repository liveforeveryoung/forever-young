package com.foreveryoung.flyout;

import java.util.ArrayList;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.foreveryoung.Navigator;
import com.foreveryoung.R;
import com.foreveryoung.Navigator.Page;
import com.foreveryoung.adapters.FlyOutAdapter;
import com.foreveryoung.models.FlyOutItem;
import com.foreveryoung.views.fragments.flyout.AccountDetails;
import com.foreveryoung.views.fragments.flyout.SectionTitle;

public class PatientFlyOutBuilder extends FlyOutBuilder{

	protected ArrayList<FlyOutItem> flyOutMenuList;
	protected ListView flyOutMenu;
	protected LinearLayout flyOutButtonsView;
	protected static FlyOutBuilder instance = null;
	protected FlyOutAdapter adapter;
	protected Activity application;
	protected DrawerLayout flyOutLayout;
	
	protected PatientFlyOutBuilder() {
	      // Exists only to defeat instantiation.
	   }
	
	public static PatientFlyOutBuilder getInstance() {
	      if(instance == null) {
	         instance = new PatientFlyOutBuilder();
	      }
	      return (PatientFlyOutBuilder) instance;
	   }
	
	@Override
	public void buildFlyOut(Activity application) {
		
		this.application = application;
		
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_account, new AccountDetails()).commit();
		
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_health_title, new SectionTitle("HEALTH TRENDS")).commit();
		
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_menu_title, new SectionTitle("MAIN MENU")).commit();
		
		flyOutMenuList = new ArrayList<FlyOutItem>();
		
		flyOutMenuList.add(new FlyOutItem(R.string.btn_my_profile,R.drawable.menu_item_arrow, R.drawable.bg_white_20, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_my_day_map,R.drawable.menu_item_arrow, R.drawable.bg_white_10, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_doctors_rec, R.drawable.menu_item_arrow, R.drawable.bg_white_20, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_refresh_suggestions, R.drawable.menu_item_arrow, R.drawable.bg_white_20, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_call_doctor, R.drawable.menu_item_arrow, R.drawable.bg_medeo, R.drawable.medeo_icon));
		

		flyOutMenu = (ListView) application.findViewById(R.id.flyout_menu);

		adapter = new FlyOutAdapter(application, R.layout.flyout_list_item,
                flyOutMenuList);
		
		flyOutMenu.setAdapter(adapter);

		flyOutMenu.setOnItemClickListener(new MenuItemClickListener());
		
		flyOutButtonsView = (LinearLayout) application.findViewById(R.id.health_trends_linear);
		
		flyOutButtonsView.addView(getButtonImageView(R.drawable.bloodpressure_flyout,Page.BLOODPRESSURE_TREND));
		flyOutButtonsView.addView(getButtonImageView(R.drawable.heartrate_flyout,Page.HEARTRATE_TREND));
		flyOutButtonsView.addView(getButtonImageView(R.drawable.sleep_flyout_icon,Page.SLEEP_TREND));
		flyOutButtonsView.addView(getButtonImageView(R.drawable.activity_flyout_icon,Page.ACTIVITY_TREND));
		flyOutButtonsView.addView(getButtonImageView(R.drawable.weight_flyout_icon,Page.WEIGHT_TREND));
		
		//addOnClick(suggestionsButton,Page.DASHBOARD);
		
	}
	
	@Override
	protected void addOnClick(View thisView, final Page to) {
		thisView.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v)
            {
            	flyOutLayout = (DrawerLayout) application.findViewById(R.id.forever_young_main_app);
                Navigator.navigate(application,to);
                flyOutLayout.closeDrawers();
                
            } 
        });
	}
	
	private ImageView getButtonImageView(int resource, final Page to) {
		ImageView button = new ImageView(application.getBaseContext());
		button.setImageResource(resource);
		
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    lp.setMargins(20, 20, 20, 10);
		
	    button.setLayoutParams(lp); 
        button.getLayoutParams().height = 120;
        button.getLayoutParams().width = 120;
        
        addOnClick(button,to);
        
		return button;
	}

	class MenuItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }
	
	/**
     * Swaps fragments in the main content view
     */
    @Override
	protected void selectItem(int position) {
        // Toast.makeText(application, R.string.app_name, Toast.LENGTH_SHORT).show();
        // Highlight the selected item, update the title, and close the drawer
        
    	flyOutLayout = (DrawerLayout) application.findViewById(R.id.forever_young_main_app);

        switch (position) {
	        case 0:
	        	Navigator.navigate(application, Page.MY_PROFILE);
	        	break;
	        case 1:
	        	Navigator.navigate(application, Page.DAY_MAP);
	        	break;
	        case 2:
	        	Navigator.navigate(application, Page.DOCTOR);
	        	break;
	        case 3:
	        	Navigator.refreshSuggestions(application);
	        	break;
	        case 4:
	        	Navigator.launchApp(application, "ca.medeo.android");
	        	break;
        }
        
        flyOutLayout.closeDrawers();
        
    }
}