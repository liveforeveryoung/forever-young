package com.foreveryoung.flyout;

import java.util.ArrayList;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.widget.ListView;
import com.foreveryoung.Navigator;
import com.foreveryoung.R;
import com.foreveryoung.Navigator.Page;
import com.foreveryoung.adapters.FlyOutAdapter;
import com.foreveryoung.models.FlyOutItem;
import com.foreveryoung.views.fragments.flyout.AccountDetails;
import com.foreveryoung.views.fragments.flyout.SectionTitle;

public class DoctorFlyOutBuilder extends FlyOutBuilder {
	
	protected DoctorFlyOutBuilder() {
	      // Exists only to defeat instantiation.
	   }
	
	public static DoctorFlyOutBuilder getInstance() {
	      if(instance == null) {
	         instance = new DoctorFlyOutBuilder();
	      }
	      return (DoctorFlyOutBuilder) instance;
	   }
	
	@Override
	public void buildFlyOut(Activity application) {
		
		this.application = application;
		
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_account, new AccountDetails()).commit();
	
		application.getFragmentManager().beginTransaction()
			.add(R.id.flyout_menu_title, new SectionTitle("MAIN MENU")).commit();
		
		flyOutMenuList = new ArrayList<FlyOutItem>();
		
		flyOutMenuList.add(new FlyOutItem(R.string.btn_my_profile,R.drawable.menu_item_arrow, R.drawable.bg_white_20, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_patients,R.drawable.menu_item_arrow, R.drawable.bg_white_10, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_make_recommendation, R.drawable.menu_item_arrow, R.drawable.bg_white_20, 0));
		flyOutMenuList.add(new FlyOutItem(R.string.btn_open_medeo, R.drawable.menu_item_arrow, R.drawable.bg_medeo, R.drawable.medeo_icon));

		flyOutMenu = (ListView) application.findViewById(R.id.flyout_menu);

		adapter = new FlyOutAdapter(application, R.layout.flyout_list_item,
                flyOutMenuList);
		
		flyOutMenu.setAdapter(adapter);

		flyOutMenu.setOnItemClickListener(new MenuItemClickListener());
			
		//addOnClick(suggestionsButton,Page.DASHBOARD);
		
	}
	
	/**
     * Swaps fragments in the main content view
     */
	@Override
	protected void selectItem(int position) {
        // Toast.makeText(application, R.string.app_name, Toast.LENGTH_SHORT).show();
        // Highlight the selected item, update the title, and close the drawer
        
    	flyOutLayout = (DrawerLayout) application.findViewById(R.id.forever_young_main_app);

        switch (position) {
	        case 0:
	        	Navigator.navigate(application, Page.MY_PROFILE);
	        	break;
	        case 1:
	        	Navigator.navigate(application, Page.PATIENTS);
	        	break;
	        case 2:
	        	Navigator.navigate(application, Page.MAKE_RECOMMENDATION);
	        	break;
	        case 3:
	        	Navigator.launchApp(application, "ca.medeo.android");
	        	break;
        }
        
        flyOutLayout.closeDrawers();
        
    }
}
