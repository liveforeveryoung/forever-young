package com.foreveryoung.google_connect;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import android.os.AsyncTask;
import android.util.Log;

public class FYAuthRequest extends AsyncTask<String, Void, Boolean> {
   
  private HttpResponse response;
  private String content = "";
 
  DefaultHttpClient httpclient;
  private String LOG_TAG = "GAEAuthMainActivity";
  
  public FYAuthRequest(DefaultHttpClient httpclient){
	  this.httpclient = httpclient;
  }
  
  @Override
protected Boolean doInBackground(String... urls) {
 
   try {
 
    HttpGet httpGet = new HttpGet(urls[0]);
    response = httpclient.execute(httpGet);
    StatusLine statusLine = response.getStatusLine();
    Log.v(LOG_TAG, statusLine.getReasonPhrase());
    for(Cookie cookie : httpclient.getCookieStore().getCookies()) {
     Log.v(LOG_TAG, cookie.getName());
    }
    if(statusLine.getStatusCode() == HttpStatus.SC_OK){
     ByteArrayOutputStream out = new ByteArrayOutputStream();
     response.getEntity().writeTo(out);
     out.close();
     content = out.toString();
     return true;
    }
   } catch (ClientProtocolException e) {
    e.printStackTrace();
    cancel(true);
   } catch (IOException e) {
    e.printStackTrace();
    cancel(true);
   } catch (Exception e) {
    e.printStackTrace();
    cancel(true);
   } 
   return false;
  }
 
  //display the response from the request above 
  @Override
protected void onPostExecute(Boolean result) {
   Log.v(LOG_TAG, content);
  }
 }