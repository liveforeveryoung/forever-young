package com.foreveryoung.google_connect;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;
import android.util.Log;

	//using the token to get an auth cookie
	 public class GetGoogleCookie extends AsyncTask<String, Void, Boolean> {
	  
	  private HttpResponse response;
	  DefaultHttpClient httpclient;
	  HttpParams params;
	  private String LOG_TAG = "GAEAuthMainActivity";
	  
	  public GetGoogleCookie(){
		  this.httpclient = new DefaultHttpClient();
		  params = httpclient.getParams();
	  }
	  
	  @Override
	protected Boolean doInBackground(String... tokens) {
	 
	   try {
	 
	    // Don't follow redirects
	    params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
	 
	    HttpGet httpGet = new HttpGet("http://foreveryngapp.appspot.com/_ah/login?continue=http://www.foreveryng.com/signin&auth=" + tokens[0]);
	    response = httpclient.execute(httpGet);
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    response.getEntity().writeTo(out);
	    out.close();
	     
	    if(response.getStatusLine().getStatusCode() != 302){
	     // Response should be a redirect
	     Log.v(LOG_TAG, "No cookie");
	     return false;
	    }
	 
	    //check if we received the ACSID or the SACSID cookie, depends on http or https request
	    for(Cookie cookie : httpclient.getCookieStore().getCookies()) {
	     Log.v(LOG_TAG, cookie.getName());
	     if(cookie.getName().equals("ACSID") || cookie.getName().equals("SACSID")){
	      Log.v(LOG_TAG, "Got cookie");
	      return true;
	     } 
	    }
	 
	   } catch (ClientProtocolException e) {
	    e.printStackTrace();
	    cancel(true);
	   } catch (IOException e) {
	    e.printStackTrace();
	    cancel(true);
	   } catch (Exception e) {
	    e.printStackTrace();
	    cancel(true);
	   } finally {
	    params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
	   }
	   return false;
	  }
	 
	  @Override
	protected void onPostExecute(Boolean result) {
	   Log.v(LOG_TAG, "Done cookie");
	   FYAuthRequest fyAuth = new FYAuthRequest(httpclient);
	   fyAuth.execute("http://foreverynghealth.appspot.com/hello");
	  }
	 }

