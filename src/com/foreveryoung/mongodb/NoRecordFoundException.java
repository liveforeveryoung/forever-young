package com.foreveryoung.mongodb;

public class NoRecordFoundException extends MongoDBException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoRecordFoundException(){
		super();
	}
	
}
