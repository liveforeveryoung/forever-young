package com.foreveryoung.mongodb;

public class MongoDBException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MongoDBException() {
		super();
	}
	
	public MongoDBException(String e) {
		super(e);
	}

}
