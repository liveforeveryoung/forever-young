package com.foreveryoung.mongodb;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.foreveryoung.HealthData;
import com.foreveryoung.models.Doctor;
import com.foreveryoung.models.Patient;
import com.foreveryoung.models.User;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.mongodb.RESTClient.RequestMethod;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

/**
 * DB Operations class runs common operations on the mongoDB web server.
 * 
 * @author jeremywallace
 * 
 */
public class MongoDBTask {

	private final String DB_URI = "api.mongolab.com/api/1/databases/foreveryoung/collections/";
	private final String API_KEY = "PDO8WGmFvKD3v5CU0LxIA_Qu2acHZock";
	private JsonArray responseJSON = null;
	private JSONObject firstRecord = null;

	protected MongoDBClient client;

	public MongoDBTask() {

	}

	public User getUser(String email, String passSHA1)
			throws MongoDBException, NoRecordFoundException {

		String db = "FYUsers";
		client = new MongoDBClient(DB_URI + db + "/", API_KEY);
		client.addMongoParam("email", email);
		client.addMongoParam("password", passSHA1);

		Log.d("FY:", passSHA1);
		// running = true;
		// while (running==this.commit());

		commit();
		
		String userType;
		try {
			userType = firstRecord.getString("userType");

			Log.d("User Type:",userType);
			
			if (userType.equals("patient")) {
				return new Patient(firstRecord);	
			} else if (userType.equals("doctor")) {
				return new Doctor(firstRecord);	
			} else {
				return null;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MongoDBException();
		}
	}

	public HealthData getAllHealthData(int userID) throws NoRecordFoundException, MongoDBException {
		
		//??? for here, not sure this query will return expected result
		// responseJSON is assumed to be exactly the same collection of mongoDB
		client = new MongoDBClient(DB_URI + "HealthData/", API_KEY);
		client.addMongoParam("userID", String.valueOf(userID));
		Log.d("FY:", DB_URI + "HealthData/");
		
		commit();

		return new HealthData(firstRecord);

		
	}
	
	public Doctor getPatientDoctor(int userID) throws NoRecordFoundException, MongoDBException {
		
		//??? for here, not sure this query will return expected result
		// responseJSON is assumed to be exactly the same collection of mongoDB
		client = new MongoDBClient(DB_URI + "FYUsers/", API_KEY);
		client.addMongoParam("_id", String.valueOf(userID));
		
		commit();
		
		try {
			return new Doctor(firstRecord);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MongoDBException();
		}
		
	}
	
	public List<Suggestion> getSuggestions() throws NoRecordFoundException, MongoDBException {
		
		client = new MongoDBClient(DB_URI + "Suggestions/", API_KEY);
		
		commit();

		List<Suggestion> allSuggestions = new ArrayList<Suggestion>();
		
		for (int i = 0; i < responseJSON.size(); i++) {
			try {
				allSuggestions.add(new Suggestion(new JSONObject(responseJSON.get(i).toString())));
				//return new (responseJSON);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				JSONObject json;
				try {
					json = new JSONObject(responseJSON.get(i).toString());
					Log.e("JSON ERROR","Problem with one or more values in record "+i+": "+json.getString("Suggestion"));
				} catch (JSONException e1) {
					Log.e("ERROR","Could not import record:"+i);
				}
				
				continue;
			}
		}
		return allSuggestions;
		
		
	}

	public List<Patient> getPatients(int doctorID) throws NoRecordFoundException, MongoDBException {
		
		//??? for here, not sure this query will return expected result
		// responseJSON is assumed to be exactly the same collection of mongoDB
		client = new MongoDBClient(DB_URI + "FYUsers/", API_KEY);
		client.addMongoParam("doctor", String.valueOf(doctorID));
		
		commit();

		List<Patient> patients = new ArrayList<Patient>();

		try {
		
			for (int i = 0; i < responseJSON.size(); i++) {
		        patients.add(new Patient(new JSONObject(responseJSON.get(i).toString())));
			}
			
			return patients;
			//return new (responseJSON);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MongoDBException();
		}
		
	}
	
	public void commit() throws NoRecordFoundException {

		try {
			RestTask rt = new RestTask();
			rt.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (responseJSON==null) {
			throw new NoRecordFoundException();
		}
		
		
	}

	class RestTask extends AsyncTask<Void, Void, Boolean> {
		
		@Override
		protected Boolean doInBackground(Void... thisClient) {

			try {
				client.initiateRESTRequest(RequestMethod.GET);
				Log.d("Response:", client.getResponse());
			} catch (Exception e) {
				Log.e("REST","Problem with REST");
			}

			
			
			try {
				responseJSON = new JsonParser().parse(client.getResponse()).getAsJsonArray();
				Log.d("JsonString",responseJSON.get(0).toString());
				
				
				firstRecord = new JSONObject(responseJSON.get(0).toString());
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("JSON","Problem with JSON");
			}
			return true;

		}

	}

	public JsonArray getResponseJSON() {
		return responseJSON;
	}

}
