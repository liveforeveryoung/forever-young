package com.foreveryoung.mongodb;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * MongoDB Client is a rest client for MongoLabs.
 * 
 * @author jeremywallace
 * 
 */
public class MongoDBClient extends RESTClient {

	private ArrayList<NameValuePair> mongoParams;

	public MongoDBClient(String url, String apiKey) {
		super(url);
		this.addParam("apiKey", apiKey);
		mongoParams = new ArrayList<NameValuePair>();
	}

	public boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  
	}
	
	public void addMongoParam(String name, String value) {
		mongoParams.add(new BasicNameValuePair(name, value));
	}

	public String parameratize(NameValuePair p)
			throws UnsupportedEncodingException {
		
		String str;
		
		str = "%22" + p.getName() + "%22:";
		
		if (isNumeric(p.getValue())) {
			str += p.getValue();
		} else {
			str += "%22" + p.getValue() + "%22";
		}
		
		return str;
		
	}

	@Override
	public String concatParams(String concatChar)
			throws UnsupportedEncodingException {
		// add parameters
		String combinedParams = "";
		String superConcatChar = "?";
		if (!mongoParams.isEmpty()) {

			StringBuilder builder = new StringBuilder();
			builder.append(parameratize(mongoParams.remove(0)));

			for (NameValuePair p : mongoParams) {
				builder.append("%2C");
				builder.append(parameratize(p));
			}
			superConcatChar = "&";
			combinedParams += "?q=%7B" + builder.toString() + "%7D";
			
		}
		
		return combinedParams += super.concatParams(superConcatChar);
	}

}
