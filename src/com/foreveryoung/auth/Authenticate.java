package com.foreveryoung.auth;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.foreveryoung.SessionData;
import com.foreveryoung.auth.Authenticate;
import com.foreveryoung.auth.SignInFailureException.SignInResult;
import com.foreveryoung.models.User;
import com.foreveryoung.mongodb.MongoDBException;
import com.foreveryoung.mongodb.MongoDBTask;
import com.foreveryoung.mongodb.NoRecordFoundException;



/**
 * Sign In controller contains all of the actions required during a sign in process.
 * @author jeremywallace
 *
 */
public class Authenticate  {

	private MongoDBTask mongo;
	/**
	 * Sign into the application
	 * @return true if sign in was successful.
	 */
	public Boolean signIn(String email, String password) {
		return false;
	}
	
	 private String sha1(String input) throws NoSuchAlgorithmException {
	        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
	        byte[] result = mDigest.digest(input.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < result.length; i++) {
	            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
	        }
	         
	        return sb.toString();
	    }
    
	public void authenticate(String email, String password) throws SignInFailureException, InterruptedException, ExecutionException, TimeoutException {
		
		User thisUser;
		
  		if (email.isEmpty()||password.isEmpty()) {
  			throw new SignInFailureException(SignInResult.EMPTY);
  		} else {
  			
  			mongo = new MongoDBTask();
  			
  			try {
				thisUser = mongo.getUser(email, sha1(password));
				SessionData.getInstance().setUser(thisUser);
			} catch (NoRecordFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw new SignInFailureException(SignInResult.FAIL);
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
				throw new SignInFailureException(SignInResult.UNKNOWN_ERROR);
			} catch (MongoDBException e1) {
				e1.printStackTrace();
				throw new SignInFailureException(SignInResult.UNKNOWN_ERROR);
			}

  		}

    }
	
}
