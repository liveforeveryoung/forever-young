package com.foreveryoung.auth;

public class SignInFailureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public enum SignInResult {
		FAIL,EMPTY,NO_CONNECTION,UNKNOWN_ERROR
	}
	
	private SignInResult type;
	
	public SignInFailureException(SignInResult type) {
		super(type.toString());
		this.type = type;
	}

	public SignInResult getType() {
		return type;
	}

}
