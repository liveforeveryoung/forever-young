package com.foreveryoung;

import java.util.HashMap;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.models.parameters.HealthParameter.HealthState;

import android.os.AsyncTask;

public class RefreshSuggestions extends AsyncTask<Void,Void,Boolean> {
	
	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		
		HashMap<ParameterType,HealthState> st;

		for (Suggestion sg : ApplicationData.getInstance().getAllSuggestions()) {
			
			st = sg.getStateTemplate();	
			Boolean toAdd = false;
			
				// Loop through remaining parameter types to find any matches.
				for (ParameterType pt : Constants.supportedTypes) {
						HealthState userHs = SessionData.getInstance().getUserHealthData().getList(pt).getOverallState();
						HealthState parameterTemplate = st.get(pt);
						int current = Tools.valueOfHealthState(userHs), compare = Tools.valueOfHealthState(parameterTemplate);
						if (current>=compare||compare==0) {
							toAdd = true;
						} else {
							toAdd = false;
							break;
						}
				}

			if (toAdd) {
				SessionData.getInstance().getNoticeList().add(sg);
			}
		}
		
		return null;
	}
	
}
