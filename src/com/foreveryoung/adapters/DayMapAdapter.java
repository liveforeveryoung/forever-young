package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.R;
import com.foreveryoung.adapters.listeners.DayMapOnClickListener;
import com.foreveryoung.models.parameters.HealthParameter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DayMapAdapter extends ArrayAdapter<HealthParameter> {
		 
	      Context context;
	      List<HealthParameter> hpItemList;
	      int layoutResID;
	      RelativeLayout noticeWrapper;
	 
	      public DayMapAdapter(Context context, int layoutResourceID,
	                  List<HealthParameter> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.hpItemList = listItems;
	            this.layoutResID = layoutResourceID;
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	    	  	
	            HealthParameterHolder hpHolder;
	            View view = convertView;
	 
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  hpHolder = new HealthParameterHolder();
	                  hpHolder.hpDateTime = (TextView) view.findViewById(R.id.hp_date_time);
	                  hpHolder.hpValue = (TextView) view.findViewById(R.id.hp_value);
	                  hpHolder.hpIcon = (ImageView) view.findViewById(R.id.hp_icon);
	                  hpHolder.hpWrapper = (RelativeLayout) view.findViewById(R.id.hp_front);
	                  hpHolder.ignore = (Button) view.findViewById(R.id.btn_ignore_id);

	                  view.setTag(hpHolder);
	                  
	 
	            } else {
	                  hpHolder = (HealthParameterHolder) view.getTag();
	                  
	            }
	 
	            HealthParameter hpItem = this.hpItemList.get(position);
	            
	            hpHolder.hpIcon.setImageResource(hpItem.getIconResID());

	            hpHolder.hpDateTime.setText(hpItem.getTimestamp().getHours()+":"+hpItem.getTimestamp().getMinutes());
	           
	            hpHolder.hpValue.setText(hpItem.toString());
	            
	            if (position%2==0) {
	            	hpHolder.hpWrapper.setBackgroundColor(Color.parseColor("#efefef"));
	            }

	            hpHolder.ignore.setOnClickListener(new DayMapOnClickListener(position,hpItem.getType()));
	            
	            return view;
	      }
	 
	      private static class HealthParameterHolder {
	            TextView hpDateTime;
	            TextView hpValue;
	            ImageView hpIcon;
	            RelativeLayout hpWrapper;
	            Button ignore;
	      }
	     
}
