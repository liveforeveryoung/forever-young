package com.foreveryoung.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.foreveryoung.R;
import com.foreveryoung.models.HealthStatusIcon;

public class HealthStatusIconAdapter extends ArrayAdapter<HealthStatusIcon> {
	 
    Context context;
    List<HealthStatusIcon> healthStatusIcons;
    int layoutResID;

    public HealthStatusIconAdapter(Context context, int layoutResourceID,
                List<HealthStatusIcon> listItems) {
          super(context, layoutResourceID, listItems);
          this.context = context;
          this.healthStatusIcons = listItems;
          this.layoutResID = layoutResourceID;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub

          HealthIconHolder hsIcon;
          View view = convertView;

          if (view == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                
                view = inflater.inflate(layoutResID, parent, false);
                
                hsIcon = new HealthIconHolder();
                hsIcon.iconTitle = (TextView) view.findViewById(R.id.hs_icon_title);
                hsIcon.iconImage = (ImageView) view.findViewById(R.id.hs_icon_image);
                hsIcon.status = (TextView) view.findViewById(R.id.hs_status);
                
                view.setTag(hsIcon);
                

          } else {
                hsIcon = (HealthIconHolder) view.getTag();

          }

          HealthStatusIcon mItem = this.healthStatusIcons.get(position);
          
          hsIcon.iconImage.setImageResource(mItem.getImageResID());
          
          hsIcon.iconTitle.setText(view.getResources().getString(mItem.getTitleStringResID()));
          
          hsIcon.status.setText(mItem.getCurrentStatus().toString());

          return view;
    }

    private static class HealthIconHolder {
          TextView iconTitle;
          ImageView iconImage;
          TextView status;
    }
   
}