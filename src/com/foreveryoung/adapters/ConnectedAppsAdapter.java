package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.ApplicationData;
import com.foreveryoung.R;
import com.foreveryoung.connect.ConnectedApp;
import com.foreveryoung.connect.ConnectedApp.AppInstallStatus;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ConnectedAppsAdapter extends ArrayAdapter<ConnectedApp> {
		 
	      Context context;
	      int layoutResID;
	      FrameLayout connectedAppWrapper;
	      List<ConnectedApp> connectedAppList;
	 
	      public ConnectedAppsAdapter(Context context, int layoutResourceID,
	                  List<ConnectedApp> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.layoutResID = layoutResourceID;
	 
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	 
	            ConnectedAppsHolder connectedAppItem;
	            View view = convertView;
	            
	            connectedAppList = ApplicationData.getInstance().getConnectedApps();
	            
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  connectedAppItem = new ConnectedAppsHolder();
	                  connectedAppItem.appName = (TextView) view.findViewById(R.id.connected_app_name);
	                  connectedAppItem.appStatus = (TextView) view.findViewById(R.id.connected_app_status);
	                  connectedAppItem.appWrapper = (RelativeLayout) view.findViewById(R.id.connected_app_front);
	                  connectedAppItem.appIcon = (ImageView) view.findViewById(R.id.connected_app_icon);
	                  connectedAppItem.connectedIcon = (ImageView) view.findViewById(R.id.connected_status_icon);
	                  connectedAppItem.connect = (Button) view.findViewById(R.id.connect);
	                  connectedAppItem.install = (Button) view.findViewById(R.id.install);
	                  view.setTag(connectedAppItem);
	                  
	 
	            } else {
	                  connectedAppItem = (ConnectedAppsHolder) view.getTag();
	                  
	            }
	 
	            ConnectedApp caItem = this.connectedAppList.get(position);
	            
	            connectedAppItem.appIcon.setImageDrawable(caItem.getIcon());

	            connectedAppItem.appName.setText(caItem.getAppName());
	            
	            connectedAppItem.appStatus.setText(caItem.getInstallStatus()+", "+caItem.getConnectedStatus());
	            
	            connectedAppItem.connectedIcon.setImageResource(caItem.getConnectedIconResID());
	            
	            connectedAppItem.connect.setText(caItem.getConnectedBtnTextResID());
	            
	            connectedAppItem.appStatus.setTextColor(view.getResources().getColor(R.color.dark_grey));
	            
	            switch (caItem.getConnectedStatus()) {
	            	case CONNECTED:
	            		connectedAppItem.appWrapper.setBackgroundResource(R.color.fy_shaded_green);
	            		break;
	            	case DISCONNECTED:
	            		connectedAppItem.appWrapper.setBackgroundResource(R.color.white);
	            		break;
	            	case BROKEN:
	            		connectedAppItem.appWrapper.setBackgroundResource(R.color.shaded_red);
	            		connectedAppItem.appStatus.setTextColor(view.getResources().getColor(R.color.red));
	            		break;
	            }

	            connectedAppItem.connect.setOnClickListener(new View.OnClickListener() {
	            	  
	                  @Override
	                  public void onClick(View v) {
	                        // TODO Auto-generated method stub
	                        Toast.makeText(context, "Coming Soon!",Toast.LENGTH_SHORT).show();
	                  }
	            });
	            
	            if (caItem.getInstallStatus()==AppInstallStatus.NOT_INSTALLED) {

	            	connectedAppItem.appWrapper.setBackgroundResource(R.color.light_grey);
	            	
	            	final String appPackageName = caItem.getPackageName();
	            	connectedAppItem.install.setOnClickListener(new View.OnClickListener() {
		            	 
		                  @Override
		                  public void onClick(View v) {
		                	  try {
		  	            	    	context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		  	            		} catch (android.content.ActivityNotFoundException anfe) {
		  	            			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
		  	            		}
		                  }
		            });
	            	
	            } else {
	            	connectedAppItem.appStatus.setTextColor(view.getResources().getColor(R.color.dark_grey));
	            	connectedAppItem.install.setBackgroundResource(R.drawable.disabled_button);
	            }
	            

	            return view;
	      }

	 
	      private static class ConnectedAppsHolder {
	            TextView appName;
	            ImageView appIcon;
	            ImageView connectedIcon;
	            RelativeLayout appWrapper;
	            Button connect;
	            Button install;
	            TextView appStatus;
	      }
	     
}
