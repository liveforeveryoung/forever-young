package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.R;
import com.foreveryoung.Tools;
import com.foreveryoung.adapters.listeners.NoticeItemOnClickListener;
import com.foreveryoung.models.notices.Notice;
import com.foreveryoung.models.notices.Notice.NoticeStatus;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NoticeAdapter extends ArrayAdapter<Notice> {
		 
	      Context context;
	      List<Notice> noticeItemList;
	      int layoutResID;
	      RelativeLayout noticeWrapper;
	      FrameLayout slideWrapper;
	 
	      public NoticeAdapter(Context context, int layoutResourceID,
	                  List<Notice> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.noticeItemList = listItems;
	            this.layoutResID = layoutResourceID;
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	    	  	
	            NoticeHolder noticeItem;
	            View view = convertView;
	 
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  noticeItem = new NoticeHolder();
	                  noticeItem.notice = (TextView) view.findViewById(R.id.notice_text);
	                  noticeItem.noticeIcon = (ImageView) view.findViewById(R.id.notice_icon);
	                  noticeItem.noticeWrapper = (RelativeLayout) view.findViewById(R.id.notice_item_front);
	                  noticeItem.slideWrapper = (FrameLayout) view.findViewById(R.id.notice_wrapper);
	                  noticeItem.check = (Button) view.findViewById(R.id.btn_good);
	                  noticeItem.remove = (Button) view.findViewById(R.id.btn_bad);
	                  view.setTag(noticeItem);
	                  
	 
	            } else {
	                  noticeItem = (NoticeHolder) view.getTag();
	                  
	            }
	 
	            
	            
	            Notice sItem = this.noticeItemList.get(position);
	            float resize = 0;
	            int noticeLength = sItem.getNotice().length();
	            noticeItem.noticeIcon.setImageResource(sItem.getIconResID());

	            noticeItem.notice.setText(sItem.getNotice());
	           
	            if (position%2==0) {
	            	noticeItem.noticeWrapper.setBackgroundColor(Color.parseColor("#efefef"));
	            }
	            
	            resize = 100;
	            
	            if (noticeLength>300) {
	            	resize = 150;
	            } else if (noticeLength>210) {
	            	resize = 120;
	            } else if (noticeLength<110) {
	            	resize = 90;
	            } else  if (noticeLength<85) {
	            	resize = 70;
	            } else if (noticeLength<70) {
	            	resize = 60;
	            } else if (noticeLength<60) {
	            	resize = 50;
	            } else if (noticeLength<40) {
	            	resize = 40;
	            }
	            
	            AbsListView.LayoutParams params = (AbsListView.LayoutParams) noticeItem.slideWrapper.getLayoutParams();
		        params.height = Tools.convertDpToPixel(((Activity) context), resize);
		        noticeItem.slideWrapper.setLayoutParams(params);
	            
	            noticeItem.check.setOnClickListener(new NoticeItemOnClickListener(position,NoticeStatus.FINISHED));
	            noticeItem.remove.setOnClickListener(new NoticeItemOnClickListener(position,NoticeStatus.DELETED));
	            
	            return view;
	      }
	 
	      private static class NoticeHolder {
	            TextView notice;
	            ImageView noticeIcon;
	            RelativeLayout noticeWrapper;
	            FrameLayout slideWrapper;
	            Button check;
	            Button remove;
	      }
	      
	      public void removeItemFromList(int position) {
	    	  noticeItemList.remove(position);
	      }
	     
}
