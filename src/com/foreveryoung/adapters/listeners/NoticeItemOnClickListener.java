package com.foreveryoung.adapters.listeners;

import com.foreveryoung.SessionData;
import com.foreveryoung.models.notices.Suggestion;
import com.foreveryoung.models.notices.Notice.NoticeStatus;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class NoticeItemOnClickListener implements OnClickListener {

    private int position;
    private NoticeStatus status;

    public NoticeItemOnClickListener(int position, NoticeStatus status) {
       this.position = position;
       this.status = status;
    }

    @Override
	public void onClick(View v) {
    	Toast.makeText(v.getContext(), "Action "+status.toString(),Toast.LENGTH_SHORT).show();
    	SessionData.getInstance().getNotices(null, Suggestion.class, null).get(position).setStatus(status);
    	if (status==NoticeStatus.DELETED) {
    		
    	}
    }

    public int getPosition() {
      return position;
    }

 }
