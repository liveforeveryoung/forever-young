package com.foreveryoung.adapters.listeners;

import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.SessionData;
import android.view.View;
import android.view.View.OnClickListener;

public class DayMapOnClickListener implements OnClickListener {

    private int position;
    private ParameterType type;

    public DayMapOnClickListener(int position, ParameterType type) {
       this.position = position;
       this.type = type;
    }

    @Override
	public void onClick(View v) {
    	SessionData.getInstance().getUserHealthData().getList(type).get(position).setIgnore(true);
    }

    public int getPosition() {
      return position;
    }

 }
