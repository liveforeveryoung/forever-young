package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.R;
import com.foreveryoung.models.FlyOutItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FlyOutAdapter extends ArrayAdapter<FlyOutItem> {
		 
	      Context context;
	      List<FlyOutItem> FlyOutItemList;
	      int layoutResID;
	 
	      public FlyOutAdapter(Context context, int layoutResourceID,
	                  List<FlyOutItem> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.FlyOutItemList = listItems;
	            this.layoutResID = layoutResourceID;
	 
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	 
	            MenuItemHolder menuItem;
	            View view = convertView;
	 
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  menuItem = new MenuItemHolder();
	                  menuItem.menuItemText = (TextView) view.findViewById(R.id.menu_item_text);
	                  menuItem.menuItemIcon = (ImageView) view.findViewById(R.id.menu_item_icon);
	                  menuItem.menuItemSecondIcon = (ImageView) view.findViewById(R.id.menu_second_icon);
	                  menuItem.menuItemWrapper = (RelativeLayout) view.findViewById(R.id.menu_item);
	                  
	                  view.setTag(menuItem);
	                  
	 
	            } else {
	                  menuItem = (MenuItemHolder) view.getTag();
	 
	            }
	 
	            FlyOutItem mItem = this.FlyOutItemList.get(position);
	            
	            menuItem.menuItemIcon.setImageResource(mItem.getIconResID());
	            
	            menuItem.menuItemSecondIcon.setImageResource(mItem.getSecondIconResID());
	            
	            menuItem.menuItemText.setText(view.getResources().getString(mItem.getItemNameID()));
	            
	            menuItem.menuItemWrapper.setBackgroundResource(mItem.getBackgroundID());

	            return view;
	      }
	 
	      private static class MenuItemHolder {
	            TextView menuItemText;
	            ImageView menuItemIcon;
	            ImageView menuItemSecondIcon;
	            RelativeLayout menuItemWrapper;
	      }
	     
}
