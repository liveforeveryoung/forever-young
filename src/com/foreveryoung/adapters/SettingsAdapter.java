package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.R;
import com.foreveryoung.settings.Setting;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingsAdapter extends ArrayAdapter<Setting> {
		 
	      Context context;
	      List<Setting> settingList;
	      int layoutResID;
	 
	      public SettingsAdapter(Context context, int layoutResourceID,
	                  List<Setting> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.settingList = listItems;
	            this.layoutResID = layoutResourceID;
	 
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	 
	            SettingHolder settingItem;
	            View view = convertView;
	 
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  settingItem = new SettingHolder();
	                  settingItem.settingName = (TextView) view.findViewById(R.id.setting_item_name);
	                  settingItem.settingIcon = (ImageView) view.findViewById(R.id.setting_item_icon);
	                  settingItem.setting = (FrameLayout) view.findViewById(R.id.setting);
	                  settingItem.settingWrapper = (RelativeLayout) view.findViewById(R.id.settings);
	                  
	                  view.setTag(settingItem);
	                  
	 
	            } else {
	                  settingItem = (SettingHolder) view.getTag();
	 
	            }
	 
	            Setting sItem = this.settingList.get(position);
	            
	            settingItem.settingIcon.setImageResource(sItem.getIconResID());
	            
	            settingItem.setting.removeAllViewsInLayout();
	            settingItem.setting.addView(sItem.getSetting());
	            
	            
	            settingItem.settingName.setText(view.getResources().getString(sItem.getItemNameID()));
	            
	            settingItem.settingWrapper.setBackgroundResource(sItem.getBackgroundID());

	            return view;
	      }
	 
	      private static class SettingHolder {
	            TextView settingName;
	            ImageView settingIcon;
	            FrameLayout setting;
	            RelativeLayout settingWrapper;
	      }
	     
}
