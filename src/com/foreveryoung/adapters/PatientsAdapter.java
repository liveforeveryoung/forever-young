package com.foreveryoung.adapters;

import java.util.List;

import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.models.Patient;
import com.foreveryoung.models.Doctor;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PatientsAdapter extends ArrayAdapter<Patient> {
		 
	      Context context;
	      int layoutResID;
	      FrameLayout suggestionWrapper;
	      List<Patient> patientsList;
	 
	      public PatientsAdapter(Context context, int layoutResourceID,
	                  List<Patient> listItems) {
	            super(context, layoutResourceID, listItems);
	            this.context = context;
	            this.layoutResID = layoutResourceID;
	 
	      }
	 
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	            // TODO Auto-generated method stub
	 
	            PatientHolder patientItem;
	            View view = convertView;
	            
	            patientsList = ((Doctor) SessionData.getInstance().getUser()).getPatients();
	            
	            if (view == null) {
	                  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	                  
	                  view = inflater.inflate(layoutResID, parent, false);
	                  
	                  patientItem = new PatientHolder();
	                  patientItem.patientName = (TextView) view.findViewById(R.id.patient_name);
	                  patientItem.patientAge = (TextView) view.findViewById(R.id.patient_age_holder);
	                  patientItem.patientIcon = (ImageView) view.findViewById(R.id.patient_icon);
	                  patientItem.status = (Button) view.findViewById(R.id.btn_status);
	                  patientItem.makeRecommendation = (Button) view.findViewById(R.id.btn_make_recommendation);
	                  view.setTag(patientItem);
	                  
	 
	            } else {
	                  patientItem = (PatientHolder) view.getTag();
	                  
	            }
	 
	            Patient pItem = this.patientsList.get(position);
	            
	            if (pItem.getGenderIcon()!=0)
	            patientItem.patientIcon.setImageResource(pItem.getGenderIcon());

	            patientItem.patientName.setText(pItem.getFullName());
	            
	            patientItem.patientAge.setText(String.valueOf(pItem.getAge()));
	            
	            patientItem.status.setOnClickListener(new View.OnClickListener() {
	            	 
	                  @Override
	                  public void onClick(View v) {
	                        // TODO Auto-generated method stub
	                        Toast.makeText(context, "Status Button Clicked",Toast.LENGTH_SHORT).show();
	                  }
	            });
	 
	            patientItem.makeRecommendation.setOnClickListener(new View.OnClickListener() {
	 
	                              @Override
	                              public void onClick(View v) {
	                                    // TODO Auto-generated method stub
	                                    Toast.makeText(context, "Make Rec Clicked",Toast.LENGTH_SHORT).show();
	                              }
	                        });

	            return view;
	      }

	 
	      private static class PatientHolder {
	            TextView patientName;
	            TextView patientAge;
	            ImageView patientIcon;
	            Button status;
	            Button makeRecommendation;
	      }
	     
}
