package com.foreveryoung.views.fragments.flyout;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foreveryoung.R;

public class HealthTrendsScrollbar extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_dashboard_scrollbar, container,
				false);
		return rootView;
	}
	
}
