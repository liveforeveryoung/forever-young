package com.foreveryoung.views.fragments.flyout;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foreveryoung.R;

@SuppressLint("ValidFragment")
public class SectionTitle extends Fragment {

	String text;
	TextView titleText;
	
	public SectionTitle() {
		this.text = "SECTION";
	}
	
	public SectionTitle(String text) {
		this.text = text;
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.flyout_section_titles, container,
				false);
		
		titleText = (TextView) rootView.findViewById(R.id.section_title);
		
		titleText.setText(text);
		
		return rootView;
	}
	
}
