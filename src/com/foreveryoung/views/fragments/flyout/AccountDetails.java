package com.foreveryoung.views.fragments.flyout;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.foreveryoung.ForeverYoungWelcome;
import com.foreveryoung.Navigator;
import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.Navigator.Page;
import com.foreveryoung.models.Doctor;
import com.foreveryoung.models.Patient;

public class AccountDetails extends Fragment {
	
	TextView fullName;
	TextView age;
	TextView height;
	TextView weight;
	TextView noticeCounter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = null;	
		if (SessionData.getInstance().getUser().getClass() == Patient.class) {
			
			rootView = inflater.inflate(R.layout.flyout_account, container,
					false);
			
			fullName = (TextView) rootView.findViewById(R.id.full_name);
			age = (TextView) rootView.findViewById(R.id.age_value);
			height = (TextView) rootView.findViewById(R.id.height_value);
			weight = (TextView) rootView.findViewById(R.id.weight_value);
			noticeCounter = (TextView) rootView.findViewById(R.id.suggestions_bubble);
		
			fullName.setText(SessionData.getInstance().getUser().getFullName());

			age.setText(String.valueOf(((Patient)SessionData.getInstance().getUser()).getAge()));
			height.setText(String.valueOf(
					((Patient)SessionData.getInstance().getUser()).getHeight() + " cm"));
			weight.setText(String.valueOf(((Patient)SessionData.getInstance().getUser()).getWeight() + " lbs"));
		
			noticeCounter.setText(String.valueOf(SessionData.getInstance().getNotices(null,null,null).size()));
			addOnClick(noticeCounter, Page.DASHBOARD);
			
		} else if (SessionData.getInstance().getUser().getClass() == Doctor.class) {
			
			rootView = inflater.inflate(R.layout.flyout_doctor_account, container,
					false);
			
			fullName = (TextView) rootView.findViewById(R.id.doctor_full_name);
			fullName.setText(SessionData.getInstance().getUser().getFullName());
			
		} else {
			Intent i = new Intent(getActivity(), ForeverYoungWelcome.class);     
  		    startActivity(i);
		}
	
		return rootView;
	}
	
	private void addOnClick(View thisView, final Page to) {
		thisView.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v)
            {
                Navigator.navigate(getActivity(),Page.DASHBOARD);
                ((DrawerLayout) getActivity().findViewById(R.id.forever_young_main_app)).closeDrawers();
                
            } 
        });
	}
	
}
