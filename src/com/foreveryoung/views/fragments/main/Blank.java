package com.foreveryoung.views.fragments.main;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.foreveryoung.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class Blank extends Fragment {

	private View rootView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		rootView = inflater.inflate(R.layout.blank, container,
				false);

		return rootView;
	}

}