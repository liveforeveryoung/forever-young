package com.foreveryoung.views.fragments.main;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foreveryoung.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class TitleBar extends Fragment {

	private String titleDesc;
	private boolean isSet = false;
	private View rootView;
	private int titleResID;
	private int statusIconResID;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		rootView = inflater.inflate(R.layout.fragment_title, container,
				false);
		
		if (titleResID!=0) {
			TextView t = (TextView) rootView.findViewById(R.id.title_text);
			t.setText(getResources().getString(titleResID));
		}
		
		if (statusIconResID!=0) {
			ImageView i = (ImageView) rootView.findViewById(R.id.title_icon);
			i.setImageResource(statusIconResID);
			i.getLayoutParams().height = 180;
			i.getLayoutParams().width = 180;
		}
		
		
		
		return rootView;
	}

	/**
	 * Set the title of the title bar fragment.
	 * @param titleResID the resource id of the string to use.
	 */
	public void setTitle(int titleResID) {
		this.titleResID = titleResID;
		this.isSet = true;
	}
	
	public boolean isSet() {
		return isSet;
	}
	
	/**
	 * Set the title of the title bar fragment.
	 * @param titleResID the resource id of the string to use.
	 */
	public void setStatusIconResID(int statusIconResID) {
		this.statusIconResID = statusIconResID;
		
	}
	
	public String getTitleDesc() {
		return titleDesc;
	}

	public void setTitleDesc(String titleDesc) {
		this.titleDesc = titleDesc;
	}


}