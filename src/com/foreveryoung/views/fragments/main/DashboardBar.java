package com.foreveryoung.views.fragments.main;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.SessionData;
import com.foreveryoung.Navigator;
import com.foreveryoung.R;
import com.foreveryoung.Navigator.Page;
import com.foreveryoung.adapters.HealthStatusIconAdapter;
import com.foreveryoung.models.HealthStatusIcon;

public class DashboardBar extends Fragment {
		
	private Application fyapp;
	private LinearLayout scrollbarContent;
	private HealthStatusIconAdapter adapter;
	private List<HealthStatusIcon> hsIconList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_dashboard_scrollbar, container,
				false);
		
		fyapp = getActivity().getApplication();
		
		scrollbarContent = (LinearLayout) rootView.findViewById(R.id.status_icons);

		hsIconList = new ArrayList<HealthStatusIcon>();
		
		hsIconList.add(new HealthStatusIcon(R.string.hs_bp,
				SessionData.getInstance().getUserHealthData().getList(ParameterType.BLOOD_PRESSURE).getLatestValue()));
		
		hsIconList.add(new HealthStatusIcon(R.string.hs_hr,
				SessionData.getInstance().getUserHealthData().getList(ParameterType.HEART_RATE).getLatestValue()));
		
		hsIconList.add(new HealthStatusIcon(R.string.hs_sleep,
				SessionData.getInstance().getUserHealthData().getList(ParameterType.SLEEP).getLatestValue()));
		
		
		hsIconList.add(new HealthStatusIcon(R.string.hs_activity,
				SessionData.getInstance().getUserHealthData().getList(ParameterType.ACTIVITY).getLatestValue()));
		
		hsIconList.add(new HealthStatusIcon(R.string.hs_weight,
				SessionData.getInstance().getUserHealthData().getList(ParameterType.WEIGHT).getLatestValue()));
		
		adapter = new HealthStatusIconAdapter(getActivity(), R.layout.model_health_status_icon,
                hsIconList);
		
		final int adapterCount = adapter.getCount();

		for (int i = 0; i < adapterCount; i++) {
		  View item = adapter.getView(i, null, null);
		  addOnClick(item,getPage(i));
		  scrollbarContent.addView(item);
		  
		}
		
		//scrollbarContent.addView(getButtonImageView(R.drawable.heartrate_gr,Page.HEARTRATE_TREND));
		//scrollbarContent.addView(getButtonImageView(R.drawable.bloodpressure_gr,Page.BLOODPRESSURE_TREND));
		//scrollbarContent.addView(getButtonImageView(R.drawable.sleep_gr,Page.SLEEP_TREND));
		//scrollbarContent.addView(getButtonImageView(R.drawable.activity_gr,Page.ACTIVITY_TREND));
		
		return rootView;
	}
	
	private Page getPage(int position) {
		switch (position) {
		case 0:
			return Page.BLOODPRESSURE_TREND;
		case 1:
			return Page.HEARTRATE_TREND;
		case 2:
			return Page.SLEEP_TREND;
		case 3:
			return Page.ACTIVITY_TREND;
		case 4:
			return Page.WEIGHT_TREND;
		}
		return Page.DASHBOARD;
	}
	
	private void addOnClick(View thisView, final Page to) {
		thisView.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v) {
                Navigator.navigate(getActivity(),to);
                ((DrawerLayout) getActivity().findViewById(R.id.forever_young_main_app)).closeDrawers();
                
            } 
        });
	}
	
	private ImageView getButtonImageView(int resource, final Page to) {
		
		ImageView button = new ImageView(fyapp.getBaseContext());
		button.setImageResource(resource);
		
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    lp.setMargins(20, 20, 20, 10);
		
	    button.setLayoutParams(lp); 
        button.getLayoutParams().height = 120;
        button.getLayoutParams().width = 120;
        
		return button;
	}

}
