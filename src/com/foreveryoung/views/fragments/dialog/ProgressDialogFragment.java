package com.foreveryoung.views.fragments.dialog;

import com.foreveryoung.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressDialogFragment extends DialogFragment {
	
	protected String progressMessage = "Loading...";
	protected TextView dialogText;
	protected ProgressBar dialogProgress;
	
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Dialog));
        
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialog_progress, null);
        
	    dialogText = (TextView) view.findViewById(R.id.fy_dialog_text);
		dialogProgress = (ProgressBar) view.findViewById(R.id.fy_progress);
		dialogText.setText(progressMessage);
		dialogProgress.animate();
        // Create the AlertDialog object and return it
		AlertDialog thisDialog = builder.create();
		thisDialog.setView(view,0,0,0,0);
		
		return thisDialog;
        
    }
    
    public void updateMessage(String text) {
    	if (dialogText!=null) {
    		dialogText.setText(text);
    	}
    }
    
    public void setMessage(String message) {
    	progressMessage = message;
    }
    
    public static ProgressDialogFragment newInstance(String progressMessage) {
        ProgressDialogFragment frag = new ProgressDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", R.string.progress_dialog_title);
        frag.setArguments(args);
        frag.setMessage(progressMessage);
        return frag;
    }
   
}
