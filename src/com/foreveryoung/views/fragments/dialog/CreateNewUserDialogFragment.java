package com.foreveryoung.views.fragments.dialog;

import com.foreveryoung.ForeverYoungPatient;
import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.models.User;
import com.foreveryoung.mongodb.MongoDBException;
import com.foreveryoung.mongodb.MongoDBTask;
import com.foreveryoung.mongodb.NoRecordFoundException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class CreateNewUserDialogFragment extends DialogFragment {
	
	protected String progressMessage;
	protected TextView titleText;
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialog_add_new_user, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);
        builder.setPositiveButton(R.string.create_new_user, new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
                // FIRE ZE MISSILES!
            	User thisUser = null;
            	MongoDBTask mongo = new MongoDBTask();
            	
        		// here I hardcoded the user as: Bob/test
        		// every time new user creation, it will jump to bob
        		// just for demo purpose...
            	
            	
//            	Authenticate auth = new Authenticate();
//            	try {
//					auth.authenticate("bob", "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3");
//				} catch (SignInFailureException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (ExecutionException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (TimeoutException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
            	
            	
            	try {
					thisUser = mongo.getUser("bob", "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3");
					SessionData.getInstance().setUser(thisUser);
					Log.d("user:", thisUser.getFullName());
				} catch (NoRecordFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MongoDBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            	
  				Intent i;
  				i = new Intent(getActivity(), ForeverYoungPatient.class);
  				  				
  				startActivity(i);
            }
        })
        	.setNegativeButton(R.string.create_new_user_cancel, new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        
        titleText = (TextView) view.findViewById(R.id.fy_dialog_text);
        AlertDialog thisDialog = builder.create();
		thisDialog.setView(view,0,0,0,0);
        // Create the AlertDialog object and return it
        return thisDialog;
        
    }
    
    public static CreateNewUserDialogFragment newInstance() {
        CreateNewUserDialogFragment frag = new CreateNewUserDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", R.string.create_new_user_dialog);
        frag.setArguments(args);
        return frag;
    }
   
}
