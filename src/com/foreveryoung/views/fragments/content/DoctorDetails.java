package com.foreveryoung.views.fragments.content;
import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.models.Doctor;
import com.foreveryoung.models.Patient;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DoctorDetails extends Fragment {

	TextView fullName;
	TextView clinic;
	TextView phone;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_doctor_details, container,
				false);
		
		fullName = (TextView) rootView.findViewById(R.id.doctor_name);
		clinic = (TextView) rootView.findViewById(R.id.clinic);
		phone = (TextView) rootView.findViewById(R.id.doctor_phone);
		
		Doctor thisDoc = ((Patient)SessionData.getInstance().getUser()).getDoctor();
		
		if (thisDoc!=null) {
			Log.d("ThisDoc",thisDoc.getFullName());
			fullName.setText(thisDoc.getFullName());
			clinic.setText(thisDoc.getAddress().getFullAddressText());
			phone.setText(thisDoc.getPhone());
		} else {
			fullName.setText(this.getString(R.string.doctor_not_found));
			clinic.setText(this.getString(R.string.click_to_add_doctor));
		}
		
		return rootView;
	}
    
}