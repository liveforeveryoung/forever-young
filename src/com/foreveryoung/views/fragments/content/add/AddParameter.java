package com.foreveryoung.views.fragments.content.add;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import com.foreveryoung.R;
import com.foreveryoung.views.fragments.content.add.AddActivity;
import com.foreveryoung.views.fragments.content.add.AddBloodPressure;
import com.foreveryoung.views.fragments.content.add.AddHeartRate;
import com.foreveryoung.views.fragments.content.add.AddSleep;
import com.foreveryoung.views.fragments.content.add.AddWeight;

public class AddParameter extends Fragment {

	private Spinner spinner, spinner2;
	private Button btnSubmit;

	private FrameLayout text;
	private FragmentTransaction transaction;
	private boolean defaultSelctedSkipFlag = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_parameter,
				container, false);

		text = (FrameLayout) rootView.findViewById(R.id.add_content_holder);

		
		spinner = (Spinner) rootView.findViewById(R.id.spinner1);
		
		spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

//			  public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
//					Toast.makeText(parent.getContext(), 
//						"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
//						Toast.LENGTH_SHORT).show();
//				  }
			
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				String selected = parent.getItemAtPosition(position).toString();
				
				// here this flag is to skip the first time load this fragment
				// since by default "Blood Pressure" is selected, this default selection will be skipped
				if (defaultSelctedSkipFlag) {
					
					transaction = getActivity().getFragmentManager().beginTransaction();
					
					if (selected.equals("Blood Pressure (mmHg)")) {
						transaction.replace(R.id.add_content_holder, new AddBloodPressure());
					}
					else if (selected.equals("Heart Rate (bps)")) {
						transaction.replace(R.id.add_content_holder, new AddHeartRate());
					}
					else if (selected.equals("Sleep")) {
						transaction.replace(R.id.add_content_holder, new AddSleep());

					}
					else if (selected.equals("Activity")) {
						transaction.replace(R.id.add_content_holder, new AddActivity());

					}
					else if (selected.equals("Weight (lbs)")) {
						transaction.replace(R.id.add_content_holder, new AddWeight());

					}
					
					transaction.commit();
				}
				
				defaultSelctedSkipFlag = true;

			}
		});

		return rootView;
	}

}
