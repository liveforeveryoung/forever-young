package com.foreveryoung.views.fragments.content.add;

import java.util.Calendar;
import java.util.Date;

import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.Exercise;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddActivity extends Fragment {
	
	private EditText steps;
	private EditText duration;
	private EditText date;
	private EditText time;
	
	private Button submitBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_activity, container,
				false);
		

		steps = (EditText) rootView.findViewById(R.id.steps);
		duration = (EditText) rootView.findViewById(R.id.duration);
		date = (EditText) rootView.findViewById(R.id.date);
		time = (EditText) rootView.findViewById(R.id.time);
		submitBtn = (Button) rootView.findViewById(R.id.btnSubmit);
		

		submitBtn.setOnClickListener(
				
				new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                    	Calendar today = Calendar.getInstance();
                    	today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
                    	Date todayDate = today.getTime();
                		
                    	Exercise exc = new Exercise(Integer.parseInt(steps.getText().toString()), Double.parseDouble(duration.getText().toString()), todayDate);
                    	
                    	SessionData.getInstance().getUserHealthData().getList(ParameterType.ACTIVITY).add(exc);
                    	// toast for test
                    	Toast.makeText(getActivity().getBaseContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                    }
            }
				
				);
		
		
		
		return rootView;
	}
}
