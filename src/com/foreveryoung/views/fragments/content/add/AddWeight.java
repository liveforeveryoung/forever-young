package com.foreveryoung.views.fragments.content.add;

import java.util.Calendar;
import java.util.Date;

import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.Weight;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddWeight extends Fragment {
	
	private EditText weight;
	private EditText date;

	private Button submitBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_weight, container,
				false);
		

		weight = (EditText) rootView.findViewById(R.id.weight);
		date = (EditText) rootView.findViewById(R.id.date);
		submitBtn = (Button) rootView.findViewById(R.id.btnSubmit);

		submitBtn.setOnClickListener(

		new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Calendar today = Calendar.getInstance();
				today.clear(Calendar.HOUR);
				today.clear(Calendar.MINUTE);
				today.clear(Calendar.SECOND);
				Date todayDate = today.getTime();

				Weight we = new Weight(Double
						.parseDouble(weight.getText().toString()), todayDate);

				SessionData.getInstance().getUserHealthData()
						.getList(ParameterType.WEIGHT).add(we);
				// toast for test
				Toast.makeText(getActivity().getBaseContext(),
						"Data Saved Successfully", Toast.LENGTH_SHORT).show();
			}
		}

		);


		
		return rootView;
	}
}
