package com.foreveryoung.views.fragments.content.add;

import java.util.Calendar;
import java.util.Date;

import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.HeartRate;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddHeartRate extends Fragment {
	
	private EditText count;
	private EditText date;
	private EditText time;
	
	private Button submitBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_heart_rate, container,
				false);
		
		

		
		count = (EditText) rootView.findViewById(R.id.count);
		date = (EditText) rootView.findViewById(R.id.date);
		time = (EditText) rootView.findViewById(R.id.time);
		submitBtn = (Button) rootView.findViewById(R.id.btnSubmit);
		

		submitBtn.setOnClickListener(
				
				new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                    	Calendar today = Calendar.getInstance();
                    	today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
                    	Date todayDate = today.getTime();
                		
    					HeartRate hr = new HeartRate(Integer.parseInt(count.getText().toString()), todayDate);
                    	SessionData.getInstance().getUserHealthData().getList(ParameterType.HEART_RATE).add(hr);
                    	// toast for test
                    	Toast.makeText(getActivity().getBaseContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                    }
            }
				
				);
		
		return rootView;
	}
}
