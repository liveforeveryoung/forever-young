package com.foreveryoung.views.fragments.content.add;

import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.models.notices.DoctorRecommendation;
import com.foreveryoung.models.Patient;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddDoctorRecommendation extends Fragment {
	
	private Patient thisPatient;
	private EditText recommendation;
	
	private Button submitBtn;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_recommendation, container,
				false);
		
		recommendation = (EditText) rootView.findViewById(R.id.recommendation);
		submitBtn = (Button) rootView.findViewById(R.id.btnSubmit);
		
		submitBtn.setOnClickListener(
				
				new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                    	
                    	DoctorRecommendation dr = new DoctorRecommendation(recommendation.getText().toString(),SessionData.getInstance().getUser().getFullName());
                		thisPatient.getNotices().add(dr);
                		
                    	Toast.makeText(getActivity().getBaseContext(), "Data Saved Successfully", Toast.LENGTH_SHORT).show();
                    }
            }
				
				);
		
		
		
		return rootView;
	}
}
