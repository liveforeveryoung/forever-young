package com.foreveryoung.views.fragments.content.add;

import java.util.Calendar;
import java.util.Date;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.models.parameters.BloodPressure;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddBloodPressure extends Fragment {
	private EditText systolic;
	private EditText diastolic;
	private EditText date;
	private EditText time;

	private Button submitBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_blood_pressure,
				container, false);

		systolic = (EditText) rootView.findViewById(R.id.systolic);
		diastolic = (EditText) rootView.findViewById(R.id.diastolic);
		date = (EditText) rootView.findViewById(R.id.date);
		time = (EditText) rootView.findViewById(R.id.time);
		submitBtn = (Button) rootView.findViewById(R.id.btnSubmit);

		submitBtn.setOnClickListener(

		new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Calendar today = Calendar.getInstance();
				today.clear(Calendar.HOUR);
				today.clear(Calendar.MINUTE);
				today.clear(Calendar.SECOND);
				Date todayDate = today.getTime();

				// if empty submitted here, then it will crashed
				// make sure put some value before submit
				BloodPressure bp = new BloodPressure(Double
						.parseDouble(systolic.getText().toString()), Double
						.parseDouble(diastolic.getText().toString()), todayDate);
				SessionData.getInstance().getUserHealthData()
						.getList(ParameterType.BLOOD_PRESSURE).add(bp);
				// toast for test
				Toast.makeText(getActivity().getBaseContext(),
						"Data Saved Successfully", Toast.LENGTH_SHORT).show();
			}
		}

		);

		return rootView;
	}

}
