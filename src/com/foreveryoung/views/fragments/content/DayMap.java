package com.foreveryoung.views.fragments.content;

import java.util.Date;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.Tools;
import com.foreveryoung.adapters.DayMapAdapter;
import com.foreveryoung.models.notices.Notice.NoticeStatus;
import com.foreveryoung.models.parameters.lists.HealthParameterList;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DayMap extends Fragment {

    private HealthParameterList hpList;
    private SwipeListView hpListView;
	private DayMapAdapter dayMapAdapter;
	private NoticeStatus status = null;
	private ParameterType type = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_daymap, container,
				false);
		
		hpList = SessionData.getInstance().getUserHealthData().getListByDate(new Date(2008,1,1));
	
		hpListView = (SwipeListView) rootView.findViewById(R.id.day_map);
		
		hpListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
	
	        @Override
	        public void onListChanged() {
	        	
	        }
	
	        @Override
	        public void onStartOpen(int position, int action, boolean right) {
	            Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
	        }
	
	        @Override
	        public void onStartClose(int position, boolean right) {
	            Log.d("swipe", String.format("onStartClose %d", position));
	        }
	
	    });
	
		//These are the swipe listview settings. you can change these
        //setting as your requirement
		hpListView.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
		hpListView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
		hpListView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
		hpListView.setOffsetLeft(Tools.convertDpToPixel(this.getActivity(),315f)); // left side offset
		hpListView.setOffsetRight(Tools.convertDpToPixel(this.getActivity(),315f)); // right side offset
		hpListView.setAnimationTime(25); // Animation time
		hpListView.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress

		dayMapAdapter = new DayMapAdapter(getActivity(), R.layout.model_health_parameter,
                hpList);
		
		hpListView.setAdapter(dayMapAdapter);
		
		return rootView;
	}
    
}