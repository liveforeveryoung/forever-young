package com.foreveryoung.views.fragments.content;

import java.util.ArrayList;
import java.util.List;

import com.foreveryoung.R;
import com.foreveryoung.adapters.SettingsAdapter;
import com.foreveryoung.settings.Setting;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Switch;

/**
 * 
 * @author gaurav.minocha
 *
 */

public class Settings extends Fragment {

	List<Setting> settingList;
	ListView settingListView;
	SettingsAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings, container,
				false);

	    settingList = new ArrayList<Setting>();
		
	    Switch notificationSwitch = new Switch(getActivity());
	    Switch trendSwitch = new Switch(getActivity());
	    
	    settingList.add(new Setting(R.string.notifications_toggle,R.drawable.notification_new,R.color.white,notificationSwitch));
	    settingList.add(new Setting(R.string.show_trends,R.drawable.notification_trend,R.color.light_grey,trendSwitch));
		
	    settingListView = (ListView) rootView.findViewById(R.id.setting_list);

		adapter = new SettingsAdapter(getActivity(), R.layout.model_setting,
				settingList);
		
		settingListView.setAdapter(adapter);

		return rootView;
	}

}
