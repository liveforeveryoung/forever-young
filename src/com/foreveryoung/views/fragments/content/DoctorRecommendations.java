package com.foreveryoung.views.fragments.content;
import java.util.List;

import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.adapters.NoticeAdapter;
import com.foreveryoung.models.notices.DoctorRecommendation;
import com.foreveryoung.models.notices.Notice;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DoctorRecommendations extends Fragment {

    private List<Notice> recommendationsList;
    private ListView recommendationsListView;
	private NoticeAdapter recommendationsAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_doctor_recommendations, container,
				false);
		
		recommendationsList = SessionData.getInstance().getNotices(null,DoctorRecommendation.class,null);
	
		recommendationsListView = (ListView) rootView.findViewById(R.id.recommendations_list);
		
		recommendationsAdapter = new NoticeAdapter(getActivity(), R.layout.model_doctor_rec,
                recommendationsList);
		
		recommendationsListView.setAdapter(recommendationsAdapter);
		
		return rootView;
	}
    
}