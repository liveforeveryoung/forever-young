package com.foreveryoung.views.fragments.content;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.foreveryoung.ApplicationData;
import com.foreveryoung.R;
import com.foreveryoung.Tools;
import com.foreveryoung.adapters.ConnectedAppsAdapter;
import com.foreveryoung.connect.ConnectedApp;
import com.foreveryoung.connect.ConnectedApp.ConnectAppStatus;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

public class ConnectedAppsList extends Fragment {

	private List<ConnectedApp> connectedAppsList;
    private SwipeListView connectedAppsListView;
	private ConnectedAppsAdapter connectedAppsAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_connected_apps, container,
				false);

		if (ApplicationData.getInstance().getConnectedApps().isEmpty()) {
			loadAllConnectedApps();
		}
		
		connectedAppsList = ApplicationData.getInstance().getConnectedApps();
		
		for (ConnectedApp app : connectedAppsList) {
			Log.d("App: ",app.getPackageName());
		}

		connectedAppsListView = (SwipeListView) rootView.findViewById(R.id.connected_apps_list);

		connectedAppsListView.setSwipeListViewListener(new BaseSwipeListViewListener() {	
	
			 @Override
		        public void onOpened(int position, boolean toRight) {
		        }
		
		        @Override
		        public void onClosed(int position, boolean fromRight) {
		        }
		
		        @Override
		        public void onListChanged() {
		        }
		
		        @Override
		        public void onMove(int position, float x) {
		        }
		
		        @Override
		        public void onStartOpen(int position, int action, boolean right) {
		            Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
		        }
		
		        @Override
		        public void onStartClose(int position, boolean right) {
		            Log.d("swipe", String.format("onStartClose %d", position));
		        }
		
		        @Override
		        public void onClickFrontView(int position) {
		            Log.d("swipe", String.format("onClickFrontView %d", position));
		
		            connectedAppsListView.openAnimate(position); //when you touch front view it will open
		
		        }
		
		        @Override
		        public void onClickBackView(int position) {
		            Log.d("swipe", String.format("onClickBackView %d", position));
		
		            connectedAppsListView.closeAnimate(position);//when you touch back view it will close
		        }
		
		        @Override
		        public void onDismiss(int[] reverseSortedPositions) {
		
		        }
	
	    });
	
		//These are the swipe listview settings. you can change these
        //setting as your requirement
        connectedAppsListView.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
        connectedAppsListView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
        connectedAppsListView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
        connectedAppsListView.setOffsetLeft(Tools.convertDpToPixel(this.getActivity(),0f)); // left side offset
        connectedAppsListView.setOffsetRight(Tools.convertDpToPixel(this.getActivity(),0f)); // right side offset
        connectedAppsListView.setAnimationTime(50); // Animation time
        connectedAppsListView.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
 		
		connectedAppsAdapter = new ConnectedAppsAdapter(getActivity(), R.layout.model_connected_app,
                connectedAppsList);
		
		connectedAppsListView.setAdapter(connectedAppsAdapter);
		
	return rootView;
	}

	protected void loadAllConnectedApps() {
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.fitbit.FitbitMobile","FitBit"));
		
		ConnectedApp medeo = new ConnectedApp(this.getActivity(), "ca.medeo.android","Medeo");
		medeo.setConnectAppStatus(ConnectAppStatus.CONNECTED);
		ApplicationData.getInstance().getConnectedApps().add(medeo);
		
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.protogeo.moves","Moves"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.lslk.sleepbot","SleepBot"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.withings.wiscale2","Withings"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.jawbone.up","Jawbone UP"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.pribble.htc.ble.hrm","BLE Heart Rate Monitor"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "si.modula.android.instantheartrate","Instant Heart Rate Monitor"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.myfitnesspal.android","My Fitness Pal"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "com.healthtap.userhtexpress","Health Tap"));
		ApplicationData.getInstance().getConnectedApps().add(new ConnectedApp(this.getActivity(), "ca.dairyfarmers.getenough","Getting Enough: Diet Tracker"));

	}
}
