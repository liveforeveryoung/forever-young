package com.foreveryoung.views.fragments.content.doctor;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.Tools;
import com.foreveryoung.adapters.PatientsAdapter;
import com.foreveryoung.models.Patient;
import com.foreveryoung.models.Doctor;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

public class PatientList extends Fragment {

	private List<Patient> patientsList;
    private SwipeListView swipelistview;
	private PatientsAdapter patientsAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_doctor_patients, container,
				false);
		patientsList = ((Doctor)SessionData.getInstance().getUser()).getPatients();
		
		swipelistview = (SwipeListView) rootView.findViewById(R.id.patients_list);

		swipelistview.setSwipeListViewListener(new BaseSwipeListViewListener() {
	        @Override
	        public void onOpened(int position, boolean toRight) {
	        }
	
	        @Override
	        public void onClosed(int position, boolean fromRight) {
	        }
	
	        @Override
	        public void onListChanged() {
	        }
	
	        @Override
	        public void onMove(int position, float x) {
	        }
	
	        @Override
	        public void onStartOpen(int position, int action, boolean right) {
	            Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
	        }
	
	        @Override
	        public void onStartClose(int position, boolean right) {
	            Log.d("swipe", String.format("onStartClose %d", position));
	        }
	
	        @Override
	        public void onClickFrontView(int position) {
	            Log.d("swipe", String.format("onClickFrontView %d", position));
	
	            swipelistview.openAnimate(position); //when you touch front view it will open
	
	        }
	
	        @Override
	        public void onClickBackView(int position) {
	            Log.d("swipe", String.format("onClickBackView %d", position));
	
	            swipelistview.closeAnimate(position);//when you touch back view it will close
	        }
	
	        @Override
	        public void onDismiss(int[] reverseSortedPositions) {
	
	        }
	
	    });
	
		//These are the swipe listview settings. you can change these
        //setting as your requirement
        swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
        swipelistview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
        swipelistview.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
        swipelistview.setOffsetLeft(Tools.convertDpToPixel(this.getActivity(),0f)); // left side offset
        swipelistview.setOffsetRight(Tools.convertDpToPixel(this.getActivity(),0f)); // right side offset
        swipelistview.setAnimationTime(50); // Animation time
        swipelistview.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
 		
		patientsAdapter = new PatientsAdapter(getActivity(), R.layout.model_patient,
                patientsList);
		
		swipelistview.setAdapter(patientsAdapter);
		
	return rootView;
	}

}
