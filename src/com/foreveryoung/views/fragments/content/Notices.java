package com.foreveryoung.views.fragments.content;

import java.util.List;

import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.Tools;
import com.foreveryoung.adapters.NoticeAdapter;
import com.foreveryoung.models.notices.Notice;
import com.foreveryoung.models.notices.Notice.NoticeStatus;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Notices extends Fragment {

    private List<Notice> noticeList;
    private SwipeListView noticeListView;
	private NoticeAdapter noticeAdapter;
	private NoticeStatus status = null;
	private ParameterType type = null;
	private Class<?> category = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notices, container,
				false);
		
		noticeList = SessionData.getInstance().getNotices(status,category,type);
	
		noticeListView = (SwipeListView) rootView.findViewById(R.id.notices_list);
		
		noticeListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
	
	        @Override
	        public void onListChanged() {
	        	
	        }
	
	        @Override
	        public void onStartOpen(int position, int action, boolean right) {
	            Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
	        }
	
	        @Override
	        public void onStartClose(int position, boolean right) {
	            Log.d("swipe", String.format("onStartClose %d", position));
	        }
	
	    });
	
		//These are the swipe listview settings. you can change these
        //setting as your requirement
		noticeListView.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
		noticeListView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
		noticeListView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
		noticeListView.setOffsetLeft(Tools.convertDpToPixel(this.getActivity(),300f)); // left side offset
		noticeListView.setOffsetRight(Tools.convertDpToPixel(this.getActivity(),300f)); // right side offset
		noticeListView.setAnimationTime(25); // Animation time
		noticeListView.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress

		noticeAdapter = new NoticeAdapter(getActivity(), R.layout.model_notice,
                noticeList);
		
		noticeListView.setAdapter(noticeAdapter);
		
		return rootView;
	}
	
	public void setItems(NoticeStatus status, ParameterType type, Class<?> category) {
		this.type = type;
		this.status = status;
		this.category = category;
	}
    
}