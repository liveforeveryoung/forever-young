package com.foreveryoung.views.fragments.content;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foreveryoung.R;

public class GeneralText extends Fragment {

	String text;
	TextView textView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_general_html_text, container,
				false);
		
		textView = (TextView) rootView.findViewById(R.id.main_text);
		textView.setText(Html.fromHtml(text));
		return rootView;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
}
