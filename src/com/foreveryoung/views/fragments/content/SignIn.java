package com.foreveryoung.views.fragments.content;

import org.apache.http.impl.client.DefaultHttpClient;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.foreveryoung.Navigator;
import com.foreveryoung.SessionData;
import com.foreveryoung.ForeverYoungDoctor;
import com.foreveryoung.ForeverYoungPatient;
import com.foreveryoung.R;
import com.foreveryoung.auth.Authenticate;
import com.foreveryoung.auth.SignInFailureException;
import com.foreveryoung.google_connect.GetGoogleCookie;
import com.foreveryoung.models.Doctor;
import com.foreveryoung.views.fragments.dialog.CreateNewUserDialogFragment;
import com.foreveryoung.views.fragments.dialog.ProgressDialogFragment;

public class SignIn extends Fragment implements OnClickListener {

	private EditText mEnterUserName;
	private EditText mEnterPassWord;
	private Button mSubmitButton;
	private TextView mForgetPWDButton;
	private TextView notification;
	private ProgressBar mProgressBar;
	private Button mRegisterButton;
	 
	private static final int USER_PERMISSION = 989;
	 private AccountManager accountManager;
	 private Account account;
	 boolean expireToken = false;
	  
	 private DefaultHttpClient httpclient = new DefaultHttpClient();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_signin, container,
				false);
        account = getAccount();
        mEnterUserName = (EditText) rootView.findViewById(R.id.email);
        mEnterPassWord = (EditText) rootView.findViewById(R.id.password);
        mSubmitButton = (Button) rootView.findViewById(R.id.sign_in_button);
        mForgetPWDButton = (TextView) rootView.findViewById(R.id.forgot_password_link);
        notification = (TextView) rootView.findViewById(R.id.notification);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.loadingProgressBar);
        mRegisterButton = (Button) rootView.findViewById(R.id.btn_register);
        
        mEnterPassWord.setTypeface(Typeface.SANS_SERIF);
        mEnterUserName.setTypeface(Typeface.SANS_SERIF);
        
        mEnterUserName.setHint("Input username here");
        mEnterPassWord.setHint("Input password here");
        
        mSubmitButton.setOnClickListener(this);
        mForgetPWDButton.setOnClickListener(this);
        mRegisterButton.setOnClickListener(this);
        
        return rootView;
    }
    
	 @Override
	public void onClick(View v) {
		 
		  switch (v.getId()) {
		   
		  //send request to get a new or existing token
		  case R.id.forgot_password_link:
		 
		   expireToken = false;
		   accountManager.getAuthToken(account, "ah", null, false, new OnTokenAcquired(), null);
		   break;
		 
		  //send request to expire a token 
		  case R.id.expireToken:
		 
		   expireToken = true;
		   accountManager.getAuthToken(account, "ah", null, false, new OnTokenAcquired(), null);
		   break;
		 
		  case R.id.sign_in_button:
			String UserName = mEnterUserName.getText().toString();
  			String PassWord = mEnterPassWord.getText().toString();
  			
  			Authenticate sign = new Authenticate();
  			
  			try {
  				
  				sign.authenticate(UserName, PassWord);

  				Intent i;
  				
  				if (SessionData.getInstance().getUser().getClass() == Doctor.class) {
  					i = new Intent(getActivity(), ForeverYoungDoctor.class);     
  				} else {
  					
  					ProgressDialogFragment pf = ProgressDialogFragment.newInstance("Loading");
  					pf.show(getFragmentManager(), "progress_dialog");
  					
  					SessionData.getInstance().loadDummyData(getActivity());
  					pf.updateMessage("Loading complete. Generating suggestions.");
  					Navigator.refreshSuggestions(getActivity());
  					pf.updateMessage("Loading complete. Setting up dashboard.");
  					
  					pf.dismiss();
  					
  					i = new Intent(getActivity(), ForeverYoungPatient.class);
  					
  				}
  				
  				startActivity(i);
  				
  			} catch(SignInFailureException sif) {
  				sif.printStackTrace();
  				int msgResID = R.string.err_failure;
  				switch(sif.getType()) {
  					case FAIL:
  						msgResID = R.string.err_failure;
  						break;
  					case EMPTY:
  						msgResID = R.string.err_empty;
  						break;
  					case NO_CONNECTION:
  						msgResID = R.string.err_no_connection;
  						break;
  					case UNKNOWN_ERROR:
  						msgResID = R.string.err_unknown;
  						break;
				default:
					break;
  				}
  				
  				Toast.makeText(getActivity().getBaseContext(),getString(msgResID),Toast.LENGTH_SHORT).show();
 
  			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
  			break;
		  
		  case R.id.btn_register:
			  CreateNewUserDialogFragment newUserDialog = CreateNewUserDialogFragment.newInstance();
			  newUserDialog.show(getActivity().getFragmentManager(), "create_user_dialog");
			  break;
		  }
		 }
	
	 @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	 {
	  switch(requestCode) {
	   
	  //if the user approved the use of the account make another request 
	  //for the auth token else display a message
	  case USER_PERMISSION: 
	   if (resultCode == Activity.RESULT_OK) {
	    accountManager.getAuthToken(account, "ah", null, false, new OnTokenAcquired(), null);
	   }
	   else if(resultCode ==  Activity.RESULT_CANCELED){
		   Toast.makeText(getActivity().getBaseContext(), "Permission denied by User!", 
			Toast.LENGTH_LONG).show();
	   }
	   break;
	  }
	 
	 }   


	 //using the auth token and ask for a auth cookie
	 protected void setAuthToken(Bundle bundle) {
	  String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
	  if(expireToken){
	   accountManager.invalidateAuthToken("ah", authToken);
	  }
	  else {
	   new GetGoogleCookie().execute(authToken);
	  }
	 }
	
	 // get a list of all Google accounts stored in the device account manager
	 // and then display them in a spinner for selection
	 private Account getAccount(){
	 
	  accountManager = AccountManager.get(getActivity().getApplicationContext());
	  Account[] accounts = accountManager.getAccountsByType("com.google");
	  
	  for(Account account: accounts){
	   Log.v("Account:", account.name + "/" + account.type);
	   return account;
	  }
	 
	  return null;
	 }
	
	//the result for the auth token request is returned to your application 
	//via the Account Manager Callback you specified when making the request.
	//check the returned bundle if an Intent is stored against the AccountManager.KEY_INTENT key.
	//if there is an Intent then start the activity using that intent to ask for user permission
	//otherwise you can retrieve the auth token from the bundle.
	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

		@Override
		public void run(AccountManagerFuture<Bundle>  result) {
		
		  Bundle bundle;
		
		  try {
		   bundle = result.getResult();
		   if (bundle.containsKey(AccountManager.KEY_INTENT)) {
		    Intent intent = bundle.getParcelable(AccountManager.KEY_INTENT);
		    intent.setFlags(intent.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
		    getActivity().startActivityForResult(intent, USER_PERMISSION);
		   } else {
		    setAuthToken(bundle);
		   }
		  }
		  catch(Exception e){
		   e.printStackTrace();
		  }
		 }
		
		
	};
}
