package com.foreveryoung.views.fragments.content;

import org.json.JSONException;
import org.json.JSONObject;

import com.foreveryoung.SessionData;
import com.foreveryoung.R;
import com.foreveryoung.models.Patient;

import android.os.Bundle;
import android.app.Fragment;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class Profile extends Fragment {

	private TextView mFullName;
	private TextView mAddress;
	private TextView mPassword;
	private TextView mEmail;
	private DatePicker mDateOfBirth;
	private TextView mDate;
	private String dob;
	private View rootView;
	private Button mEditButton;
	private Button mSaveButton;
	private KeyListener listener;
	private int year;
	private int month;
	private int day;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_profile, container,
				false);

		mFullName = (EditText) rootView.findViewById(R.id.fullname);
		mDateOfBirth = (DatePicker) rootView.findViewById(R.id.date_picker);
		mAddress = (EditText) rootView.findViewById(R.id.address);
//		mPassword = (EditText) rootView.findViewById(R.id.password);
		mEmail = (EditText) rootView.findViewById(R.id.email);
		mDate = (EditText) rootView.findViewById(R.id.date);
		mEditButton = (Button) rootView.findViewById(R.id.edit);
		mSaveButton = (Button) rootView.findViewById(R.id.save);
		mEditButton.setOnClickListener(getEditClickListener());
		mSaveButton.setOnClickListener(getSaveClickListener());

		// fetch the values from user object passed from the login
		// following are temporary values
		SessionData sdata = SessionData.getInstance();

		// replace with session data here
		dob = String.valueOf(((Patient) sdata.getUser()).getDob());
		try {
			dob = (new JSONObject(dob)).getString("day") + "/"
					+ (new JSONObject(dob)).getString("month") + "/"
					+ (new JSONObject(dob)).getString("year");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mFullName.setText(sdata.getUser().getFullName());
		

			mAddress.setText(((Patient) sdata.getUser()).getCity()
					+ ((Patient) sdata.getUser()).getProvince()
					+ ((Patient) sdata.getUser()).getCountry());

//		mPassword.setText(sdata.getUser().getPassword());
		mEmail.setText(((Patient) sdata.getUser()).getEmail());
		mDateOfBirth.init(year, month, day, null);
		mDate.setText(dob);

		listener = mFullName.getKeyListener();
		mDateOfBirth.setVisibility(View.GONE);

		return rootView;
	}

	private OnClickListener getSaveClickListener() {
		return new OnClickListener() {

			@Override
			public void onClick(View v) {
				// read and save the values in DB
				month = mDateOfBirth.getMonth();
				year = mDateOfBirth.getYear();
				day = mDateOfBirth.getDayOfMonth();
				// change the mode to save mode
				listener = mFullName.getKeyListener();
				mDateOfBirth.setVisibility(View.GONE);
				mDate.setText(day + "/" + (month + 1) + "/" + year);
				mDate.setVisibility(View.VISIBLE);
			}
		};
	}

	private OnClickListener getEditClickListener() {
		return new OnClickListener() {

			@Override
			public void onClick(View v) {
				// change the mode to edit mode
				mFullName.setKeyListener(listener);
				mAddress.setKeyListener(listener);
//				mPassword.setKeyListener(listener);
				mEmail.setKeyListener(listener);
				mDateOfBirth.setVisibility(View.VISIBLE);
				mDate.setVisibility(View.GONE);
			}
		};
	}

}
