package com.foreveryoung.views.trends;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.foreveryoung.views.LineGraph;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.lists.SleepList;

public class SleepActivityTrend extends Trend {


	@Override
	public void drawChart() {
		List<Date> listX = new ArrayList<Date>();
		List<Double> listY1 = new ArrayList<Double>();
		List<Double> listY2 = new ArrayList<Double>();

		setData(listX, listY1, listY2);
		double min = 0, max = 0;
		if (listX.size() != 0){
			min = max = listY1.get(0);
			for (int i = 0; i < listY1.size(); i++){
				if (listY1.get(i) > max){
					max = listY1.get(i);
				}
				if (listY2.get(i) > max){
					max = listY2.get(i);
				}
				if (listY1.get(i) < min){
					min = listY1.get(i);
				}
				if (listY2.get(i) < min){
					min = listY2.get(i);
				}
			}
		}

		LineGraph lg = new LineGraph();
		//GraphicalView chartView;

		FYGraph fyg1 = new FYGraph("", "", "SLEEP TIME (HRS)", 10, 15, (int)max + 40, 0, listX, listY1);
		fyg1.setGraphTitle("Deep Sleep Time");
		FYGraph fyg2 = new FYGraph("", "", "SLEEP TIME (HRS)", 10, 15, (int)max + 40, 0, listX, listY2);
		fyg2.setGraphTitle("Total Sleep Time");
		Date temp = new Date(getYear(), getMonth(), 1);
		chartView = lg.getViewTime(temp, fyg1, fyg2, getActivity());
		//LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.dashboard_chart_layout);	    
		layout.addView(chartView);			
	}

	@SuppressWarnings("deprecation")
	void setData(List<Date> listX, List<Double> listY1, List<Double> listY2){

		SleepList slList = (SleepList) sd.getUserHealthData().getList(ParameterType.SLEEP);
		Calendar mycal = new GregorianCalendar(getYear(), getMonth(), 1);
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		//System.out.println("Days       " + daysInMonth);
		List<Date> listXTemp = new ArrayList<Date>();
		List<Double> listY1Temp = new ArrayList<Double>();
		List<Double> listY2Temp = new ArrayList<Double>();

		for (int i = 0; i < slList.size(); i++){

			Date dateTemp = slList.get(i).getTimestamp();

			if ((getMonth() == dateTemp.getMonth()) && (getYear() == dateTemp.getYear())){
				listXTemp.add(slList.get(i).getTimestamp());
				listY1Temp.add(slList.get(i).getDeepSleep());
				listY2Temp.add(slList.get(i).getTotalSleep());
			}
		}
		int count, sum1, sum2;
		for (int i = 1; i <= daysInMonth; i++){
			count = 0;
			sum1 = 0;
			sum2 = 0;

			for (int j = 0; j < listXTemp.size(); j++){

				if (i == listXTemp.get(j).getDate()){
					sum1 += listY1Temp.get(j);
					sum2 += listY2Temp.get(j);
					count++;
				}
			}
			if (count > 0){
				listX.add(new Date(getYear(), getMonth(), i));
				listY1.add((double)sum1/count);
				listY2.add((double)sum2/count);
			}
		}

	}

	@Override
	protected void setMonth(int month){	
		sd.setSlMonth(month);
	}

	@Override
	protected void setYear(int year){
		sd.setSlYear(year);
	}

	@Override
	protected int getMonth(){	
		return sd.getSlMonth();
	}

	@Override
	protected int getYear(){	
		return sd.getSlYear();
	}

}
