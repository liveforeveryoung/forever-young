package com.foreveryoung.views.trends;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.foreveryoung.views.LineGraph;
import com.foreveryoung.SessionData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.lists.ExerciseList;

public class ExcerciseTrend extends Trend {

	@Override
	public void drawChart() {
		List<Date> listX = new ArrayList<Date>();
		List<Double> listY = new ArrayList<Double>();

		setData(listX, listY);
		
		double min = 0, max = 0;
		if (listX.size() != 0){
			min = max = listY.get(0);
			for (int i = 0; i < listY.size(); i++){
				if (listY.get(i) > max){
					max = listY.get(i);
				}
				if (listY.get(i) < min){
					min = listY.get(i);
				}
			}
		}
		LineGraph lg = new LineGraph();
		//GraphicalView chartView;
	
		FYGraph fyg1 = new FYGraph("Activity over this month", "", "No of Steps / 100", 10, 15, (int)max + 300, 0, listX, listY);
		Date temp = new Date(getYear(), getMonth(), 1);
		chartView = lg.getViewTime(temp, fyg1, null, getActivity());
		//LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.dashboard_chart_layout);	    
		layout.addView(chartView);
	}

	void setData(List<Date> listX, List<Double> listY){

		SessionData ad = SessionData.getInstance();
		ExerciseList exList = (ExerciseList) ad.getUserHealthData().getList(ParameterType.ACTIVITY);
		Calendar mycal = new GregorianCalendar(getYear(), getMonth(), 1);
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		//System.out.println("Days       " + daysInMonth);
		List<Date> listXTemp = new ArrayList<Date>();
		List<Double> listYTemp = new ArrayList<Double>();
		for (int i = 0; i < exList.size(); i++){
			Date dateTemp = exList.get(i).getTimestamp();
			if ((getMonth() == dateTemp.getMonth()) && (getYear() == dateTemp.getYear())){
				listXTemp.add(exList.get(i).getTimestamp());

				listYTemp.add((double)exList.get(i).getStepsTaken()/100);
			}
		}
		int count, sum;
		for (int i = 1; i <= daysInMonth; i++){
			count = 0;
			sum = 0;
			for (int j = 0; j < listXTemp.size(); j++){

				if (i == listXTemp.get(j).getDate()){
					sum += listYTemp.get(j);
					count++;
				}
			}
			if (count > 0){
				listX.add(new Date(getYear(), getMonth(), i));
				listY.add((double)sum/count);			
			}
		}

	} 
	
	@Override
	protected void setMonth(int month){	
		sd.setAcMonth(month);
	}
	
	@Override
	protected void setYear(int year){
		sd.setAcYear(year);
	}
	
	@Override
	protected int getMonth(){	
		return sd.getAcMonth();
	}
	
	@Override
	protected int getYear(){	
		return sd.getAcYear();
	}

}
