package com.foreveryoung.views.trends;

import java.util.Date;
import java.util.List;

public class FYGraph {
	
	String graphTitle;
	String xTitle;
	String yTitle1;
		
	int xRangeMax;
	int xRangeMin;
	int yRangeMax;
	int yRangeMin;
	List<Date> listX;
	List<Double> listY;
	
	public FYGraph( String graphTitle, String xTitle, String yTitle1, int xRangeMax,
			int xRangeMin, int yRangeMax, int yRangeMin, List<Date> listX,
			List<Double> listY){
		
		this.graphTitle = graphTitle;
		this.xTitle = xTitle;
		this.yTitle1 = yTitle1;
		this.xRangeMax = xRangeMax;
		this.xRangeMin = xRangeMin;
		this.yRangeMax = yRangeMax;
		this.yRangeMin = yRangeMin;
		this.listX = listX;
		this.listY = listY;
	}
	
	public List<Date> getListX() {
		return listX;
	}
	public void setListX(List<Date> listX) {
		this.listX = listX;
	}
	public List<Double> getListY() {
		return listY;
	}
	public void setListY(List<Double> listY) {
		this.listY = listY;
	}
	public String getGraphTitle() {
		return graphTitle;
	}
	public void setGraphTitle(String graphTitle) {
		this.graphTitle = graphTitle;
	}
	public String getxTitle() {
		return xTitle;
	}
	public void setxTitle(String xTitle) {
		this.xTitle = xTitle;
	}

	public String getyTitle1() {
		return yTitle1;
	}
	public void setyTitle1(String yTitle1) {
		this.yTitle1 = yTitle1;
	}
	public int getxRangeMax() {
		return xRangeMax;
	}
	public void setxRangeMax(int xRangeMax) {
		this.xRangeMax = xRangeMax;
	}
	public int getxRangeMin() {
		return xRangeMin;
	}
	public void setxRangeMin(int xRangeMin) {
		this.xRangeMin = xRangeMin;
	}
	public int getyRangeMax() {
		return yRangeMax;
	}
	public void setyRangeMax(int yRangeMax) {
		this.yRangeMax = yRangeMax;
	}
	public int getyRangeMin() {
		return yRangeMin;
	}
	public void setyRangeMin(int yRangeMin) {
		this.yRangeMin = yRangeMin;
	}
}
