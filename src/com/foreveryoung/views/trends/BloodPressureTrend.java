package com.foreveryoung.views.trends;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.foreveryoung.views.LineGraph;
import com.foreveryoung.SessionData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.lists.BloodPressureList;

public class BloodPressureTrend extends Trend {

	@Override
	public void drawChart() {

		List<Date> listX = new ArrayList<Date>();
		List<Double> listY1 = new ArrayList<Double>();
		List<Double> listY2 = new ArrayList<Double>();

		setData(listX, listY1, listY2);

		LineGraph lg = new LineGraph();
		
			
		FYGraph fyg1 = new FYGraph("Diastolic", "", "BLOOD PRESSURE", 10, 15, 200, 0, listX, listY1);
		FYGraph fyg2 = new FYGraph("Systolic", "", "BLOOD PRESSURE", 10, 15, 200, 0, listX, listY2);
		
		Date temp = new Date(getYear(), getMonth(), 1);
		chartView = lg.getViewTime(temp, fyg1, fyg2, getActivity());
		layout.addView(chartView);

	}

	void setData(List<Date> listX, List<Double> listY1, List<Double> listY2){

		SessionData ad = SessionData.getInstance();
		BloodPressureList bpList = (BloodPressureList) ad.getUserHealthData().getList(ParameterType.BLOOD_PRESSURE);
		Calendar mycal = new GregorianCalendar(getYear(), getMonth(), 1);
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		List<Date> listXTemp = new ArrayList<Date>();
		List<Double> listY1Temp = new ArrayList<Double>();
		List<Double> listY2Temp = new ArrayList<Double>();
		
		for (int i = 0; i < bpList.size(); i++){
			Date dateTemp = bpList.get(i).getTimestamp();

			if ((getMonth() == dateTemp.getMonth()) && (getYear() == dateTemp.getYear())){
				listXTemp.add(dateTemp);
				listY1Temp.add(bpList.get(i).getDiastolic());
				listY2Temp.add(bpList.get(i).getSystolic());
			}
		}
		int count, sum1, sum2;
		for (int i = 1; i <= daysInMonth; i++){
			count = 0;
			sum1 = 0;
			sum2 = 0;
			
			for (int j = 0; j < listXTemp.size(); j++){

				if (i == listXTemp.get(j).getDate()){
					sum1 += listY1Temp.get(j);
					sum2 += listY2Temp.get(j);
					count++;
				}
			}
			if (count > 0){
				listX.add(new Date(getYear(), getMonth(), i));
				listY1.add((double)sum1/count);
				listY2.add((double)sum2/count);
			}
		}
	}
	@Override
	protected void setMonth(int month){	
		sd.setBpMonth(month);
	}
	
	@Override
	protected void setYear(int year){
		sd.setBpYear(year);
	}
	
	@Override
	protected int getMonth(){	
		return sd.getBpMonth();
	}
	
	@Override
	protected int getYear(){	
		return sd.getBpYear();
	}
}
