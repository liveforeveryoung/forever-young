package com.foreveryoung.views.trends;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import com.foreveryoung.views.LineGraph;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.models.parameters.lists.HealthParameterList;
import com.foreveryoung.models.parameters.lists.HeartRateList;

public class HeartRateTrend extends Trend {


	@Override
	public void drawChart() {

		List<Date> listX = new ArrayList<Date>();
		List<Double> listY = new ArrayList<Double>();
		HealthParameterList hrList = sd.getUserHealthData().getList(ParameterType.HEART_RATE);
		setData(listX, listY, hrList);
		double min = 0, max = 0;
		if (listX.size() != 0){
			min = max = listY.get(0);
			for (int i = 0; i < listY.size(); i++){
				if (listY.get(i) > max){
					max = listY.get(i);
				}
				if (listY.get(i) < min){
					min = listY.get(i);
				}
			}
		}

		LineGraph lg = new LineGraph();
		//lg.setShowLegend(false);

		FYGraph fyg1 = new FYGraph("Heart rate over the month", "", "HEART RATE", 10, 10, (int)max + 30, (int)min - 30, listX, listY);

		Date temp = new Date(getYear(), getMonth(), 1);
		chartView = lg.getViewTime(temp, fyg1, null, getActivity());

		layout.addView(chartView);
	}

	void setData(List<Date> listX, List<Double> listY, HealthParameterList list){

		HeartRateList hrList = (HeartRateList)list;		
		Calendar mycal = new GregorianCalendar(getYear(), getMonth(), 1);
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
		List<Date> listXTemp = new ArrayList<Date>();
		List<Double> listYTemp = new ArrayList<Double>();

		for (int i = 0; i < hrList.size(); i++){
			Date dateTemp = hrList.get(i).getTimestamp();
			if ((getMonth() == dateTemp.getMonth()) && (getYear() == dateTemp.getYear())){
				listXTemp.add(hrList.get(i).getTimestamp());
				listYTemp.add((double)hrList.get(i).getHeartRate());
			}

		}
		int count, sum;
		for (int i = 1; i <= daysInMonth; i++){
			count = 0;
			sum = 0;
			for (int j = 0; j < listXTemp.size(); j++){

				if (i == listXTemp.get(j).getDate()){
					sum += listYTemp.get(j);
					count++;
				}
			}
			if (count > 0){
				listX.add(new Date(getYear(), getMonth(), i));
				listY.add((double)sum/count);			
			}
		}
	} 
	
	@Override
	protected void setMonth(int month){	
		sd.setHrMonth(month);
	}
	
	@Override
	protected void setYear(int year){
		sd.setHrYear(year);
	}
	
	@Override
	protected int getMonth(){	
		return sd.getHrMonth();
	}
	
	@Override
	protected int getYear(){	
		return sd.getHrYear();
	}
	
	
}
