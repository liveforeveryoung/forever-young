package com.foreveryoung.views.trends;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foreveryoung.ApplicationData;
import com.foreveryoung.Constants.ParameterType;
import com.foreveryoung.R;
import com.foreveryoung.SessionData;
import com.foreveryoung.models.parameters.lists.HealthParameterList.HealthTrendResult;

public class TrendConditionFrame extends Fragment {

	String text;
	RelativeLayout trendBox;
	HealthTrendResult hr;
	TextView title;
	TextView desc;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_trend_condition, container,
				false);
		
		trendBox = (RelativeLayout) rootView.findViewById(R.id.trend_condition_overall);
		title = (TextView) rootView.findViewById(R.id.trend_condition_title);
		desc = (TextView) rootView.findViewById(R.id.trend_condition_details);
		
		if (hr!=null) {
			Integer[] resIds = ApplicationData.healthTrendTextResIDs.get(hr);
			title.setText(resIds[0]);
			desc.setText(resIds[1]);
			trendBox.setBackgroundResource(resIds[2]);
		}
		return rootView;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void setHealthTrendResult(HealthTrendResult rs) {
		this.hr = rs;
	}
	
	public static TrendConditionFrame newInstance(ParameterType type) {
		TrendConditionFrame tf = new TrendConditionFrame();
		tf.setHealthTrendResult(SessionData.getInstance().getUserHealthData().getList(type).analyzeState());
		return tf;
	}
}