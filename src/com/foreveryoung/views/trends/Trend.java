package com.foreveryoung.views.trends;

import org.achartengine.GraphicalView;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import com.foreveryoung.R;
import com.foreveryoung.SessionData;

public class Trend extends Fragment {
	
	private Spinner sMonth;
	private Spinner sYear;
	protected GraphicalView chartView;
	protected LinearLayout layout;
	SessionData sd = SessionData.getInstance();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_trend, container,
				false);
		sMonth = (Spinner) rootView.findViewById(R.id.spinner1);
		sYear = (Spinner) rootView.findViewById(R.id.spinner2); 
		layout = (LinearLayout) rootView.findViewById(R.id.dashboard_chart_layout);

		sMonth.setSelection(getMonth());
		sYear.setSelection(getYear() - 2005);

		sMonth.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			boolean initialize = true; 
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				if (initialize){
					initialize  = false;
					return;
				}

				layout.removeView(chartView);
				setMonth(position);
				drawChart();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});

		sYear.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			boolean initialize = true; 

			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
			{
				if (initialize){
					initialize  = false;
					return;
				}
				layout.removeView(chartView);
				setYear(position + 2005);
				drawChart();
			}

			

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
		return rootView;
	}
	protected void setMonth(int position) {
		// TODO Auto-generated method stub
		
	}
	protected int getYear() {
		// TODO Auto-generated method stub
		return 0;
	}
	protected int getMonth() {
		// TODO Auto-generated method stub
		return 0;
	}
	protected void setYear(int i) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		drawChart();
	}
	
	protected void drawChart(){}
}
