package com.foreveryoung.views;

import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.foreveryoung.views.trends.FYGraph;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint.Align;

public class LineGraph {

	private int AXIS_TITLE_SIZE = 1;
	private int LINE_WIDTH = 4;
	private int POINT_STROKE_SIZE = 2;
	private int AxisTitleTextSize = 30;
	private int ChartTitleTextSize = 20;
	private int LabelsTextSize = 15;
	private int LegendTextSize = 20;
	private float PointSize = 10f;
	private int XLabels = 60;
	private int YLabels = 60;
	private boolean ApplyBackgroundColor = true;
	private int AxesColor = Color.BLACK;
	private int BackgroundColor = Color.WHITE;
	private int MarginsColor = Color.WHITE;
	private boolean ShowGrid = true;
	private Align XLabelsAlign = Align.CENTER;
	private int LabelsColor = Color.BLACK;  
	private int GridColor = Color.blue(100);
	private int XLabelsColor = Color.BLACK;
	private int YLabelsColor1 = Color.BLACK;
	private int YLabelsColor2 = Color.BLACK;
	private int topM = 30;
	private int leftM = 74;
	private int bottomM = 50;
	private int rightM = 5;
	private float labelPad = 30f;

	private boolean ShowLegend = true;

	public GraphicalView getViewXY(FYGraph graph1, FYGraph graph2, Context context){

		XYSeries series1 = new XYSeries(graph1.getGraphTitle(), 0);
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		int noOfSeries = 1;

		for(int i = 0; i < graph1.getListX().size(); i++){
			//series1.add(graph1.getListX().get(i), graph1.getListY().get(i));

		}
		dataset.addSeries(series1);

		if (graph2 != null){
			XYSeries series2 = new XYSeries(graph2.getGraphTitle(), 1);
			for(int i = 0; i < graph2.getListX().size(); i++){
				//series2.add(graph2.getListX().get(i), graph2.getListY().get(i));
			}
			dataset.addSeries(series2);
			noOfSeries = 2;
		}

		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer(noOfSeries);
		XYSeriesRenderer renderer1 = new XYSeriesRenderer();		
		renderer1.setColor(Color.BLACK);
		renderer1.setPointStyle(PointStyle.CIRCLE);
		mRenderer.addSeriesRenderer(renderer1);
		mRenderer.setYTitle(graph1.getyTitle1(), 0);		
		mRenderer.setYAxisMin(graph1.getyRangeMin(), 0);
		mRenderer.setYAxisMax(graph1.getyRangeMax(), 0);
		mRenderer.setYAxisAlign(Align.LEFT, 0);
		mRenderer.setYLabelsAlign(Align.LEFT, 0);
		setGraphProp(mRenderer);

		if (graph2 != null){
			XYSeriesRenderer renderer2 = new XYSeriesRenderer();
			renderer2.setColor(Color.BLUE);
			renderer2.setPointStyle(PointStyle.SQUARE);
			mRenderer.addSeriesRenderer(renderer2);
			mRenderer.setYTitle(graph2.getyTitle1(), 1);
			mRenderer.setYAxisMin(graph2.getyRangeMin(), 1);
			mRenderer.setYAxisMax(graph2.getyRangeMax(), 1);
			mRenderer.setYAxisAlign(Align.RIGHT, 1);
			mRenderer.setYLabelsAlign(Align.RIGHT, 1);
			mRenderer.setYLabelsColor(1, YLabelsColor2);
			mRenderer.setXTitle(graph1.getxTitle());
		}

		GraphicalView chartView = ChartFactory.getLineChartView(context, dataset, mRenderer);

		return chartView;
	}

	public GraphicalView getViewTime(Date curr, FYGraph graph1, FYGraph graph2, Context context){

		TimeSeries series1 = new TimeSeries(graph1.getGraphTitle());
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		int noOfSeries = 1;

		for(int i = 0; i < graph1.getListX().size(); i++){
			series1.add(graph1.getListX().get(i), graph1.getListY().get(i));

		}
		dataset.addSeries(series1);

		if (graph2 != null){
			TimeSeries series2 = new TimeSeries(graph2.getGraphTitle());
			for(int i = 0; i < graph2.getListX().size(); i++){
				series2.add(graph2.getListX().get(i), graph2.getListY().get(i));
			}
			dataset.addSeries(series2);
			noOfSeries = 2;
		}

		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer(noOfSeries);	
		mRenderer.addSeriesRenderer(fyRenderer(Color.RED));
		mRenderer.setYTitle(graph1.getyTitle1(), 0);		
		mRenderer.setYAxisMin(graph1.getyRangeMin(), 0);
		mRenderer.setYAxisMax(graph1.getyRangeMax(), 0);
		mRenderer.setYAxisAlign(Align.LEFT, 0);
		mRenderer.setYLabelsAlign(Align.LEFT, 0);
		mRenderer.setXRoundedLabels(false);
		setGraphProp(mRenderer);
		//int count = series1.getItemCount();
		//DateFormat fromFormat = new SimpleDateFormat("MMM, yy");
		//String date = fromFormat.format(curr);
		//mRenderer.setXTitle(date);

		if (graph2 != null){
			mRenderer.addSeriesRenderer(fyRenderer(Color.BLUE));
			mRenderer.setYTitle(graph2.getyTitle1(), 1);
			mRenderer.setYAxisMin(graph2.getyRangeMin(), 1);
			mRenderer.setYAxisMax(graph2.getyRangeMax(), 1);
			mRenderer.setYAxisAlign(Align.RIGHT, 1);
			mRenderer.setYLabelsAlign(Align.RIGHT, 1);
			mRenderer.setYLabelsColor(1, YLabelsColor2);

		}

		GraphicalView chartView = ChartFactory.getTimeChartView(context, dataset,
				mRenderer, "dd");

		return chartView;

	}

	private XYSeriesRenderer fyRenderer(int color) {
		XYSeriesRenderer renderer = new XYSeriesRenderer();
		renderer.setColor(color);
		renderer.setLineWidth(LINE_WIDTH);
		renderer.setPointStrokeWidth(POINT_STROKE_SIZE);
		//renderer.setPointStyle(PointStyle.CIRCLE);
		return renderer;
	}

	private void setGraphProp(XYMultipleSeriesRenderer mRenderer){
		mRenderer.setAxisTitleTextSize(AxisTitleTextSize);
		mRenderer.setChartTitleTextSize(ChartTitleTextSize);
		mRenderer.setLabelsTextSize(LabelsTextSize);
		mRenderer.setLegendTextSize(LegendTextSize);
		mRenderer.setPointSize(PointSize);
		mRenderer.setXLabels(XLabels);
		mRenderer.setYLabels(YLabels);
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setAxesColor(AxesColor);
		mRenderer.setBackgroundColor(BackgroundColor);
		mRenderer.setMarginsColor(MarginsColor);
		mRenderer.setShowGrid(ShowGrid);
		mRenderer.setXLabelsAlign(XLabelsAlign);	
		mRenderer.setLabelsColor(LabelsColor);          
		mRenderer.setShowGrid(ShowGrid);
		mRenderer.setGridColor(GridColor);
		mRenderer.setXLabelsColor(XLabelsColor);
		mRenderer.setYLabelsColor(0, YLabelsColor1);
		mRenderer.setYLabelsPadding(labelPad);
		mRenderer.setMargins(new int[]{30,74,50,5});
		mRenderer.setExternalZoomEnabled(true);
		mRenderer.setZoomEnabled(true);
		mRenderer.setFitLegend(true);
		mRenderer.setInScroll(true);
		mRenderer.setShowLegend(ShowLegend);
	}


	public int getAXIS_TITLE_SIZE() {
		return AXIS_TITLE_SIZE;
	}

	public void setAXIS_TITLE_SIZE(int aXIS_TITLE_SIZE) {
		AXIS_TITLE_SIZE = aXIS_TITLE_SIZE;
	}

	public int getAxisTitleTextSize() {
		return AxisTitleTextSize;
	}

	public void setAxisTitleTextSize(int axisTitleTextSize) {
		AxisTitleTextSize = axisTitleTextSize;
	}

	public int getChartTitleTextSize() {
		return ChartTitleTextSize;
	}

	public void setChartTitleTextSize(int chartTitleTextSize) {
		ChartTitleTextSize = chartTitleTextSize;
	}

	public int getLabelsTextSize() {
		return LabelsTextSize;
	}

	public void setLabelsTextSize(int labelsTextSize) {
		LabelsTextSize = labelsTextSize;
	}

	public int getLegendTextSize() {
		return LegendTextSize;
	}

	public void setLegendTextSize(int legendTextSize) {
		LegendTextSize = legendTextSize;
	}

	public float getPointSize() {
		return PointSize;
	}

	public void setPointSize(float pointSize) {
		PointSize = pointSize;
	}

	public int getXLabels() {
		return XLabels;
	}

	public void setXLabels(int xLabels) {
		XLabels = xLabels;
	}

	public int getYLabels() {
		return YLabels;
	}

	public void setYLabels(int yLabels) {
		YLabels = yLabels;
	}

	public boolean isApplyBackgroundColor() {
		return ApplyBackgroundColor;
	}

	public void setApplyBackgroundColor(boolean applyBackgroundColor) {
		ApplyBackgroundColor = applyBackgroundColor;
	}

	public int getAxesColor() {
		return AxesColor;
	}

	public void setAxesColor(int axesColor) {
		AxesColor = axesColor;
	}

	public int getBackgroundColor() {
		return BackgroundColor;
	}

	public void setBackgroundColor(int backgroundColor) {
		BackgroundColor = backgroundColor;
	}

	public int getMarginsColor() {
		return MarginsColor;
	}

	public void setMarginsColor(int marginsColor) {
		MarginsColor = marginsColor;
	}

	public boolean isShowGrid() {
		return ShowGrid;
	}

	public void setShowGrid(boolean showGrid) {
		ShowGrid = showGrid;
	}

	public Align getXLabelsAlign() {
		return XLabelsAlign;
	}

	public void setXLabelsAlign(Align xLabelsAlign) {
		XLabelsAlign = xLabelsAlign;
	}

	public int getLabelsColor() {
		return LabelsColor;
	}

	public void setLabelsColor(int labelsColor) {
		LabelsColor = labelsColor;
	}

	public int getGridColor() {
		return GridColor;
	}

	public void setGridColor(int gridColor) {
		GridColor = gridColor;
	}

	public int getXLabelsColor() {
		return XLabelsColor;
	}

	public void setXLabelsColor(int xLabelsColor) {
		XLabelsColor = xLabelsColor;
	}

	public int getYLabelsColor1() {
		return YLabelsColor1;
	}

	public void setYLabelsColor1(int yLabelsColor1) {
		YLabelsColor1 = yLabelsColor1;
	}

	public int getYLabelsColor2() {
		return YLabelsColor2;
	}

	public void setYLabelsColor2(int yLabelsColor2) {
		YLabelsColor2 = yLabelsColor2;
	}

	public boolean isShowLegend() {
		return ShowLegend;
	}

	public void setShowLegend(boolean showLegend) {
		ShowLegend = showLegend;
	}
	public float getLabelPad() {
		return labelPad;
	}

	public void setLabelPad(float labelPad) {
		this.labelPad = labelPad;
	}

	public int getTopM() {
		return topM;
	}

	public void setTopM(int topM) {
		this.topM = topM;
	}

	public int getLeftM() {
		return leftM;
	}

	public void setLeftM(int leftM) {
		this.leftM = leftM;
	}

	public int getBottomM() {
		return bottomM;
	}

	public void setBottomM(int bottomM) {
		this.bottomM = bottomM;
	}

	public int getRightM() {
		return rightM;
	}

	public void setRightM(int rightM) {
		this.rightM = rightM;
	}


}
