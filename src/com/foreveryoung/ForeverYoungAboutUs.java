package com.foreveryoung;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class ForeverYoungAboutUs extends FYBootstrapActivity {
		
		private ImageView logo;
		private RelativeLayout aboutContent;
	
		public ForeverYoungAboutUs() {
			super();
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_about_app);
	        
	        logo = (ImageView) findViewById(R.id.logo_welcome);
	        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.about_app_slide_up);
	        aboutContent = (RelativeLayout) findViewById(R.id.about_content);
	        aboutContent.startAnimation(slideUp);
	}

	}
