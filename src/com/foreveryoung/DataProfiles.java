package com.foreveryoung;

import android.util.Log;

public class DataProfiles {
	
	// Dummy Data
	private static Integer[] FIT_PERSON_ARRAY = {120,100,9,6,5,0,3,5,5,20000,9000,150,30,85,50,80,65,165,125};
	private static Integer[] OBESE_PERSON_ARRAY = {135,120,10,6,8,2,2,20,0,6000,1000,30,0,95,75,95,80,100,90};
	private static Integer[] INACTIVE_PERSON_ARRAY = {130,110,10,7,8,2,2,20,0,6000,1000,10,0,95,75,85,75,90,70};
	private static Integer[] STRESSED_PERSON_ARRAY = {135,125,4,1,10,1,2,2,10,12000,10000,30,0,95,75,95,70,135,125};
	private static Integer[] HIGH_BLOOD_PRESSURE_PERSON_ARRAY = {155,140,6,3,20,10,3,5,5,12000,10000,10,0,90,70,110,100,135,125};
	
	Integer[] data = new Integer[19]; 
	
	public enum PersonType {
		FIT,OBESE,INACTIVE,STRESSED,HIGH_BP
	}
	
	public int	systolic_high, systolic_low, sleep_high, sleep_low,
				times_awake_high, times_awake_low, deep_sleep_low,
				weight_delta_high, weight_delta_low, steps_taken_high, steps_taken_low,
				active_time_high, active_time_low, resting_heart_rate_high, resting_heart_rate_low, 
				diastolic_high, diastolic_low,
				exercise_heart_rate_high, exercise_heart_rate_low;
	
	public DataProfiles(PersonType type) {
		
		switch (type) {
			case FIT:
				data = FIT_PERSON_ARRAY;
				break;
			case OBESE:
				data = OBESE_PERSON_ARRAY;
				break;
			case INACTIVE:
				data = INACTIVE_PERSON_ARRAY;
				break;
			case STRESSED:
				data = STRESSED_PERSON_ARRAY;
				break;
			case HIGH_BP:
				data = HIGH_BLOOD_PRESSURE_PERSON_ARRAY;
				break;
			default:
				break;
		}
		
		
		try {
			
			systolic_high = data[0];
			systolic_low = data[1];
			sleep_high = data[2];
			sleep_low = data[3];
			times_awake_high = data[4];
			times_awake_low = data[5];
			deep_sleep_low = data[6];
			weight_delta_high = data[7];
			weight_delta_low = data[8];
			steps_taken_high = data[9];
			steps_taken_low = data[10];
			active_time_high = data[11];
			active_time_low = data[12];
			resting_heart_rate_high = data[13];
			resting_heart_rate_low = data[14];
			diastolic_high = data[15];
			diastolic_low = data[16];
			exercise_heart_rate_high = data[17];
			exercise_heart_rate_low = data[18];
			
		} catch (NullPointerException ex) {
			Log.d("DATA MISMATCH","The amount of fields did not match template.");
		}
	}
	
	
}
