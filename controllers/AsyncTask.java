package com.foreveryoung.controllers;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AsyncTask {

	private static AsyncHttpClient client = new AsyncHttpClient();
	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler){
		client.post(url, params, responseHandler);
	}
}
