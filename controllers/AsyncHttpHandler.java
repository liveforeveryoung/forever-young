package com.foreveryoung.controllers;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.foreveryoung.ApplicationData;
import com.foreveryoung.models.Suggestion;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AsyncHttpHandler {

	/*
	 * trying to do with async http client
	 */
	private static final String BASE_URL = "http://health-engine.herokuapp.com/";
	//private static AsyncHttpClient client = new AsyncHttpClient();
	ArrayList<Suggestion> final_out = null;
	Suggestion.SuggestionType _type_rec = null;
	public void getSuggestionEngine(RequestParams params) throws JSONException{
		Log.d("inside getSuggestionEngine", params.toString());
		final_out = new ArrayList<Suggestion>();
		AsyncTask.post(BASE_URL,params, new JsonHttpResponseHandler(){
		
		//client.post(BASE_URL,params, new JsonHttpResponseHandler(){
		@Override
		public void onSuccess(JSONArray suggestions){
			try{
			JSONObject first = suggestions.getJSONObject(0);
			String disp = first.toString();
			Log.d("in async ", disp);
			for(int i=0;i<suggestions.length();i++){
				JSONObject json_sugg = suggestions.getJSONObject(i);
				String _final_suggestion = json_sugg.getString("recommendation");
				String _url = json_sugg.getString("url");
				int id = json_sugg.getInt("id");
				id = id%100;
				switch(id){
					case 1:
						_type_rec = Suggestion.SuggestionType.ACTIVITY;
					case 2:
						_type_rec = Suggestion.SuggestionType.BLOOD_PRESSURE;
					case 3:
						_type_rec = Suggestion.SuggestionType.HEART_RATE;
					case 4:
						_type_rec = Suggestion.SuggestionType.SLEEP;
					default:
						_type_rec = null;
				}			
				Log.d("Fibal Suggestion ", _final_suggestion);
				final_out.add(new Suggestion(_final_suggestion,_url,_type_rec));
			}
			    
			}catch(JSONException je){
				je.printStackTrace();
			}
		}
	});
		ApplicationData.getInstance().setSuggestions(final_out);
	}
}
